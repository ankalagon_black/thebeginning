package com.example.thebeginning;

/**
 * Created by Константин on 08.04.2016.
 */
public interface onExtensionEventListener {

    void onPhotoDataEvent();

    void onPhotoBarcodeDataEvent();

    void onStringListDataEvent();

    void onHashMapDataEvent();

    void onFolderEvent();

    void onXmlDataEvent();

    void on3gpEvent();

    void onTxtEvent();
}
