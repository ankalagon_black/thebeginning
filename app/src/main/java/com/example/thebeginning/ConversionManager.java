package com.example.thebeginning;

import android.app.Activity;
import android.util.Log;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Константин on 19.07.2017.
 */

public class ConversionManager {
    private ExecutorService executorService;

    private FileManager target, source;

    private Activity activity;

    public ConversionManager(FileManager target, FileManager source, Activity activity){
        this.target = target;
        this.source = source;
        this.activity = activity;
    }

    public void convert(final List<ListItem> listItems, final ProgressBarDialog dialog, final int level){
        executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                dialog.waitForCreation(true);
                dialog.initializeFolderBar(listItems.size());

                target.createFolder();

                String firstLevelCatalogName = getFirstLevelCatalog(level);
                target.createFolder(firstLevelCatalogName);
                target.goAhead(firstLevelCatalogName);

                for (ListItem i : listItems) {
                    String name = i.getName();
                    String destinationName = target.prepareXml(source, name, level, activity);

                    if (createFolder(destinationName)) {
                        if(level != 2) source.goAhead(name);
                        target.goAhead(destinationName);

                        dialog.setCurrentFolder(name);
                        File[] files = source.getFiles();
                        if(files != null) {
                            dialog.initializeFileBar(files.length);
                            for (File j : files) {
                                dialog.setCurrentFile(j.getName());
                                target.writeFile(source, j, dialog);
                                dialog.incrementFileBar();
                            }
                        }

                        source.goBack();
                        if(!name.equals(destinationName)){
                            for (int j = 0; j < 4; j ++) target.goBack();
                        }
                        else target.goBack();

                        dialog.incrementFolderBar();
                    }
                    else break;
                }
                dialog.finish();
                shutdown(executorService);
            }
        });
    }

//    public void convert(final List<ListItem> listItems, final int level, final File cacheDir, final Callback callback, final boolean create){
//        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
//            @Override
//            public void onComplete() {
//                clear(cacheDir);
//                callback.onSuccess();
//            }
//
//            @Override
//            public void onError(Throwable e) {
//               callback.onError();
//            }
//        };
//
//        Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//                send(listItems, level, cacheDir, create);
//            }
//        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
//                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
//    }

//    public void send(final List<ListItem> listItems, final int level, final File cacheDir, boolean create) throws Exception {
//        File zippedFile = null;
//
//        if(create) {
//            target.createFolder();
//
//            String firstLevelCatalogName = getFirstLevelCatalog(level);
//            target.createFolder(firstLevelCatalogName);
//            target.goAhead(firstLevelCatalogName);
//
//            for (ListItem i : listItems) {
//                String name = i.getName();
//                String destinationName = target.prepareXml(source, name, level);
//
//                if (createFolder(destinationName)) {
//                    if (level != 2) source.goAhead(name);
//                    target.goAhead(destinationName);
//
//                    File[] files = source.getFiles();
//                    if (files != null) {
//                        for (File j : files) {
//                            if (j.getName().equals("mode.xml")) {
//                                target.writeFile(source, source.duplicateModeXmlAsSent(cacheDir, destinationName));
//                            } else target.writeFile(source, j);
//                        }
//                    }
//
//                    source.goBack();
//                    if (!name.equals(destinationName)) {
//                        for (int j = 0; j < 4; j++) target.goBack();
//                    } else target.goBack();
//                } else break;
//            }
//
//            target.goTo("");
//
//            zippedFile = zipFiles(firstLevelCatalogName);
//        }
//        else {
//            File[] files = target.getFiles();
//            if (files != null){
//                for (File i: files){
//                    String name = i.getName();
//                    if(name.substring(name.lastIndexOf(".") + 1).equals("zip")){
//                        zippedFile = i;
//                        break;
//                    }
//                }
//            }
//        }
//
//        if(zippedFile != null){
//            //collect all required data
//            //try to send data
//
//            //if successfully then update all mode.xml files (do not forget about level of source)
//            //else do nothing
//        }
//        else throw new Exception("Cannot create zip-file");
//    }

//    private void clear(File cacheDir){
//        target.deleteAll();
//
//        File[] files = cacheDir.listFiles();
//        if(files != null) for (File i: files) i.delete();
//    }

//    private File zipFiles(String folder){
//        File file = new File(target.getRoot(), folder), zippedFolder = new File(target.getRoot(), folder + ".zip");
//
//        try {
//            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zippedFolder)));
//
//            target.goAhead(file.getName());
//            zipRecursively(file, out);
//            target.goBack();
//
//            out.flush();
//            out.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        return zippedFolder;
//    }
//
//    private void zipRecursively(File file, ZipOutputStream outputStream) throws IOException {
//        if(file.isDirectory()){
//            ZipEntry zipEntry = new ZipEntry(target.getLocalPath() + "/");
//            outputStream.putNextEntry(zipEntry);
//            File[] files = file.listFiles();
//            if(files != null){
//                for (File i: files){
//                    target.goAhead(i.getName());
//                    zipRecursively(i, outputStream);
//                    target.goBack();
//                }
//            }
//        }
//        else {
//            ZipEntry zipEntry = new ZipEntry(target.getLocalPath());
//            outputStream.putNextEntry(zipEntry);
//
//            FileInputStream inputStream = new FileInputStream(file);
//
//            byte data[] = new byte[1024];
//            int count;
//
//            while ((count = inputStream.read(data)) != -1) {
//                outputStream.write(data, 0, count);
//            }
//
//            inputStream.close();
//        }
//    }

    private String getFirstLevelCatalog(int level){
        if(level == 2){
            String currentFolderName = source.getCurrentFolderName();

            source.goBack();

            String folderName = source.getCurrentFolderName();

            source.goAhead(currentFolderName);

            return folderName;
        }
        else return source.getCurrentFolderName();
    }

    private boolean createFolder(String destinationName){
        File file = new File(target.getChildFullPath(destinationName));
        return file.exists() || file.mkdirs();
    }

    private void shutdown(ExecutorService service){
        try{
            service.shutdown();
            service.awaitTermination(3, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            Log.v(WifiTransmissionManager.class.toString(), e.toString());
        }
        finally {
            service.shutdownNow();
        }
    }

//    public interface Callback{
//        void onSuccess();
//        void onError();
//    }
}
