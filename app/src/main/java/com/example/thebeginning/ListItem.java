package com.example.thebeginning;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Константин on 17.04.2016.
 */
public class ListItem implements Parcelable{

    private String name;
    private String state;

    public ListItem(String name, String state){
        this.name = name;
        this.state = state;
    }

    protected ListItem(Parcel in) {
        name = in.readString();
        state = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(state);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ListItem> CREATOR = new Creator<ListItem>() {
        @Override
        public ListItem createFromParcel(Parcel in) {
            return new ListItem(in);
        }

        @Override
        public ListItem[] newArray(int size) {
            return new ListItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state){
        this.state = state;
    }
}
