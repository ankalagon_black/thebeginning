package com.example.thebeginning;

/**
 * Created by Константин on 08.04.2016.
 */
public class ExtensionManager {

    private onExtensionEventListener onExtensionEventListener;
    private String extension, nameWithoutExtension;

    public ExtensionManager(String name){
        int position = name.lastIndexOf(".");
        if(position != -1){
            extension = name.substring(position + 1, name.length());
            nameWithoutExtension = name.substring(0, name.lastIndexOf("."));
        }
        else {
            extension = "";
            nameWithoutExtension = "";
        }
    }

    public void run(){
        if(extension.equals(PhotoData.class.getSimpleName())) onExtensionEventListener.onPhotoDataEvent();

        else if(extension.equals(PhotoBarcodeData.class.getSimpleName())) onExtensionEventListener.onPhotoBarcodeDataEvent();

        else if(extension.equals(StringListData.class.getSimpleName())) onExtensionEventListener.onStringListDataEvent();

        else if(extension.equals(HashMapData.class.getSimpleName())) onExtensionEventListener.onHashMapDataEvent();

        else if(extension.equals("")) onExtensionEventListener.onFolderEvent();

        else if(extension.equals("xml")) onExtensionEventListener.onXmlDataEvent();

        else if(extension.equals("3gp")) onExtensionEventListener.on3gpEvent();

        else if(extension.equals("txt")) onExtensionEventListener.onTxtEvent();
    }

    public void setOnExtensionEventListener(onExtensionEventListener onExtensionEventListener){
        this.onExtensionEventListener = onExtensionEventListener;
    }

    public String getExtension(){
        return extension;
    }

    public String getNameWithoutExtension(){
        return nameWithoutExtension;
    }
}
