package com.example.thebeginning.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.thebeginning.FileController;
import com.example.thebeginning.R;
import com.example.thebeginning.activities.BaseAppCompatActivity;
import com.example.thebeginning.extended.SettingsDomain;
import com.example.thebeginning.extended.base.GetDomain;

import java.io.File;

import static com.example.thebeginning.MainActivity.externalStoragePath;

/**
 * Created by Константин on 16.01.2016.
 */
public class SettingsActivity extends BaseAppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, GetDomain<SettingsDomain> {

    private static final String path_on_sd_card_key = "path_on_sd_card";
    private static final String ip_key = "ip";
    private static final String path_on_server_key = "path_on_server";
    private static final String user_name = "user_name";
    private static final String user_password = "user_password";
    private static final String backwards_synchronization_path ="backwards_synchronization_path";
    private static final String server_backwards_synchronization_path = "server_backwards_synchronization_path";

    private SettingsFragment settingsFragment;
    private static String appSDPath;
    private boolean appSDPathChanged;


    public static String getPathOnSdCard(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(path_on_sd_card_key, context.getString(R.string.path_on_sd_card_default));
    }

    public static String getBackwards_synchronization_path(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(backwards_synchronization_path, context.getString(R.string.backwards_synchronization_path_default));
    }

    public static String getIp(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(ip_key, context.getString(R.string.ip_default));
    }

    public static String getPathOnServer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(path_on_server_key, context.getString(R.string.path_on_server_default));
    }

    public static String getServer_backwards_synchronization_path(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(server_backwards_synchronization_path, context.getString(R.string.server_backwards_synchronization_path_default));
    }

    public static String getUserName(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(user_name, context.getString(R.string.user_name_default));
    }

    public static String getUserPassword(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(user_password, context.getString(R.string.user_password_default));
    }


    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        appSDPath = getPathOnSdCard(this);
        appSDPathChanged = false;

        toolbar = findViewById(R.id.settings_toolbar);
        updateTitle();
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if (appSDPathChanged) setResult(RESULT_OK, intent);
                else setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        getFragmentManager().beginTransaction()
                .replace(R.id.settings_layout, settingsFragment = new SettingsFragment(), SettingsFragment.TAG).commit();
    }

    public void updateTitle(){
        toolbar.setTitle(R.string.title_activity_settings_activity);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key){
            case path_on_sd_card_key:
                String value = sharedPreferences.getString(key, getString(R.string.path_on_sd_card_default));
                if(FileController.initialize(value)){

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.message).setMessage(R.string.root_catalog_was_changed);
                    builder.setPositiveButton(R.string.ok, null);

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    appSDPathChanged = true;
                }
                else{
                    if(!value.equals(appSDPath)){
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(key, appSDPath);
                        editor.apply();

                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle(R.string.message).setMessage(R.string.new_root_catalog_path_is_incorrect);
                        builder.setPositiveButton(R.string.ok, null);

                        AlertDialog dialog = builder.create();
                        dialog.show();

                        appSDPathChanged = false;
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        settingsFragment.getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        settingsFragment.getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public SettingsDomain getDomain() {
        File file = new File(externalStoragePath + "/TheBeginning_certificate");
        file.mkdirs();

        return new SettingsDomain(this);
    }
}
