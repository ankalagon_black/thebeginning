package com.example.thebeginning.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.ListView;

import com.example.thebeginning.FileManager;
import com.example.thebeginning.MainActivity;
import com.example.thebeginning.ProgressBarDialog;
import com.example.thebeginning.R;
import com.example.thebeginning.WifiTransmissionManager;
import com.example.thebeginning.activities.BaseAppCompatActivity;
import com.example.thebeginning.data_types.GlobalSettings;
import com.example.thebeginning.extended.GetSettingsDialog;
import com.example.thebeginning.settings.adapters.ChooseLanguageAdapter;

import java.util.Locale;

/**
 * Created by Константин on 16.01.2016.
 */
public class SettingsFragment extends PreferenceFragment {
    public static String TAG = "SettingsFragment";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        initialize();
    }

    private void initialize(){
        addPreferencesFromResource(R.xml.preferences);
        Preference synchronization = findPreference(getString(R.string.synchronization)), backwardsSynchronization = findPreference(getString(R.string.backwards_synchronization));
        synchronization.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                action_synchronize();
                return true;
            }
        });
        backwardsSynchronization.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                backwards_synchronize();
                return true;
            }
        });

        Preference getSettings = findPreference(getString(R.string.get_settings_key));
        getSettings.setOnPreferenceClickListener(preference -> {
            GetSettingsDialog dialog = new GetSettingsDialog();
            dialog.show(getFragmentManager(), dialog.getTag());
            return true;
        });

        Preference selectLanguage = findPreference(getString(R.string.language));

        String[] languagesTitles = getResources().getStringArray(R.array.languages);
        String currentLanguage = ((BaseAppCompatActivity) getActivity()).getCurrentLanguage();

        selectLanguage.setTitle(String.format(Locale.US, getString(R.string.current_language), GlobalSettings.getLanguageTitle(currentLanguage, languagesTitles)));

        selectLanguage.setOnPreferenceClickListener(preference -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsFragment.this.getActivity());

            //ListView listView = (ListView) LayoutInflater.from(SettingsFragment.this.getActivity()).inflate(R.layout.choose_a_language_dialog, null);
            //listView.setAdapter(chooseLanguageAdapter);

            ChooseLanguageAdapter chooseLanguageAdapter = new ChooseLanguageAdapter(getActivity(), MainActivity.globalSettings.getLanguages(), languagesTitles);

            builder.setTitle(R.string.choose_a_language).
                    setAdapter(chooseLanguageAdapter, (dialogInterface, i) -> {
                        ((BaseAppCompatActivity) getActivity()).setCurrentLanguage(chooseLanguageAdapter.getItem(i));
                        refresh();
                    }).
                    setNegativeButton(R.string.cancel, null).
                    create().
                    show();

            return true;
        });
    }

    private boolean isWifiEnabled(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo == null) return false;
        return networkInfo.isConnected();
    }

    public void action_synchronize(){
        Context context = getActivity();
        if(isWifiEnabled(context)) {
            WifiTransmissionManager manager = new WifiTransmissionManager(SettingsActivity.getIp(context),
                    SettingsActivity.getPathOnServer(context),
                    SettingsActivity.getUserName(context),
                    SettingsActivity.getUserPassword(context),
                    new FileManager(MainActivity.externalStoragePath + SettingsActivity.getPathOnSdCard(context), ""), getActivity());
            if(manager.isServerAvailable()){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.message);
                builder.setPositiveButton(R.string.ok, null);

                if(manager.synchronize()) builder.setMessage(R.string.sync_was_succesfully_finished);
                else builder.setMessage(R.string.error_occured_during_sync);

                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.message).setMessage(R.string.cannot_establish_connection);
                builder.setPositiveButton(R.string.ok, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.message).setMessage(R.string.no_wifi_connection);
            builder.setPositiveButton(R.string.ok, null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void backwards_synchronize(){
        Context context = getActivity();
        if(isWifiEnabled(context)) {
            WifiTransmissionManager manager = new WifiTransmissionManager(SettingsActivity.getIp(context),
                    SettingsActivity.getServer_backwards_synchronization_path(context),
                    SettingsActivity.getUserName(context),
                    SettingsActivity.getUserPassword(context),
                    new FileManager(MainActivity.externalStoragePath + SettingsActivity.getBackwards_synchronization_path(context), ""), getActivity());
            if(manager.isServerAvailable()){
                ProgressBarDialog progressBarDialog = new ProgressBarDialog();
                progressBarDialog.show(getFragmentManager(), ProgressBarDialog.class.getSimpleName());
                manager.transmit(progressBarDialog);
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.message).setMessage(R.string.cannot_establish_connection);
                builder.setPositiveButton(R.string.ok, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.message).setMessage(R.string.no_wifi_connection);
            builder.setPositiveButton(R.string.ok, null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void refresh(){
        getPreferenceScreen().removeAll();
        initialize();
        ((SettingsActivity) getActivity()).updateTitle();
    }
}
