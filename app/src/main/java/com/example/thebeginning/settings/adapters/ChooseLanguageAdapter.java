package com.example.thebeginning.settings.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thebeginning.data_types.GlobalSettings;

import java.util.List;

/**
 * Created by Константин on 21.01.2018.
 */

public class ChooseLanguageAdapter extends ArrayAdapter<String> {
    private String[] languagesTitles;

    public ChooseLanguageAdapter(@NonNull Context context, @NonNull List<String> objects, String[] languagesTitles) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.languagesTitles = languagesTitles;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        TextView textView = (TextView) view;
        textView.setText(GlobalSettings.getLanguageTitle(getItem(position), languagesTitles));

        return view;
    }
}
