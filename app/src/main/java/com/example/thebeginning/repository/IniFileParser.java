package com.example.thebeginning.repository;

import com.example.thebeginning.data_types.GlobalSettings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Константин on 14.04.2016.
 */
public class IniFileParser {
    public static final String INI_FILE_NAME = "mode.ini";

    private File iniFile;

    public IniFileParser(File iniFile){
        this.iniFile = iniFile;
    }

    public GlobalSettings getGlobalSettings(){
        if(iniFile.exists()){
            List<String> modes = new ArrayList<>(), languages = new ArrayList<>();
            int jpegQuality = 5;

            try{
                BufferedReader bufferedReader = new BufferedReader(new FileReader(iniFile));

                String line;
                while ((line = bufferedReader.readLine()) != null){
                    if(line.matches("modes=" + "(" + GlobalSettings.MODE_FULL + "|" + GlobalSettings.MODE_PHOTO + "|" + GlobalSettings.MODE_INVENTORY + ")" +
                            "(,(" + GlobalSettings.MODE_FULL + "|" + GlobalSettings.MODE_PHOTO + "|" + GlobalSettings.MODE_INVENTORY + "))*")){
                        modes.addAll(Arrays.asList(line.replace("modes=","").split(",")));
                    }
                    else if(line.matches("languages=(" + GlobalSettings.RU + "|" + GlobalSettings.EN + "|" + GlobalSettings.CH + ")" +
                            "(,(" + GlobalSettings.RU + "|" + GlobalSettings.EN + "|" + GlobalSettings.CH + "))*")){
                        languages.addAll(Arrays.asList(line.replace("languages=","").split(",")));
                    }
                    else if(line.matches("jpegQuality=10|[1-9]")){
                        jpegQuality = Integer.parseInt(line.replace("jpegQuality",""));
                    }
                }

                bufferedReader.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }

            if(modes.isEmpty()) modes.add(GlobalSettings.MODE_READ_ONLY);
            if(languages.isEmpty()) languages.add(GlobalSettings.RU);

            return new GlobalSettings(modes, languages, jpegQuality);
        }
        else return new GlobalSettings(Collections.singletonList(GlobalSettings.MODE_READ_ONLY), Collections.singletonList(GlobalSettings.RU), 5);
    }
}
