package com.example.thebeginning;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * Created by Константин on 02.05.2016.
 */
public class ShowCameraResultFialog extends DialogFragment{
    private FileManager fileManager;
    private MediaPlayer mediaPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        fileManager = bundle.getParcelable(CameraPreview.fileManagerKey);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final CameraPreview cameraPreview = (CameraPreview) getActivity();
        final Dialog dialog = getDialog();
        dialog.setCancelable(false);
        final AbstractData abstractData = cameraPreview.getAbstractData();
        View view;
        if(abstractData == null){
            view = inflater.inflate(R.layout.camera_show_result_video, null);
            final VideoView videoView = view.findViewById(R.id.show_video_view);
            final String name = String.valueOf(fileManager.getFilesCount() - 1) + ".3gp";
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer = mp;
                }
            });
            videoView.setVideoPath(fileManager.getFullPath() + "/" + name);
            videoView.setZOrderOnTop(true);
            videoView.seekTo(100);
            final ImageButton playButton = view.findViewById(R.id.video_play_button);
            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (videoView.isPlaying()) {
                        playButton.setImageResource(R.drawable.ic_action_playback_play);
                        mediaPlayer.pause();
                    } else {
                        playButton.setImageResource(R.drawable.ic_action_playback_pause);
                        mediaPlayer.start();
                    }
                }
            });
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playButton.setImageResource(R.drawable.ic_action_playback_play);
                }
            });
            final CommentaryData commentaryData = new CommentaryData();
            ImageButton okButton = view.findViewById(R.id.show_video_action_ok);
            ImageButton cancelButton = view.findViewById(R.id.show_video_action_cancel);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentaryData.getText() != null) {
                        String newName = name.substring(0, name.lastIndexOf(".")) + ".txt";
                        fileManager.writeToFile(newName, commentaryData);
                    }
                    dialog.dismiss();
                    cameraPreview.startPreview();
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fileManager.delete(name);
                    dialog.dismiss();
                    cameraPreview.startPreview();
                }
            });
            Button commentaryButton = view.findViewById(R.id.show_video_add_commentary);
            commentaryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(cameraPreview);
                    builder.setTitle(R.string.input_comment);
                    final EditText editText = new EditText(cameraPreview);
                    if(commentaryData.getText() != null) editText.setText(commentaryData.getText());
                    builder.setView(editText);
                    builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            commentaryData.setText(editText.getText().toString());
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

        }
        else if(abstractData.getClass().equals(PhotoData.class)){
            view = inflater.inflate(R.layout.camera_show_result_photo, null);
            ZoomableImageView zoomableImageView = view.findViewById(R.id.result_photo_data);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            zoomableImageView.setImage(abstractData.getBitmap(), displayMetrics.widthPixels, displayMetrics.heightPixels);
            final CommentaryData commentaryData = new CommentaryData();
            ImageButton okButton = view.findViewById(R.id.content_button_ok);
            ImageButton cancelButton = view.findViewById(R.id.content_button_cancel);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = String.valueOf(fileManager.getFilesCount());
                    fileManager.writeToFile(name + "." + abstractData.getClass().getSimpleName() ,abstractData);
                    if(commentaryData.getText() != null) fileManager.writeToFile(name + ".txt", commentaryData);
                    dialog.dismiss();
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    cameraPreview.startPreview();
                }
            });
            Button addButton = view.findViewById(R.id.result_photo_add_commentary);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(cameraPreview);
                    builder.setTitle(R.string.input_comment);
                    final EditText editText = new EditText(cameraPreview);
                    if(commentaryData.getText() != null) editText.setText(commentaryData.getText());
                    builder.setView(editText);
                    builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            commentaryData.setText(editText.getText().toString());
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
        }
        else if(abstractData.getClass().equals(PhotoBarcodeData.class)){
            view = inflater.inflate(R.layout.camera_show_result_photo_barcode, null);
            ZoomableImageView zoomableImageView = view.findViewById(R.id.result_photo_data);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            dialog.getWindow().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            zoomableImageView.setImage(abstractData.getBitmap(), displayMetrics.widthPixels, displayMetrics.heightPixels);
            TextView textView = view.findViewById(R.id.result_barcode_data);
            textView.setText(abstractData.getBarcode());
            ImageButton okButton = view.findViewById(R.id.content_button_ok);
            ImageButton cancelButton = view.findViewById(R.id.content_button_cancel);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fileManager.writeToFile(String.valueOf(fileManager.getFilesCount()) + "." + abstractData.getClass().getSimpleName(), abstractData);
                    dialog.dismiss();
                    cameraPreview.startPreview();
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    cameraPreview.startPreview();
                }
            });
        }
        else{
            view = inflater.inflate(R.layout.camera_show_result_string_list, null);
            ListView listView = view.findViewById(R.id.result_string_list);
            listView.setAdapter(new ArrayAdapter<>(cameraPreview, android.R.layout.simple_list_item_1, abstractData.getList()));
            ImageButton okButton = view.findViewById(R.id.content_button_ok);
            ImageButton cancelButton = view.findViewById(R.id.content_button_cancel);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fileManager.writeToFile(String.valueOf(fileManager.getFilesCount()) + "." + abstractData.getClass().getSimpleName(), abstractData);
                    dialog.dismiss();
                    cameraPreview.startPreview();
                    cameraPreview.setPreviewCallback();
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    cameraPreview.startPreview();
                    cameraPreview.setPreviewCallback();
                }
            });
        }
        return view;
    }
}
