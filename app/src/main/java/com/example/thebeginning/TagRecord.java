package com.example.thebeginning;

/**
 * Created by Константин on 24.04.2016.
 */
public class TagRecord {
    private String tag;
    private String value;

    public TagRecord(String tag, String value){
        this.tag = tag;
        this.value = value;
    }

    public String getTag(){
        return tag;
    }

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }
}
