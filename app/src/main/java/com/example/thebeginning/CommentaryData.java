package com.example.thebeginning;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

/**
 * Created by Константин on 30.04.2016.
 */
public class CommentaryData extends AbstractData {

    private String text;

    public CommentaryData(){
        text = null;
    }

    @Override
    public void write(FileOutputStream outputStream) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(text);
            writer.close();

        } catch (IOException e) {
            Log.v(CommentaryData.class.toString(), e.toString());
        }
    }

    @Override
    public void read(FileInputStream inputStream) {
        try {
            StringWriter writer = new StringWriter();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                writer.append(line);
                writer.append("\n");
            }
            text = writer.toString();
            reader.close();
            writer.close();
        }
        catch (IOException e) {
            Log.v(CommentaryData.class.toString(), e.toString());
        }
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }
}
