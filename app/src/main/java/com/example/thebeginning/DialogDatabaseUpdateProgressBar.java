package com.example.thebeginning;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Locale;

/**
 * Created by apple on 10.08.16.
 */
public class DialogDatabaseUpdateProgressBar extends DialogFragment {

    private TextProgressBar progressBar;
    private Button closeButton;

    private Handler handler;
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        setCancelable(false);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ProgressBarDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dialog = getDialog();
        dialog.setTitle(R.string.database_progress_dialog_parsing);
        View view = inflater.inflate(R.layout.dialog_database_progress_bar, null);
        progressBar = view.findViewById(R.id.database_progress_bar);
        closeButton = view.findViewById(R.id.database_progress_bar_close_button);
        closeButton.setAlpha(0.4f);
        closeButton.setEnabled(false);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    public void waitForCreation(){
        while (!isVisible());
    }

    public void initialize(final int size){
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialog.setTitle(R.string.database_progress_dialog_update);
                progressBar.setProgress(0);
                progressBar.setMax(size);
                progressBar.setText(String.format(Locale.US, getString(R.string.out_of_rows), 0, size));
            }
        });
    }

    public void increment(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressBar.incrementProgressBy(1);
                progressBar.setText(String.format(Locale.US, getString(R.string.out_of_rows), progressBar.getProgress(), progressBar.getMax()));
            }
        });
    }

    public void finish(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialog.setTitle(R.string.database_progress_dialog_finish);
                closeButton.setAlpha(1.0f);
                closeButton.setEnabled(true);
            }
        });
    }
}
