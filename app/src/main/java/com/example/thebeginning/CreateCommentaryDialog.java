package com.example.thebeginning;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Константин on 03.05.2016.
 */
public class CreateCommentaryDialog extends DialogFragment {
    private FileManager fileManager;
    private String name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
        name = bundle.getString(ContentManager.listItemNameKey);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_commentary_layout, null);
        final EditText editText = view.findViewById(R.id.create_commentary_edit_text);
        final MainActivity mainActivity = (MainActivity) getActivity();
        Button createButton = view.findViewById(R.id.commentary_create);
        Button cancelButton = view.findViewById(R.id.commentary_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Dialog dialog = getDialog();
        final CommentaryData commentaryData = new CommentaryData();
        if(name == null){
            dialog.setTitle(R.string.create_comment);
            createButton.setText(R.string.create);
            createButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    commentaryData.setText(editText.getText().toString());
                    String newName = String.valueOf(fileManager.getFilesCount()) + ", " + new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime()) + ".txt";
                    fileManager.writeToFile(newName, commentaryData);
                    mainActivity.updateContent(newName, null);
                    String name = fileManager.getCurrentFolderName();
                    fileManager.goBack();
                    HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
                    hashMapManager.getHashMap();
                    hashMapManager.put(fileManager.getChildLocalPath(name), false);
                    hashMapManager.saveChanges();
                    fileManager.goAhead(name);
                    dismiss();
                }
            });
        }
        else{
            int dotPosition = name.lastIndexOf(".");
            if(name.substring(dotPosition + 1, name.length()).equals("txt")){
                dialog.setTitle(R.string.edit_comment);
                fileManager.readFromFile(name, commentaryData);
                editText.setText(commentaryData.getText());
                createButton.setText(R.string.edit);
                createButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        commentaryData.setText(editText.getText().toString());
                        fileManager.writeToFile(name, commentaryData);
                        dismiss();
                    }
                });
            }
            else{
                name = name.substring(0, dotPosition);
                name += ".txt";
                if(fileManager.exists(name)){
                    dialog.setTitle(R.string.edit_comment);
                    fileManager.readFromFile(name, commentaryData);
                    editText.setText(commentaryData.getText());
                    createButton.setText(R.string.edit);
                    createButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            commentaryData.setText(editText.getText().toString());
                            fileManager.writeToFile(name, commentaryData);
                            dismiss();
                        }
                    });
                }
                else{
                    dialog.setTitle(R.string.create_comment);
                    createButton.setText(R.string.create);
                    createButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            commentaryData.setText(editText.getText().toString());
                            fileManager.writeToFile(name, commentaryData);
                            mainActivity.updateContent(name, null);
                            String name = fileManager.getCurrentFolderName();
                            fileManager.goBack();
                            HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
                            hashMapManager.getHashMap();
                            hashMapManager.put(fileManager.getChildLocalPath(name), false);
                            hashMapManager.saveChanges();
                            fileManager.goAhead(name);
                            dismiss();
                        }
                    });
                }
            }
        }
        return view;
    }
}
