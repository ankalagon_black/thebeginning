package com.example.thebeginning;

import android.app.Activity;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 * Created by Константин on 05.04.2016.
 */
public class SmbFileManager {

    private String root, localPath;
    private NtlmPasswordAuthentication authentication;

    public SmbFileManager(String root, String localPath, NtlmPasswordAuthentication authentication){
        this.root = root;
        this.localPath = localPath;
        this.authentication = authentication;
    }

    public void goAhead(String name){
        localPath = getChildLocalPath(name);
    }

    public void goTo(String localPath){
        this.localPath = localPath;
    }

    public void goBack(){
        localPath = getParentLocalPath();
    }

    public void goBack(int times){
        for(int i = 0; i < times; i++) goBack();
    }

    public String getChildLocalPath(String name){
        return localPath + "/" + name;
    }

    public String getParentLocalPath(){
        return localPath.substring(0, localPath.lastIndexOf("/"));
    }

    public String getFullPath(){
        return root + localPath;
    }

    public String getChildFullPath(String name){
        return root + getChildLocalPath(name);
    }


    public boolean createFolder(){
        try {
            SmbFile smbFile = new SmbFile(getFullPath(), authentication);
            if (!smbFile.exists()) smbFile.mkdirs();
        } catch (MalformedURLException | SmbException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
            return false;
        }
        return true;
    }

    public boolean createFolder(String name){
        try{
            SmbFile smbFile = new SmbFile(getChildFullPath(name), authentication);
            if(!smbFile.exists()) smbFile.mkdirs();
        } catch (MalformedURLException | SmbException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
            return false;
        }
        return true;
    }

    public SmbFile[] getFiles() throws SmbException, MalformedURLException, UnknownHostException {
        SmbFile smbFile = new SmbFile(getFullPath() + "/", authentication);
        return smbFile.listFiles();
    }

    public SmbFile getFile(String name){
        SmbFile smbFile = null;
        try {
            smbFile =  new SmbFile(getFullPath() + "/" + name, authentication);
        } catch (MalformedURLException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
        return smbFile;
    }

    public String prepareXml(FileManager fileManager, String name, Activity activity){
        String creationDate = activity.getString(R.string.date_of_creation),
                syncDate = activity.getString(R.string.sync_date), syncCatalog = activity.getString(R.string.sync_catalog);

        fileManager.goAhead(name);
        AbstractData abstractData = fileManager.readFromFile("mode.xml", new XmlData());
        String content = abstractData.getContent();
        String result = name;
        if(content != null) {
            int index = content.indexOf("<DivideByDates>") + 15;
            String divideByDates = content.substring(index, index + 1);
            if (divideByDates.equals("1")) {
                index = content.indexOf("<" + creationDate + ">") + 14;

                String dateOfCreationStr = content.substring(index, index + 16);
                try {
                    Date dateOfCreation = new SimpleDateFormat("dd-MM-yyyy HH-mm").parse(dateOfCreationStr);
                    String formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateOfCreation);
                    result = formattedDate + "/" + result;
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            String currentDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(Calendar.getInstance().getTime());

            StringBuilder stringBuilder = new StringBuilder(content);
            if(content.contains(syncDate)){
                stringBuilder.replace(stringBuilder.indexOf("<" + syncDate + ">") + 19, stringBuilder.indexOf("</" + syncDate + ">"), currentDate);
                stringBuilder.replace(stringBuilder.indexOf("<" + syncCatalog + ">") + 22, stringBuilder.indexOf("</" + syncCatalog + ">"), result);
            }
            else{
                index = stringBuilder.indexOf("</SystemData>") - 1;
                stringBuilder.insert(index, "<" + syncDate + ">" + currentDate + "</" + syncDate + ">" + "\n" +
                        "<" + syncCatalog + ">" + result + "</" + syncCatalog + ">" + "\n");
            }

            abstractData.setContent(stringBuilder.toString());
            fileManager.writeToFile("mode.xml", abstractData);
            if (content.contains("<RewriteCatalogMode>")) {
                index = content.indexOf("RewriteCatalogMode") + 19;
                String rewrite = content.substring(index, index + 1);
                if(rewrite.equals("1")) delete(result);
            }
        }
        fileManager.goBack();
        return result;
    }


//    Calendar calendar = Calendar.getInstance();
//
//    int index = content.indexOf("<DivideByDates>") + 15;
//    String divideByDates = content.substring(index, index + 1);
//            if (divideByDates.equals("1")) {
//        index = content.indexOf("<TimeOfShift>") + 13;
//        String timeOfShift = content.substring(index, index + 5);
//        int hours = calendar.get(Calendar.HOUR_OF_DAY), minutes = calendar.get(Calendar.MINUTE);
//        int shiftHours = Integer.parseInt(timeOfShift.substring(0, 2)), shiftMinutes = Integer.parseInt(timeOfShift.substring(3, 5));
//        String dateCopy = strDate;
//        if(shiftHours > hours || (shiftHours == hours && shiftMinutes > minutes)) {
//            calendar.add(Calendar.DATE, -1);
//            dateCopy = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(calendar.getTime());
//        }
//        String datePath = dateCopy.substring(0, dateCopy.lastIndexOf(" "));
//        int firstIndex = datePath.indexOf("-") + 1, lastIndex = datePath.lastIndexOf("-") + 1;
//        result = datePath.substring(lastIndex, datePath.length())  + "/" + datePath.substring(firstIndex, lastIndex - 1) + "/" + datePath.substring(0, firstIndex - 1) + "/" + result;
//    }
//
//    String currentDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(calendar.getTime());
//
//    StringBuilder stringBuilder = new StringBuilder(content);
//            if(content.contains("ДатаСинхронизации")){
//        stringBuilder.replace(stringBuilder.indexOf("<ДатаСинхронизации>") + 19, stringBuilder.indexOf("</ДатаСинхронизации>"), currentDate);
//        stringBuilder.replace(stringBuilder.indexOf("<КаталогСинхронизации>") + 22, stringBuilder.indexOf("</КаталогСинхронизации>"), result);
//    }
//            else{
//        index = stringBuilder.indexOf("</SystemData>") - 1;
//        stringBuilder.insert(index, "<ДатаСинхронизации>" + currentDate + "</ДатаСинхронизации>" + "\n" + "<КаталогСинхронизации>" + result + "</КаталогСинхронизации>" + "\n");
//    }
//
//            abstractData.setContent(stringBuilder.toString());
//            fileManager.writeToFile("mode.xml", abstractData);
//            if (content.contains("<RewriteCatalogMode>")) {
//        index = content.indexOf("RewriteCatalogMode") + 19;
//        String rewrite = content.substring(index, index + 1);
//        if(rewrite.equals("1")) delete(result);
//    }

    public void delete(String name){
        try{
            SmbFile smbFile = new SmbFile(getChildFullPath(name + "/"), authentication);
            smbFile.delete();
        } catch (MalformedURLException | SmbException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }

    public void writeFile(final FileManager fileManager, final File file, final ProgressBarDialog progressBarDialog){
        final ExtensionManager extensionManager = new ExtensionManager(file.getName());
        extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
            @Override
            public void onPhotoDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoData());
                byte[] jpegBytes = data.getJpegBytes();
                progressBarDialog.initializeByteBar(jpegBytes.length / 1024);
                writeBytes(extensionManager.getNameWithoutExtension(), ".jpeg", 8192, jpegBytes, progressBarDialog);
            }

            @Override
            public void onPhotoBarcodeDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoBarcodeData());
                String nameWithoutExtension = extensionManager.getNameWithoutExtension();
                byte[] jpegBytes = data.getJpegBytes(), barcodeBytes = data.getBarcodeBytes();
                progressBarDialog.initializeByteBar((jpegBytes.length + barcodeBytes.length) / 1024);
                writeBytes(nameWithoutExtension, ".jpeg", 8192, jpegBytes, progressBarDialog);
                writeBytes(nameWithoutExtension, ".tbf", 1024, barcodeBytes, progressBarDialog);
            }

            @Override
            public void onStringListDataEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".tbf", progressBarDialog);
            }

            @Override
            public void onHashMapDataEvent() {
            }

            @Override
            public void onFolderEvent() {

            }

            @Override
            public void onXmlDataEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".xml", progressBarDialog);
            }

            @Override
            public void on3gpEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeBytes(extensionManager.getNameWithoutExtension(), ".mp4", 8192, file, progressBarDialog);
            }

            @Override
            public void onTxtEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".txt", progressBarDialog);
            }
        });
        extensionManager.run();
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, byte[] bytes, ProgressBarDialog progressBarDialog){
        try{
            SmbFile smbFile = new SmbFile(getChildFullPath(nameWithoutExtension + extensionWithDot), authentication);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            SmbFileOutputStream outputStream = new SmbFileOutputStream(smbFile);
            int bytesRead;
            int incrementValue = volume / 1024;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(incrementValue);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, File file, ProgressBarDialog progressBarDialog){
        try{
            InputStream inputStream = new FileInputStream(file);
            SmbFile smbFile = new SmbFile(getChildFullPath(nameWithoutExtension + extensionWithDot), authentication);
            SmbFileOutputStream outputStream = new SmbFileOutputStream(smbFile);
            int bytesRead;
            int incrementValue = volume / 1024;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(incrementValue);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }


    private void writeFile(String nameWithoutExtension, File file, String extension, ProgressBarDialog progressBarDialog){
        try{
            SmbFile smbFile = new SmbFile(getChildFullPath(nameWithoutExtension + extension), authentication);
            FileInputStream inputStream = new FileInputStream(file);
            SmbFileOutputStream outputStream = new SmbFileOutputStream(smbFile);
            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(1);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }
}
