package com.example.thebeginning;

import java.util.HashMap;

/**
 * Created by Константин on 07.04.2016.
 */
public class HashMapManager {

    private static String name = "HashMap.HashMapData";
    private FileManager fileManager;
    private HashMapData hashMapData;

    public HashMapManager(FileManager fileManager){
        this.fileManager = fileManager;
    }

    public void getHashMap(){
        hashMapData = (HashMapData) fileManager.readFromFile(name, new HashMapData());
        if(hashMapData.isHashMapNull()) hashMapData.setHashMap(new HashMap<String, Boolean>());
    }

    public void put(String key, Boolean value) {
        hashMapData.put(key, value);
    }

    public Boolean get(String key){
        Boolean result = hashMapData.get(key);
        if(result == null){
            put(key, false);
            return false;
        }
        return result;
    }

    public void remove(String key){
        hashMapData.remove(key);
    }

    public void saveChanges(){
        fileManager.writeToFile(name, hashMapData);
    }
}
