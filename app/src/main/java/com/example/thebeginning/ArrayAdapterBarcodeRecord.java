package com.example.thebeginning;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


/**
 * Created by apple on 10.08.16.
 */
public class ArrayAdapterBarcodeRecord extends ArrayAdapter<BarcodeRecord> {

    private ActivityCreateBacode activity;
    private FragmentCreateBarcode fragment;
    private DatabaseManager databaseManager;

    private List<BarcodeRecord> list;

    private List<String> infoFields = null;

    public ArrayAdapterBarcodeRecord(Context context, int resource, FragmentCreateBarcode fragment) {
        super(context, resource);
        this.activity = (ActivityCreateBacode) context;
        this.fragment = fragment;
        databaseManager = fragment.getDatabaseManager();
        list = new ArrayList<>();
        Collections.reverse(list);
        Cursor cursor = databaseManager.getListInfo();
        if(cursor != null) {
            int size = cursor.getCount();
            if (size > 0) {
                infoFields = new ArrayList<>();
                cursor.moveToFirst();
                int index = cursor.getColumnIndex(DatabaseManager.TAG_FIELD);
                for (int i = 0; i < size; i++) {
                    infoFields.add(cursor.getString(index));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null || convertView.getClass().getName().equals("android.view.View")){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.create_barcode_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.index = convertView.findViewById(R.id.create_barcode_index);
            viewHolder.quantity = convertView.findViewById(R.id.create_barcode_list_item_quantity);
            viewHolder.dbQuantity = convertView.findViewById(R.id.create_barcode_list_item_db_quantity);
            viewHolder.value = convertView.findViewById(R.id.create_barcode_list_item_barcode);
            viewHolder.listInfo = convertView.findViewById(R.id.create_barcode_list_item_list_info);
            viewHolder.search = convertView.findViewById(R.id.create_barcode_list_item_search);
            viewHolder.choice = convertView.findViewById(R.id.create_barcode_list_item_choice);
            convertView.setTag(viewHolder);
        }

        final BarcodeRecord record = getItem(position);
        int color = record.getColor();

        if(fragment.getAdapterId() == 1){
            //данные бд
            final String strValue = record.getValue();
            if(!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
                if(record.getDbQuantity() < 0){
                    convertView.setBackgroundColor(getColor(5));
                }
                else convertView.setBackgroundColor(getColor(color));
            }
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.index.setText(String.format(activity.getString(R.string.create_barcode_list_item_index), position + 1));
            viewHolder.value.setText(strValue);
            viewHolder.quantity.setText(String.format(Locale.US, "%.2f", record.getQuantity()));
            viewHolder.dbQuantity.setText(String.format(Locale.US, "%.2f",record.getDbQuantity()));
            viewHolder.listInfo.setText(getInfoString(String.valueOf(record.getId()), true));
            viewHolder.search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(activity.isSearchAvailable()){
                        DialogDisplayDBQuery dialog = new DialogDisplayDBQuery();
                        Bundle bundle = new Bundle();
                        bundle.putString(DialogDisplayDBQuery.SEARCH_VALUE_KEY ,String.valueOf(record.getId()));
                        bundle.putParcelable(DialogDisplayDBQuery.FILE_MANAGER_KEY, activity.getFileManager());
                        bundle.putBoolean(DialogDisplayDBQuery.ID_SEARCH_KEY, true);
                        bundle.putString(DialogDisplayDBQuery.FILE_NAME_KEY, databaseManager.getFileName());
                        dialog.setArguments(bundle);
                        dialog.show(activity.getFragmentManager(), "1");
                    }
                }
            });
            viewHolder.choice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    //view configuration
                    View view = LayoutInflater.from(activity).inflate(R.layout.create_barcode_list_item_choice, null);
                    ListView listView = view.findViewById(R.id.create_barcode_list_item_choice_list_view);
                    ArrayAdapter arrayAdapter = new ArrayAdapterChoiceRecord(activity, R.layout.create_barcode_list_item_choice,
                            new String[] {
                                    getContext().getString(R.string.move_to_data_file)
                            },
                            new Integer[] {
                                    R.drawable.ic_action_arrow_left
                            });
                    listView.setAdapter(arrayAdapter);
                    builder.setTitle(R.string.choose_an_operation);
                    //
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            switch (position){
                                case 0:
                                    showEditBarcodeRecordDialog(record, false);
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
                    dialog.show();
                }
            });
        }
        else {
            //данные файла инвентаризации
            if(color == -1 || fragment.getAllowedColors().contains(color)){
                final String strValue = record.getValue();
                ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.index.setText(String.format(activity.getString(R.string.create_barcode_list_item_index), position + 1));
                viewHolder.value.setText(strValue);
                viewHolder.quantity.setText(String.format(Locale.US, "%.2f", record.getQuantity()));
                viewHolder.dbQuantity.setText(String.format(Locale.US, "%.2f",record.getDbQuantity()));
                if(record.getSome()){
                    viewHolder.search.setImageResource(R.drawable.ic_action_search_violet);
                }
                else {
                    viewHolder.search.setImageResource(R.drawable.ic_action_search);
                }
                viewHolder.listInfo.setText(getInfoString(strValue, false));
                viewHolder.search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(activity.isSearchAvailable()){
                            DialogDisplayDBQuery dialog = new DialogDisplayDBQuery();
                            Bundle bundle = new Bundle();
                            bundle.putString(DialogDisplayDBQuery.SEARCH_VALUE_KEY ,strValue);
                            bundle.putParcelable(DialogDisplayDBQuery.FILE_MANAGER_KEY, activity.getFileManager());
                            bundle.putBoolean(DialogDisplayDBQuery.ID_SEARCH_KEY, false);
                            bundle.putString(DialogDisplayDBQuery.FILE_NAME_KEY, databaseManager.getFileName());
                            dialog.setArguments(bundle);
                            dialog.show(activity.getFragmentManager(), "1");
                        }
                    }
                });
                viewHolder.choice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        //view configuration
                        View view = LayoutInflater.from(activity).inflate(R.layout.create_barcode_list_item_choice, null);
                        ListView listView = view.findViewById(R.id.create_barcode_list_item_choice_list_view);
                        ArrayAdapter arrayAdapter = new ArrayAdapterChoiceRecord(activity, R.layout.create_barcode_list_item_choice,
                                new String[] {
                                        getContext().getString(R.string.action_delete),
                                        "--------------------------",
                                        getContext().getString(R.string.edit),
                                        "--------------------------",
                                        getContext().getString(R.string.search_by_match),
                                        getContext().getString(R.string.search_occurrence)
                                },
                                new Integer[] {
                                        R.drawable.ic_action_cancel,
                                        null,
                                        R.drawable.ic_action_document,
                                        null,
                                        R.drawable.ic_action_search,
                                        R.drawable.ic_action_search
                                });
                        listView.setAdapter(arrayAdapter);
                        builder.setTitle(R.string.choose_an_operation);
                        //
                        builder.setView(view);
                        final AlertDialog dialog = builder.create();
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                switch (position){
                                    case 0:
                                        fragment.delete(record);
                                        dialog.dismiss();
                                        break;
                                    case 2:
                                        showEditBarcodeRecordDialog(record, true);
                                        dialog.dismiss();
                                        break;
                                    case 4:
                                        showFindBarcodeRecordDialog(record, true);
                                        dialog.dismiss();
                                        break;
                                    case 5:
                                        showFindBarcodeRecordDialog(record, false);
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });
                        dialog.show();
                    }
                });
                if(color != -1) convertView.setBackgroundColor(getColor(color));
            }
            else return new View(activity);
        }

        return convertView;
    }

    public ActivityCreateBacode getActivity(){
        return activity;
    }

    private void showEditBarcodeRecordDialog(final BarcodeRecord barcodeRecord, final boolean dataFile){
        AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getContext());
        builder.setTitle(R.string.input_new_value);

        LayoutInflater layoutInflater = LayoutInflater.from(fragment.getContext());
        View view =  layoutInflater.inflate(R.layout.edit_barcode_record_dialog, null);
        final EditText barcode = view.findViewById(R.id.create_barcode_dialog_barcode),
                quantity = view.findViewById(R.id.create_barcode_dialog_number);

        barcode.setText(barcodeRecord.getValue());
        barcode.setEnabled(false);
        quantity.setText(String.format(Locale.US, "%.3f",barcodeRecord.getQuantity()));

        builder.setView(view);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                float adjustment;
                if(dataFile) adjustment = Float.parseFloat(quantity.getText().toString()) - barcodeRecord.getQuantity();
                else adjustment = Float.parseFloat(quantity.getText().toString());
                fragment.add(barcodeRecord.getValue(), String.valueOf(adjustment), false);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showFindBarcodeRecordDialog(final BarcodeRecord barcodeRecord, boolean state){

        if(!activity.isColorIdentificationAvailable()){
            Toast.makeText(activity, R.string.action_is_disabled_by_settings, Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getContext());
        builder.setTitle(R.string.choose_an_element_from_list);

        LayoutInflater layoutInflater = LayoutInflater.from(fragment.getContext());
        View view =  layoutInflater.inflate(R.layout.find_barcode_dialog, null);

        ListView listView = view.findViewById(R.id.find_barcode_dialog_list_view);
        List<BarcodeRecord> list = new ArrayList<>();
        if(barcodeRecord.getValue().length() > 3){
            list = databaseManager.searchFor(barcodeRecord.getValue(), state);
        }
        else{
            Toast.makeText(activity, R.string.search_word_must_be_at_least_three_characters_long, Toast.LENGTH_SHORT).show();
        }
        final ArrayAdapterFindBarcode adapterFindBarcode = new ArrayAdapterFindBarcode(getContext(), R.layout.find_barcode_dialog, list
                , this, barcodeRecord);
        listView.setAdapter(adapterFindBarcode);

        final TextView noResult = view.findViewById(R.id.find_barcode_dialog_no_results);

        if(adapterFindBarcode.getCount() == 0){
            noResult.setVisibility(View.VISIBLE);
        }

        final TextView textView = view.findViewById(R.id.find_barcode_dialog_title);

        EditText editText = view.findViewById(R.id.find_barcode_dialog_edit_text);
        editText.setText(barcodeRecord.getValue());
        Switch aSwitch = view.findViewById(R.id.find_barcode_dialog_switch);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                List<BarcodeRecord> list = new ArrayList<>();
                if(barcodeRecord.getValue().length() > 3){
                    list = databaseManager.searchFor(barcodeRecord.getValue(), isChecked);
                }
                else{
                    Toast.makeText(activity, R.string.search_word_must_be_at_least_three_characters_long, Toast.LENGTH_SHORT).show();
                }
                adapterFindBarcode.setRecords(list);
                if(isChecked) textView.setText(R.string.search_by_match);
                else textView.setText(R.string.search_occurrence);
                if(adapterFindBarcode.getCount() == 0){
                    noResult.setVisibility(View.VISIBLE);
                }
                else if(noResult.getVisibility() == View.VISIBLE){
                    noResult.setVisibility(View.GONE);
                }
            }
        });


        aSwitch.setChecked(state);
        if(state) textView.setText(R.string.search_by_match);
        else textView.setText(R.string.search_occurrence);

        builder.setView(view);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        adapterFindBarcode.setDialog(dialog);
        dialog.show();
        textView.post(new Runnable() {
            @Override
            public void run() {
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            }
        });
    }

    private int getColor(int color){
        switch (color) {
            case 0:
                return Color.rgb(28, 250, 10);
            case 1:
                return Color.rgb(17, 252, 233);
            case 2:
                return Color.rgb(245, 69, 69);
            case 3:
                return Color.rgb(165, 31, 31);
            case 4:
                return Color.rgb(249, 253, 63);
            default:
                return Color.rgb(255, 128, 0);
        }
    }

    public FragmentCreateBarcode getFragment(){
        return fragment;
    }

    public String getInfoString(String value, boolean idSearch){
        if(activity.isUpdating()) return "-";
        if(idSearch){
            if(infoFields != null){
                Cursor cursor = databaseManager.getRecordWithId(Integer.parseInt(value));
                cursor.moveToFirst();
                String result = "";
                for(String i: infoFields){
                    int index = cursor.getColumnIndex(i);
                    result += cursor.getString(index) + " ";
                }
                cursor.close();
                return result;
            }
        }
        else {
            if(infoFields != null){
                Cursor cursor = databaseManager.search(value,false);
                if(cursor.getCount() == 0) return "-";
                cursor.moveToFirst();
                String result = "";
                for(String i: infoFields){
                    int index = cursor.getColumnIndex(i);
                    result += cursor.getString(index) + " ";
                }
                cursor.close();
                return result;
            }
        }
        return "-";
    }

    public void insert(BarcodeRecord barcodeRecord){
        insert(barcodeRecord, 0);
        list.add(0, barcodeRecord);
    }

    @Override
    public void add(BarcodeRecord object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public void remove(BarcodeRecord object) {
        super.remove(object);
        list.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
        list.clear();
    }

    public List<BarcodeRecord> getAll(){
        return list;
    }

    private static class ViewHolder {
        public TextView index;
        public TextView value;
        public TextView quantity;
        public TextView dbQuantity;
        public TextView listInfo;
        public ImageButton search;
        public ImageButton choice;
    }
}
