package com.example.thebeginning;

/**
 * Created by apple on 10.08.16.
 */
public class BarcodeRecord {

    private int id;
    private String value;
    private float quantity;
    private float dbQuantity = 0;
    private boolean some = false;
    private int color;

    public BarcodeRecord(String value, float quantity){
        this.value = value;
        this.quantity = quantity;
        this.color = -1;
    }

    public BarcodeRecord(int id, String value, float dbQuantity, float quantity, int color){
        this.id = id;
        this.value = value;
        this.quantity = quantity;
        this.dbQuantity = dbQuantity;
        this.color = color;
    }

    public BarcodeRecord(BarcodeRecord record){
        id = record.getId();
        value = record.getValue();
        quantity = record.getQuantity();
        color = record.getColor();
    }

    public void addQuantity(float quantity){
        this.quantity += quantity;
    }

    public float getQuantity(){
        return quantity;
    }

    public void setDbQuantity(float dbQuantity){
        this.dbQuantity = dbQuantity;
    }

    public float getDbQuantity(){
        return dbQuantity;
    }

    public void setColor(int color){
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setValue(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String mergeData(){
        return value + "," + Float.toString(quantity);
    }

    public void setSome(boolean some){
        this.some = some;
    }

    public boolean getSome(){
        return some;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
}
