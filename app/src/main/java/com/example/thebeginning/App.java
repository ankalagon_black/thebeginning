package com.example.thebeginning;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.example.thebeginning.data_types.GlobalSettings;

import java.util.Locale;

/**
 * Created by Константин on 20.01.2018.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        setLocale();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocale();
    }

    public String getCurrentLanguage(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);;
        return sharedPreferences.getString(getString(R.string.language), getString(R.string.language_default));
    }

    public void setLocale() {
        final Resources resources = getResources();
        final Configuration configuration = resources.getConfiguration();
        final Locale locale = GlobalSettings.getLocale(getCurrentLanguage());
        if (!configuration.locale.equals(locale)) {
            configuration.setLocale(locale);
            resources.updateConfiguration(configuration, null);
        }
    }

//    public void updateLanguageConfiguration(){
//        if(MainActivity.globalSettings.getLanguages() != null) {
//            Configuration config = getApplication().getResources().getConfiguration();
//            config.locale = GlobalSettings.getLocale(getCurrentLanguage());
//            getApplication().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//        }
//    }

//    Configuration config = getBaseContext().getResources().getConfiguration();
//    locale = new Locale(langval);
//    Locale.setDefault(locale);
//    config.locale = locale;
//    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//    recreate();
}
