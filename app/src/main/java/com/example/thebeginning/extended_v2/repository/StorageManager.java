package com.example.thebeginning.extended_v2.repository;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Build;
import android.os.Environment;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;

import com.example.thebeginning.R;
import com.example.thebeginning.extended_v2.data_types.Storage;
import com.github.mjdev.libaums.UsbMassStorageDevice;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Константин on 01.09.2017.
 */

public class StorageManager {

    public static List<Storage> getAllStorages(Context context){
        List<Storage> storages = new ArrayList<>();

        storages.addAll(getExternalStorages(context));
        storages.addAll(getUSBStorages(context));

        return storages;
    }

    public static List<Storage> getExternalStorages(Context context){
        final List<Storage> storages = new ArrayList<>();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            File[] externalDirs = context.getExternalFilesDirs(null);

            for (File i: externalDirs){
                if(i != null) {
                    String path = i.getPath();

                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) path = path.split("/Android")[0];

                    boolean isRemovable;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        isRemovable = Environment.isExternalStorageRemovable(i);
                    }
                    else{
                        isRemovable = Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(i));
                    }

                    if(isRemovable) storages.add(new Storage(Storage.SD_CARD, path));
                    else storages.add(new Storage(Storage.DEVICE_MEMORY, path));
                }
            }
        }
        else {
            List<String> directories = new ArrayList<>();

            final String rawSecondaryStorageStr = System.getenv("SECONDARY_STORAGE");
            if(!TextUtils.isEmpty(rawSecondaryStorageStr)){
                final String[] rawSecondaryStorages = rawSecondaryStorageStr.split(File.pathSeparator);
                Collections.addAll(directories, rawSecondaryStorages);
            }

            String externalStorage = System.getenv("EXTERNAL_STORAGE");
            if(!TextUtils.isEmpty(externalStorage)) directories.add(externalStorage);

            for (String i: directories){
                File file = new File(i);
                if(Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(file))) storages.add(new Storage(Storage.SD_CARD, i));
                else storages.add(new Storage(Storage.DEVICE_MEMORY, i));
            }
        }

        return storages;
    }

    public static List<Storage> getUSBStorages(Context context){
        List<Storage> storages = new ArrayList<>();

        UsbMassStorageDevice[] devices = UsbMassStorageDevice.getMassStorageDevices(context);

        for (UsbMassStorageDevice i: devices){
            UsbDevice usbDevice = i.getUsbDevice();
            storages.add(new Storage(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ?
                    usbDevice.getManufacturerName() + " " + usbDevice.getProductName() :
                    usbDevice.getDeviceName(),
                    context.getString(R.string.usb_device_root), usbDevice.getDeviceId()));
        }

        return storages;
    }
}
