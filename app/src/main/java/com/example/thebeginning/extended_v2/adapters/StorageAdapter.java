package com.example.thebeginning.extended_v2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thebeginning.R;
import com.example.thebeginning.extended_v2.data_types.Storage;
import com.example.thebeginning.extended_v2.repository.StorageManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 01.09.2017.
 */

public class StorageAdapter extends RecyclerView.Adapter<StorageAdapter.Holder> {
    private StorageChosen storageChosen;

    private List<Storage> storages = new ArrayList<>();

    public StorageAdapter(StorageChosen storageChosen){
        this.storageChosen = storageChosen;
    }

    public void getStorages(Context context){
        storages = StorageManager.getAllStorages(context);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.storage, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Storage storage = storages.get(position);
        holder.setStorage(storage);
    }

    @Override
    public int getItemCount() {
        return storages.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        private TextView type, path;

        public Holder(View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.type);
            path = itemView.findViewById(R.id.path);
        }

        public void setStorage(final Storage storage){
            Context context = itemView.getContext();
            switch (storage.getType()){
                case Storage.DEVICE_MEMORY:
                    this.type.setText(context.getString(R.string.device_memory));
                    break;
                case Storage.SD_CARD:
                    this.type.setText(context.getString(R.string.sd_card));
                    break;
                case Storage.USB:
                    this.type.setText(String.format(Locale.US, context.getString(R.string.usb), storage.getStorageName()));
                    break;
            }

            this.path.setText(storage.getPath());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    storageChosen.onStorageChosen(storage);
                }
            });
        }
    }

    public interface StorageChosen{
        void onStorageChosen(Storage storage);
    }
}
