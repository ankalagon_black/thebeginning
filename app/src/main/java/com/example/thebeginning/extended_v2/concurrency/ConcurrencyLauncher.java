package com.example.thebeginning.extended_v2.concurrency;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 25.08.2017.
 */

public interface ConcurrencyLauncher {
    void launch(DisposableCompletableObserver observer, Completable completable);
    <T> void launch(DisposableSingleObserver<T> observer, Single<T> single);
    <T> void launch(DisposableObserver<T> observer, Observable<T> observable);
}
