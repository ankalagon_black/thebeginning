package com.example.thebeginning.extended_v2.domains;

import com.example.thebeginning.PhotoBarcodeData;
import com.example.thebeginning.PhotoData;
import com.example.thebeginning.StringListData;

import java.io.File;
import java.io.IOException;

/**
 * Created by Константин on 02.09.2017.
 */

public class GetFileType {
    private String extension, nameWithoutExtension;

    public GetFileType(File file){
        String name = file.getName();
        int position = name.lastIndexOf(".");
        if(position != -1){
            extension = name.substring(position + 1, name.length());
            nameWithoutExtension = name.substring(0, name.lastIndexOf("."));
        }
        else {
            extension = "";
            nameWithoutExtension = "";
        }
    }

    public void getType(OnFileTypeDetermined onFileTypeDetermined) throws IOException {
        if(extension.equals(PhotoData.class.getSimpleName())) onFileTypeDetermined.onPhotoData(nameWithoutExtension);
        else if(extension.equals(PhotoBarcodeData.class.getSimpleName())) onFileTypeDetermined.onPhotoBarcodeData(nameWithoutExtension);
        else if(extension.equals(StringListData.class.getSimpleName())) onFileTypeDetermined.onStringListData(nameWithoutExtension);
        else if(extension.equals("xml")) onFileTypeDetermined.onXmlData(nameWithoutExtension);
        else if(extension.equals("3gp")) onFileTypeDetermined.on3gp(nameWithoutExtension);
        else if(extension.equals("txt")) onFileTypeDetermined.onTxt(nameWithoutExtension);
    }

    public interface OnFileTypeDetermined{
        void onPhotoData(String nameWithoutExtension) throws IOException;
        void onPhotoBarcodeData(String nameWithoutExtension) throws IOException;
        void onStringListData(String nameWithoutExtension) throws IOException;
        void onXmlData(String nameWithoutExtension) throws IOException;
        void on3gp(String nameWithoutExtension) throws IOException;
        void onTxt(String nameWithoutExtension) throws IOException;
    }
}
