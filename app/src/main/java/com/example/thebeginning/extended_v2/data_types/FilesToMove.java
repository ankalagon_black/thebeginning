package com.example.thebeginning.extended_v2.data_types;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Константин on 02.09.2017.
 */

public class FilesToMove implements Parcelable{
    private String root;
    private String firstLevelCatalog;
    private ArrayList<String> secondLevelCatalogs;

    public FilesToMove(String root, String firstLevelCatalog, ArrayList<String> secondLevelCatalogs){
        this.root = root;
        this.firstLevelCatalog = firstLevelCatalog;
        this.secondLevelCatalogs = secondLevelCatalogs;
    }

    protected FilesToMove(Parcel in) {
        root = in.readString();
        firstLevelCatalog = in.readString();
        secondLevelCatalogs = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(root);
        dest.writeString(firstLevelCatalog);
        dest.writeStringList(secondLevelCatalogs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilesToMove> CREATOR = new Creator<FilesToMove>() {
        @Override
        public FilesToMove createFromParcel(Parcel in) {
            return new FilesToMove(in);
        }

        @Override
        public FilesToMove[] newArray(int size) {
            return new FilesToMove[size];
        }
    };

    public String getFirstLevelCatalog() {
        return firstLevelCatalog;
    }

    public ArrayList<String> getSecondLevelCatalogs() {
        return secondLevelCatalogs;
    }

    public String getRoot() {
        return root;
    }
}
