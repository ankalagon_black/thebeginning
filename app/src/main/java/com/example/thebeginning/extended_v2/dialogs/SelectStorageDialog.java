package com.example.thebeginning.extended_v2.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.example.thebeginning.R;
import com.example.thebeginning.extended_v2.adapters.StorageAdapter;
import com.example.thebeginning.extended_v2.data_types.FilesToMove;
import com.example.thebeginning.extended_v2.data_types.Storage;


/**
 * Created by Константин on 01.09.2017.
 */

public class SelectStorageDialog extends DialogFragment implements StorageAdapter.StorageChosen{
    public static final String FILES_TO_MOVE = "FilesToMove";

    private StorageAdapter storageAdapter;

    public static SelectStorageDialog getInstance(FilesToMove filesToMove){
        SelectStorageDialog dialog = new SelectStorageDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(FILES_TO_MOVE, filesToMove);

        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) LayoutInflater.from(getContext()).inflate(R.layout.recycler_view, null);

        storageAdapter = new StorageAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(storageAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.choose_storage).
                setView(recyclerView).
                setNegativeButton(R.string.cancel, null).create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        storageAdapter.getStorages(getContext());
    }

    @Override
    public void onStorageChosen(Storage storage) {
        FilesToMove filesToMove = getArguments().getParcelable(FILES_TO_MOVE);
        MoveFilesDialog moveFilesDialog = MoveFilesDialog.getInstance(filesToMove, storage);
        moveFilesDialog.show(getFragmentManager(), moveFilesDialog.getTag());

        dismiss();
    }
}
