package com.example.thebeginning.extended_v2.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.thebeginning.R;
import com.example.thebeginning.extended_v2.data_types.HttpsInfo;
import com.example.thebeginning.extended_v2.data_types.SmbInfo;

/**
 * Created by Константин on 03.09.2017.
 */

public class SharedPreferencesManager {
    private Context context;
    private SharedPreferences preferences;

    public SharedPreferencesManager(Context context){
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SmbInfo getSmbForwardInfo(){
        String ip = preferences.getString(context.getString(R.string.ip),
                context.getString(R.string.ip_default));
        String catalog = preferences.getString(context.getString(R.string.path_on_server),
                context.getString(R.string.path_on_server_default));
        String login = preferences.getString(context.getString(R.string.user_name),
                context.getString(R.string.user_name_default));
        String password = preferences.getString(context.getString(R.string.user_password),
                context.getString(R.string.user_password_default));

        return new SmbInfo(ip, catalog, login, password);
    }

    public SmbInfo getSmbBackwardInfo(){
        String ip = preferences.getString(context.getString(R.string.ip),
                context.getString(R.string.ip_default));
        String catalog = preferences.getString(context.getString(R.string.server_backwards_synchronization_path),
                context.getString(R.string.server_backwards_synchronization_path_default));
        String login = preferences.getString(context.getString(R.string.user_name),
                context.getString(R.string.user_name_default));
        String password = preferences.getString(context.getString(R.string.user_password),
                context.getString(R.string.user_password_default));

        return new SmbInfo(ip, catalog, login, password);
    }

    public HttpsInfo getSettingsHttpsInfo(){
        String ip = preferences.getString(context.getString(R.string.http_settings_ip_key), context.getString(R.string.http_settings_ip_default));
        String gateway = preferences.getString(context.getString(R.string.settings_gateway_key), context.getString(R.string.settings_gateway_default));
        String gatewayCode = preferences.getString(context.getString(R.string.settings_gateway_code_key), context.getString(R.string.settings_gateway_code_default));

        return new HttpsInfo(ip, gateway, gatewayCode);
    }

    public HttpsInfo getCommandsHttpsInfo(){
        String ip = preferences.getString(context.getString(R.string.http_commands_ip_key), context.getString(R.string.http_commands_ip_default));
        String gateway = preferences.getString(context.getString(R.string.commands_gateway_key), context.getString(R.string.commands_gateway_default));
        String gatewayCode = preferences.getString(context.getString(R.string.commands_gateway_code_key), context.getString(R.string.commands_gateway_code_default));

        return new HttpsInfo(ip, gateway, gatewayCode);
    }

    public String getDCTNumber(){
        return preferences.getString(context.getString(R.string.dct_number_key), context.getString(R.string.dct_number_default));
    }
}
