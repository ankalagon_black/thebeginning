package com.example.thebeginning.extended_v2.data_types;

/**
 * Created by Константин on 03.09.2017.
 */

public class HttpsInfo {
    private String ip;
    private String gateway;
    private String gatewayCode;

    public HttpsInfo(String ip, String gateway, String gatewayCode){
        this.ip = ip;
        this.gateway = gateway;
        this.gatewayCode = gatewayCode;
    }

    public String getIp() {
        return ip;
    }

    public String getGateway() {
        return gateway;
    }

    public String getGatewayCode() {
        return gatewayCode;
    }

    public String getHttpsUrl(){
        return "https://" + ip + "/" + gateway + "/";
    }
}
