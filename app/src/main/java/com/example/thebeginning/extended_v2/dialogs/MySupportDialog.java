package com.example.thebeginning.extended_v2.dialogs;

import android.support.v4.app.DialogFragment;

import com.example.thebeginning.extended_v2.concurrency.ConcurrencyLauncher;
import com.example.thebeginning.extended_v2.concurrency.ConcurrencyManager;

/**
 * Created by Константин on 01.09.2017.
 */

public class MySupportDialog extends DialogFragment {
    private ConcurrencyManager concurrencyManager = new ConcurrencyManager();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        concurrencyManager.disposeAllBackgroundTasks();
    }

    public ConcurrencyLauncher getConcurrencyLauncher() {
        return concurrencyManager;
    }
}
