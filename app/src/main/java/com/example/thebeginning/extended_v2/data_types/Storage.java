package com.example.thebeginning.extended_v2.data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Константин on 01.09.2017.
 */

public class Storage implements Parcelable {
    public static final int DEVICE_MEMORY = 0, SD_CARD = 1, USB = 2;

    private int type;
    private String storageName;
    private String path;
    private int deviceId;

    public Storage(int type, String path){
       this(type, null, path);
    }

    public Storage(String storageName, String path, int deviceId){
        this(USB, storageName, path);
        this.deviceId = deviceId;
    }

    protected Storage(int type, String storageName, String path){
        this.type = type;
        this.path = path;
        this.storageName = storageName;
    }

    protected Storage(Parcel in) {
        type = in.readInt();
        storageName = in.readString();
        path = in.readString();
        deviceId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeString(storageName);
        dest.writeString(path);
        dest.writeInt(deviceId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Storage> CREATOR = new Creator<Storage>() {
        @Override
        public Storage createFromParcel(Parcel in) {
            return new Storage(in);
        }

        @Override
        public Storage[] newArray(int size) {
            return new Storage[size];
        }
    };

    public int getType(){
        return type;
    }

    public String getStorageName() {
        return storageName;
    }

    public String getPath() {
        return path;
    }

    public int getDeviceId() {
        return deviceId;
    }
}
