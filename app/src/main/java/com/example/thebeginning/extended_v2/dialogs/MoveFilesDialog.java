package com.example.thebeginning.extended_v2.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.example.thebeginning.R;
import com.example.thebeginning.extended_v2.data_types.FilesToMove;
import com.example.thebeginning.extended_v2.data_types.Storage;
import com.example.thebeginning.extended_v2.domains.MoveFilesToStorage;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;

/**
 * Created by Константин on 01.09.2017.
 */

public class MoveFilesDialog extends MySupportDialog {
    public static final String FILES_TO_MOVE = "FilesToMove";
    public static final String STORAGE = "Storage";

    public static MoveFilesDialog getInstance(FilesToMove filesToMove, Storage storage){
        MoveFilesDialog moveFilesDialog = new MoveFilesDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(FILES_TO_MOVE, filesToMove);
        bundle.putParcelable(STORAGE, storage);

        moveFilesDialog.setArguments(bundle);
        return moveFilesDialog;
    }

    private Disposable disposable;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.progress_bar_default, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.files_movement_in_progress).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        disposable.dispose();
                        dismiss();
                    }
                }).
                create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                Toast.makeText(getContext(), R.string.files_moved_successfully, Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getContext(), R.string.during_files_movement_error_occured, Toast.LENGTH_SHORT).show();
                dismiss();
            }
        };

        disposable = observer;

        Bundle bundle = getArguments();

        final FilesToMove filesToMove = bundle.getParcelable(FILES_TO_MOVE);
        final Storage storage = bundle.getParcelable(STORAGE);

        getConcurrencyLauncher().launch(observer, Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                MoveFilesToStorage moveFilesToStorage = new MoveFilesToStorage(filesToMove);
                moveFilesToStorage.move(storage, getContext());
            }
        }));
    }
}
