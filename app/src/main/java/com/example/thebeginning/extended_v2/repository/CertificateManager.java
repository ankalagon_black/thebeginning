package com.example.thebeginning.extended_v2.repository;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Константин on 12.07.2017.
 */

public class CertificateManager {
//    private static final char[] PASSWORD = new char[] {'f', 'G', '1', 's', 'd', '7', 'L', 'A', '3'};
//
//    private PathManager pathManager;

//    public CertificateManager(PathManager pathManager){
//        this.pathManager = pathManager;
//    }

    //        KeyStore keyStore;
//
//        if(certificateManager.isCertificateCreated()) keyStore = certificateManager.getCertificate();
//        else keyStore = certificateManager.createCertificate(context);
//
//        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//        trustManagerFactory.init(keyStore);
//
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//        sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
//
//        return sslContext;

    public CertificateManager(){

    }

    public SSLContext getDefaultContext() throws KeyManagementException, NoSuchAlgorithmException {
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new X509TrustManager[]{new X509TrustManager(){
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {}
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {}
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }}}, new SecureRandom());

        return sslContext;
    }

//    public boolean isCertificateCreated(){
//        File file = new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE);
//        return file.exists();
//    }
//
//    public KeyStore createCertificate(Context context) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException {
//        CertificateFactory factory = CertificateFactory.getInstance("X.509");
//
//        Certificate certificate;
//        InputStream in = context.getResources().openRawResource(R.raw.certificate);
//        certificate = factory.generateCertificate(in);
//        in.close();
//
//        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//        keyStore.load(null, null);
//        keyStore.setCertificateEntry("certificate", certificate);
//
//        OutputStream out = new FileOutputStream(new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE));
//        keyStore.store(out, PASSWORD);
//        out.close();
//
//        return keyStore;
//    }
//
//    public KeyStore getCertificate() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
//        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//
//        InputStream inputStream = new FileInputStream(new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE));
//        keyStore.load(inputStream, PASSWORD);
//        inputStream.close();
//
//        return keyStore;
//    }
}
