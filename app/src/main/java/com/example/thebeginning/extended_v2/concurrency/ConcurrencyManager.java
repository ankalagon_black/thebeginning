package com.example.thebeginning.extended_v2.concurrency;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 25.08.2017.
 */

public class ConcurrencyManager implements ConcurrencyLauncher {
    private List<Disposable> disposables = new ArrayList<>();
    private boolean isClearing = false;

    @Override
    public void launch(final DisposableCompletableObserver observer, Completable completable) {
        completable.
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isClearing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);

        disposables.add(observer);
    }

    @Override
    public <T> void launch(final DisposableSingleObserver<T> observer, Single<T> single) {
        single.
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isClearing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);

        disposables.add(observer);
    }

    @Override
    public <T> void launch(final DisposableObserver<T> observer, Observable<T> observable) {
        observable.
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isClearing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);

        disposables.add(observer);
    }

    public void disposeAllBackgroundTasks(){
        isClearing = true;
        for (Disposable i: disposables){
            if(!i.isDisposed()) i.dispose();
        }

        disposables.clear();
    }
}
