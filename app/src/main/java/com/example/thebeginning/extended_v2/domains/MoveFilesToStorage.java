package com.example.thebeginning.extended_v2.domains;

import android.content.Context;

import com.example.thebeginning.AbstractData;
import com.example.thebeginning.PhotoBarcodeData;
import com.example.thebeginning.PhotoData;
import com.example.thebeginning.XmlData;
import com.example.thebeginning.extended_v2.data_types.FilesToMove;
import com.example.thebeginning.extended_v2.data_types.Storage;
import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;
import com.github.mjdev.libaums.fs.UsbFileStreamFactory;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Константин on 02.09.2017.
 */

public class MoveFilesToStorage {
    public static final String MOVE_FOLDER = "TheBeginning_Photo";

    private FilesToMove filesToMove;

    public MoveFilesToStorage(FilesToMove filesToMove){
       this.filesToMove = filesToMove;
    }

    public void move(Storage storage, Context context) throws Exception {
        if(storage.getType() == Storage.USB) moveToUSBStorage(storage.getDeviceId(), context);
        else moveToExternalStorage(storage.getPath());
    }

    private void moveToUSBStorage(int deviceId, Context context) throws Exception {
        UsbMassStorageDevice[] devices = UsbMassStorageDevice.getMassStorageDevices(context);
        for (UsbMassStorageDevice i: devices){
            if(i.getUsbDevice().getDeviceId() == deviceId){
                i.init();

                FileSystem fileSystem = i.getPartitions().get(0).getFileSystem();

                UsbFile rootDirectory = fileSystem.getRootDirectory();

                UsbFile destinationRoot = rootDirectory.search(MOVE_FOLDER);
                if(destinationRoot == null) destinationRoot = rootDirectory.createDirectory(MOVE_FOLDER);

                UsbFile moveFolder = destinationRoot.search(filesToMove.getFirstLevelCatalog());
                if(moveFolder == null) moveFolder = destinationRoot.createDirectory(filesToMove.getFirstLevelCatalog());

                File firstLevelDir = new File(filesToMove.getRoot(), filesToMove.getFirstLevelCatalog());

                List<String> secondLevelCatalogs = filesToMove.getSecondLevelCatalogs();
                for (String j: secondLevelCatalogs){
                    File sourceDir = new File(firstLevelDir, j);
                    copy(sourceDir, getDestinationDir(sourceDir, moveFolder), fileSystem);
                }

                i.close();
            }
        }
    }

    private AbstractData readFromFile(File file, AbstractData abstractData) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        abstractData.read(fileInputStream);
        fileInputStream.close();
        return abstractData;
    }

    private void moveToExternalStorage(String path) throws Exception {
        File destinationRoot = new File(new File(path, MOVE_FOLDER),
                filesToMove.getFirstLevelCatalog());
        destinationRoot.mkdirs();

        File firstLevelDir = new File(filesToMove.getRoot(), filesToMove.getFirstLevelCatalog());

        List<String> secondLevelCatalogs = filesToMove.getSecondLevelCatalogs();
        for (String i: secondLevelCatalogs){
            File sourceDir = new File(firstLevelDir, i);
            copy(sourceDir, getDestinationDir(sourceDir, destinationRoot));
        }
    }

    private File getDestinationDir(File sourceDir, File destinationRoot) throws Exception {
        String innerPath = getMovementInnerPath(sourceDir);

        if(innerPath == null) throw new Exception("Can't get destination directory");

        File destinationDir = new File(destinationRoot, innerPath);
        if(!destinationDir.exists()) destinationDir.mkdirs();

        return destinationDir;
    }

    private UsbFile getDestinationDir(File sourceDir, UsbFile destinationRoot) throws Exception {
        String innerPath = getMovementInnerPath(sourceDir);

        if(innerPath == null) throw new Exception("Can't get destination directory");

        String[] directoriesToCreate = innerPath.split("/");

        UsbFile destinationDir = destinationRoot;

        for (String i: directoriesToCreate){
            UsbFile temp = destinationDir.search(i);
            if(temp != null) destinationDir = temp;
            else destinationDir = destinationDir.createDirectory(i);
        }

        return destinationDir;
    }

    private String getMovementInnerPath(File sourceDir) throws ParseException, IOException {
        AbstractData abstractData = readFromFile(new File(sourceDir, "mode.xml"), new XmlData());

        String content = abstractData.getContent(), result = sourceDir.getName();

        if(content != null) {
            int index = content.indexOf("<DivideByDates>") + 15;
            String divideByDates = content.substring(index, index + 1);
            if (divideByDates.equals("1")) {
                index = content.indexOf("<ДатаСоздания>") + 14;

                String dateOfCreationStr = content.substring(index, index + 16);
                Date dateOfCreation = new SimpleDateFormat("dd-MM-yyyy HH-mm").parse(dateOfCreationStr);
                String formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateOfCreation);
                result = formattedDate + "/" + result;
            }
        }

        return result;
    }

    private void copy(File fromDir, final File toDir) throws IOException {
        File[] fromFiles = fromDir.listFiles();
        if(fromFiles != null){
            for (final File i: fromFiles){
                GetFileType getFileType = new GetFileType(i);

                getFileType.getType(new GetFileType.OnFileTypeDetermined() {
                    @Override
                    public void onPhotoData(String nameWithoutExtension) throws IOException {
                        AbstractData abstractData = readFromFile(i, new PhotoData());
                        byte[] jpegBytes = abstractData.getJpegBytes();
                        write(jpegBytes, new File(toDir, nameWithoutExtension + ".jpeg"));
                    }

                    @Override
                    public void onPhotoBarcodeData(String nameWithoutExtension) throws IOException {
                        AbstractData abstractData = readFromFile(i, new PhotoBarcodeData());
                        byte[] jpegBytes = abstractData.getJpegBytes(),
                                barcodeBytes = abstractData.getBarcodeBytes();

                        write(jpegBytes, new File(toDir, nameWithoutExtension + ".jpeg"));
                        write(barcodeBytes, new File(toDir, nameWithoutExtension + ".tbf"));
                    }

                    @Override
                    public void onStringListData(String nameWithoutExtension) throws IOException {
                        write(i, new File(toDir, nameWithoutExtension + ".tbf"));
                    }

                    @Override
                    public void onXmlData(String nameWithoutExtension) throws IOException {
                        write(i, new File(toDir, nameWithoutExtension + ".xml"));
                    }

                    @Override
                    public void on3gp(String nameWithoutExtension) throws IOException {
                        write(i, new File(toDir, nameWithoutExtension + ".mp4"));
                    }

                    @Override
                    public void onTxt(String nameWithoutExtension) throws IOException {
                        write(i, new File(toDir, nameWithoutExtension + ".txt"));
                    }
                });
            }
        }
    }

    private void copy(File fromDir, final UsbFile toDir, final FileSystem fileSystem) throws IOException {
        File[] fromFiles = fromDir.listFiles();
        if (fromFiles != null){
            for (final File i: fromFiles){
                GetFileType getFileType = new GetFileType(i);

                getFileType.getType(new GetFileType.OnFileTypeDetermined() {
                    @Override
                    public void onPhotoData(String nameWithoutExtension) throws IOException {
                        AbstractData abstractData = readFromFile(i, new PhotoData());
                        byte[] jpegBytes = abstractData.getJpegBytes();

                        write(jpegBytes, toDir, nameWithoutExtension + ".jpeg", fileSystem);
                    }

                    @Override
                    public void onPhotoBarcodeData(String nameWithoutExtension) throws IOException {
                        AbstractData abstractData = readFromFile(i, new PhotoBarcodeData());
                        byte[] jpegBytes = abstractData.getJpegBytes(),
                                barcodeBytes = abstractData.getBarcodeBytes();

                        write(jpegBytes, toDir, nameWithoutExtension + ".jpeg", fileSystem);
                        write(barcodeBytes, toDir, nameWithoutExtension + ".tbf", fileSystem);
                    }

                    @Override
                    public void onStringListData(String nameWithoutExtension) throws IOException {
                        write(i, toDir, nameWithoutExtension + ".tbf", fileSystem);
                    }

                    @Override
                    public void onXmlData(String nameWithoutExtension) throws IOException {
                        write(i, toDir, nameWithoutExtension + ".xml", fileSystem);
                    }

                    @Override
                    public void on3gp(String nameWithoutExtension) throws IOException {
                        write(i, toDir, nameWithoutExtension + ".mp4", fileSystem);
                    }

                    @Override
                    public void onTxt(String nameWithoutExtension) throws IOException {
                        write(i, toDir, nameWithoutExtension + ".txt", fileSystem);
                    }
                });
            }
        }
    }

    private void write(File from, File to) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(to);
        write(from, fileOutputStream);
        fileOutputStream.close();
    }

    private void write(byte[] from, File to) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(to);
        write(from, fileOutputStream);
        fileOutputStream.close();
    }

    private void write(File from, UsbFile toDir, String fileName, FileSystem fileSystem) throws IOException {
        UsbFile usbFile = toDir.search(fileName);
        if(usbFile == null) usbFile = toDir.createFile(fileName);

        BufferedOutputStream outputStream = UsbFileStreamFactory.createBufferedOutputStream(usbFile, fileSystem);
        write(from, outputStream);
        outputStream.close();
    }

    private void write(byte[] from, UsbFile toDir, String fileName, FileSystem fileSystem) throws IOException {
        UsbFile usbFile = toDir.search(fileName);
        if(usbFile == null) usbFile = toDir.createFile(fileName);

        BufferedOutputStream outputStream = UsbFileStreamFactory.createBufferedOutputStream(usbFile, fileSystem);
        write(from, outputStream);
        outputStream.close();
    }

    private void write(File from, OutputStream outputStream) throws IOException {
        InputStream inputStream = new FileInputStream(from);

        int bytesRead;
        byte[] buffer = new byte[1024];
        while((bytesRead = inputStream.read(buffer)) != -1){
            outputStream.write(buffer, 0, bytesRead);
        }

        inputStream.close();
    }

    private void write(byte[] from, OutputStream outputStream) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(from);

        int bytesRead;
        byte[] buffer = new byte[1024];
        while((bytesRead = inputStream.read(buffer)) != -1){
            outputStream.write(buffer, 0, bytesRead);
        }

        inputStream.close();
    }
}
