package com.example.thebeginning;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.List;

/**
 * Created by Константин on 13.04.2016.
 */
public class ViewFileActivity extends FragmentActivity {

    private int itemsCount;
    private FileManager fileManager;
    private List<String> listNames;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_file);
        Bundle bundle = getIntent().getExtras();
        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
        listNames = bundle.getStringArrayList(ContentManager.listItemsNamesKey);
        itemsCount = fileManager.getFilesCount();
        ViewPager viewPager = findViewById(R.id.view_file_view_pager);
        viewPager.setAdapter(new ViewFileFragmentAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(bundle.getInt(ContentManager.listItemPositionKey));
    }

    private class ViewFileFragmentAdapter extends FragmentStatePagerAdapter {

        public ViewFileFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ViewPageFragment.getInstance(listNames.get(position), fileManager);
        }

        @Override
        public int getCount() {
            return itemsCount;
        }
    }
}
