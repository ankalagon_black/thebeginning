package com.example.thebeginning;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Константин on 17.04.2016.
 */
public class ListItemAdapter extends ArrayAdapter<ListItem> {

    private int icon;

    public ListItemAdapter(Context context, List<ListItem> objects, int icon) {
        super(context, R.layout.list_item, objects);
        this.icon = icon;
    }

    private static class ViewHolder{
        public ImageView icon;
        public TextView name;
        public TextView state;
    }

    public void setIcon(int icon){
        if(icon != this.icon) this.icon = icon;
    }

    public void setListItems(List<ListItem> listItems){
        clear();
        addAll(listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.icon = convertView.findViewById(R.id.list_item_image);
            viewHolder.name = convertView.findViewById(R.id.list_item_name);
            viewHolder.state = convertView.findViewById(R.id.list_item_state);

            convertView.setTag(viewHolder);
        }

        ListItem listItem = getItem(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.icon.setImageResource(icon);
        viewHolder.name.setText(listItem.getName());
        String state = listItem.getState();
        if(state != null) viewHolder.state.setText(listItem.getState());
        else viewHolder.state.setText("");

        return convertView;
    }
}
