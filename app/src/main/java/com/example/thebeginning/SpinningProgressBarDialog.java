package com.example.thebeginning;

import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

/**
 * Created by Константин on 11.04.2016.
 */
public class SpinningProgressBarDialog extends DialogFragment {

    private ProgressBar progressBar;
    private Button closeButton;

    private Handler handler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        setCancelable(false);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ProgressBarDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.transmission_in_progress);

        View view = inflater.inflate(R.layout.spinning_progress_bar, container);
        progressBar = view.findViewById(R.id.spinning_progress_bar);
        closeButton = view.findViewById(R.id.spinning_progress_bar_close_button);
        closeButton.setAlpha(0.4f);
        closeButton.setEnabled(false);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    public void waitForCreation(){
        while (!isVisible());
    }

    public void finish(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                getDialog().setTitle(R.string.transmission_succesfully_finished);
                closeButton.setAlpha(1.0f);
                closeButton.setEnabled(true);
            }
        });
    }

}
