//package com.example.thebeginning;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.app.DialogFragment;
//import android.content.ContentValues;
//import android.content.DialogInterface;
//import android.database.Cursor;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DatabaseRecordsFragment;
//import android.support.v7.app.AlertDialog;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
///**
// * Created by Константин on 04.05.2016.
// */
//public class DialogCreateBarcode extends android.support.v4.app.DialogFragment {
//
//    private boolean areSearchTags = false, areData = false, isDisplayPattern = false;
//    private String quantityTag = null;
//    //temporary
//    private boolean changeHashOnFinish;
//    private FileManager fileManager;
//    private DatabaseManager databaseManager;
//    //temporary
//    private String inputName;
//    private StringListData data;
//
//    private Activity activity;
//
//    private long startTime;
//
//    private ArrayAdapterBarcodeRecord mainArrayAdapter;
//    private ArrayAdapterBarcodeRecord supportArrayAdapter;
//    private int ADAPTER_STATE = -1;
//    //0 - main, 1 - support
//
//    private EditText barcodeEdit, quantityEdit;
//    private Button all;
//    private CheckBox lightGreen, lightBlue, lightRed, darkRed;
//    private Button lightYellow;
//
//    private float LIGHT_GREEN = 0, LIGHT_BLUE = 0, LIGHT_RED = 0, DARK_RED = 0, LIGHT_YELLOW = 0;
//
//    private List<Integer> allowedColor = new ArrayList<>();
//
//    public static DatabaseRecordsFragment getInstance(FileManager fileManager, boolean changeHashOnFinish, String inputName){
//        DialogCreateBarcode dialogCreateBarcode = new DialogCreateBarcode();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(ContentManager.fileManagerKey, fileManager);
//        bundle.putBoolean(ContentManager.changeHashMapKey, changeHashOnFinish);
//        bundle.putString(ContentManager.listItemNameKey, inputName);
//        dialogCreateBarcode.setArguments(bundle);
//        return dialogCreateBarcode;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        activity = getActivity();
//        Bundle bundle = getArguments();
//        //temporary
//        changeHashOnFinish = bundle.getBoolean(ContentManager.changeHashMapKey);
//        if(!changeHashOnFinish) setStyle(STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//        else setStyle(STYLE_NORMAL, android.R.style.Theme_Holo_Light_DialogWhenLarge);
//
//        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
//        inputName = bundle.getString(ContentManager.listItemNameKey);
//        databaseManager = new DatabaseManager();
//        databaseManager.open(MainActivity.externalStoragePath + "/" + DatabaseManager.FOLDER_NAME);
//
//        final List<String> errorList = new ArrayList<>();
//
//        Cursor dataCntCursor = databaseManager.getDataRecordsCount();
//        dataCntCursor.moveToFirst();
//        int dataRowCount = dataCntCursor.getInt(dataCntCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD));
//        if(dataRowCount != 0) areData = true;
//        else errorList.add(getString(R.string.error_no_data));
//        dataCntCursor.close();
//
//        Cursor quantityCursor = databaseManager.getQuantityTag();
//        if(quantityCursor.getCount() != 0){
//            quantityCursor.moveToFirst();
//            quantityTag = quantityCursor.getString(quantityCursor.getColumnIndex(DatabaseManager.TAG_FIELD));
//        }
//        else errorList.add(getString(R.string.error_no_quantity_tag));
//        quantityCursor.close();
//
//        Cursor displayCursor = databaseManager.getDisplayRecordsCount();
//        displayCursor.moveToFirst();
//        if(displayCursor.getInt(displayCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD)) != 0) isDisplayPattern = true;
//        else errorList.add(getString(R.string.error_no_display_pattern));
//        displayCursor.close();
//
//        Cursor searchTagsCursor = databaseManager.getSearchTagsRecordsCount();
//        searchTagsCursor.moveToFirst();
//        if(searchTagsCursor.getInt(searchTagsCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD)) != 0) areSearchTags = true;
//        else errorList.add(getString(R.string.error_no_search_tags));
//        searchTagsCursor.close();
//
//        if(errorList.size() > 0){
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                    builder.setTitle(R.string.error);
//                    builder.setItems(errorList.toArray(new CharSequence[errorList.size()]), null);
//                    builder.setPositiveButton(getString(R.string.ok), null);
//                    builder.create().show();
//                }
//            }, 1000);
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
//        Dialog dialog = getDialog();
//        dialog.setTitle(R.string.create_barcode_dialog_title);
//        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
//            @Override
//            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                    builder.setTitle(R.string.warning);
//                    builder.setMessage(R.string.create_barcode_exit_message);
//                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            int size = mainArrayAdapter.getCount();
//                            for (int i = 0; i < size; i++) {
//                                data.add(mainArrayAdapter.requestData(i).mergeData());
//                                if (changeHashOnFinish) {
//                                    String newName = inputName == null ? String.valueOf(fileManager.getFilesCount()) + ", " +
//                                            new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime())
//                                            + "." + StringListData.class.getSimpleName() : inputName;
//                                    fileManager.writeToFile(newName, data);
//                                    if (inputName == null)
//                                        ((MainActivity) activity).updateContent(newName, null);
//                                    String currentFolderName = fileManager.getCurrentFolderName();
//                                    fileManager.goBack();
//                                    HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
//                                    hashMapManager.getHashMap();
//                                    hashMapManager.put(fileManager.getChildLocalPath(currentFolderName), false);
//                                    hashMapManager.saveChanges();
//                                    fileManager.goAhead(currentFolderName);
//                                } else
//                                    fileManager.writeToFile(String.valueOf(fileManager.getFilesCount()) + "." +
//                                            StringListData.class.getSimpleName(), data);
//                                dismiss();
//                            }
//                        }
//                    });
//                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dismiss();
//                        }
//                    });
//                    builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//                    if(mainArrayAdapter.getCount() == 0){
//                        button.setEnabled(false);
//                        button.setAlpha(0.4f);
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });
//
//        View view = inflater.inflate(R.layout.create_barcode_dialog_layout, null);
//
//        mainArrayAdapter = new ArrayAdapterBarcodeRecord(activity, R.layout.create_barcode_list_item, this);
//        final ListView listview = (ListView) view.findViewById(R.id.create_barcode_dialog_list_view);
//        listview.setAdapter(mainArrayAdapter);
//
//        barcodeEdit = (EditText) view.findViewById(R.id.create_barcode_dialog_barcode);
//        barcodeEdit.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (after == 1 || (start == 0 && after > 5)){
//                    startTime = System.currentTimeMillis();
//                }
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        barcodeEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View data, boolean hasFocus) {
//                if (!hasFocus) {
//                    long diff = System.currentTimeMillis() - startTime;
//                    if (diff < 300 && barcodeEdit.getText().length() > 5) {
//                        add(barcodeEdit.getText().toString(), quantityEdit.getText().toString());
//                    }
//                }
//            }
//        });
//        quantityEdit = (EditText) view.findViewById(R.id.create_barcode_dialog_number);
//        quantityEdit.setText("1");
//
//        all = (Button) view.findViewById(R.id.create_barcode_summary_all);
//        lightGreen = (CheckBox) view.findViewById(R.id.create_barcode_summary_light_green);
//        lightBlue = (CheckBox) view.findViewById(R.id.create_barcode_summary_light_blue);
//        lightRed = (CheckBox) view.findViewById(R.id.create_barcode_summary_light_red);
//        darkRed = (CheckBox) view.findViewById(R.id.create_barcode_summary_dark_red);
//        lightYellow = (Button) view.findViewById(R.id.create_barcode_summary_light_yellow);
//
//        if(!areData || !areSearchTags || quantityTag == null){
//            all.setEnabled(false);
//            lightGreen.setEnabled(false);
//            lightBlue.setEnabled(false);
//            lightRed.setEnabled(false);
//            darkRed.setEnabled(false);
//            lightYellow.setEnabled(false);
//        }
//        else{
//            Cursor yellowRecordsSum = databaseManager.getDataQuantitySum(quantityTag);
//            yellowRecordsSum.moveToFirst();
//            LIGHT_YELLOW = yellowRecordsSum.getInt(yellowRecordsSum.getColumnIndex(DatabaseManager.AGR_CNT_FIELD));
//            yellowRecordsSum.close();
//            supportArrayAdapter = new ArrayAdapterBarcodeRecord(activity, R.layout.create_barcode_list_item, this);
//        }
//
//        all.setText(String.format(getString(R.string.create_barcode_color_all), 0.0f));
//        all.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                lightGreen.setChecked(true);
//                lightBlue.setChecked(true);
//                lightRed.setChecked(true);
//                darkRed.setChecked(true);
//            }
//        });
//
//        allowedColor.add(0);
//        allowedColor.add(1);
//        allowedColor.add(2);
//        allowedColor.add(3);
//
//        lightGreen.setText(String.valueOf(LIGHT_GREEN));
//        lightGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked) allowedColor.add(0);
//                else allowedColor.remove(Integer.valueOf(0));
//                mainArrayAdapter.notifyDataSetChanged();
//            }
//        });
//        lightBlue.setText(String.valueOf(LIGHT_BLUE));
//        lightBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked) allowedColor.add(1);
//                else allowedColor.remove(Integer.valueOf(1));
//                mainArrayAdapter.notifyDataSetChanged();
//            }
//        });
//        lightRed.setText(String.valueOf(LIGHT_RED));
//        lightRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked) allowedColor.add(2);
//                else allowedColor.remove(Integer.valueOf(2));
//                mainArrayAdapter.notifyDataSetChanged();
//            }
//        });
//        darkRed.setText(String.valueOf(DARK_RED));
//        darkRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked) allowedColor.add(3);
//                else allowedColor.remove(Integer.valueOf(3));
//                mainArrayAdapter.notifyDataSetChanged();
//            }
//        });
//        lightYellow.setText(String.valueOf(LIGHT_YELLOW));
//        lightYellow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                if(ADAPTER_STATE < 1 && supportArrayAdapter != null){
//                    ADAPTER_STATE = 1;
//                    if(supportArrayAdapter.getCount() == 0){
//                        Cursor searchTagCursor = databaseManager.getFirstSerachTag();
//                        searchTagCursor.moveToFirst();
//                        String searchTag = searchTagCursor.getString(searchTagCursor.getColumnIndex(DatabaseManager.TAG_FIELD));
//                        searchTagCursor.close();
//
//                        int mainArrayAdapterSize = mainArrayAdapter.getCount();
//                        List<BarcodeRecord> allRecords = mainArrayAdapter.getAll(), greenRecords = new ArrayList<>();
//                        for(int i = 0; i < mainArrayAdapterSize; i++){
//                            BarcodeRecord barcodeRecord = allRecords.get(i);
//                            if(barcodeRecord.getColor() == 0){
//                                greenRecords.add(barcodeRecord);
//                            }
//                        }
//
//                        Cursor cursor = databaseManager.getAllMainTableRecords(searchTag, quantityTag);
//                        cursor.moveToFirst();
//                        int size = cursor.getCount(), idColumn = cursor.getColumnIndex(DatabaseManager.ID),
//                                valueColumn = cursor.getColumnIndex(searchTag), quantityColumn = cursor.getColumnIndex(quantityTag),
//                                greenSize = greenRecords.size();
//                        for(int i = 0; i < size; i++){
//                            int id = cursor.getInt(idColumn);
//                            boolean flag = true;
//                            for(int j = 0; j < greenSize; j++){
//                                if(greenRecords.get(j).getDataId() == id){
//                                    flag = false;
//                                    break;
//                                }
//                            }
//                            if(flag){
//                                String value = cursor.getString(valueColumn);
//                                float quantity = cursor.getFloat(quantityColumn);
//                                supportArrayAdapter.add(new BarcodeRecord(id, value, quantity, 4));
//                            }
//                            cursor.moveToNext();
//                        }
//                        cursor.close();
//                    }
//                    listview.setAdapter(supportArrayAdapter);
//                }
//                else {
//                    ADAPTER_STATE = 0;
//                    listview.setAdapter(mainArrayAdapter);
//                }
//            }
//        });
//
//        data = new StringListData();
//        if(inputName != null){
//            fileManager.readFromFile(inputName, data);
//            List<String> values = data.getList();
//            for(String i: values){
//                int commaPosition = i.lastIndexOf(",");
//                String barcode = i.substring(0, commaPosition), quantity = i.substring(commaPosition + 1);
//                add(barcode, quantity);
//            }
//            data.clear();
//        }
//
//
//        final ImageButton add = (ImageButton) view.findViewById(R.id.create_barcode_dialog_add_barcode);
//        add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                add(barcodeEdit.getText().toString(), quantityEdit.getText().toString());
//            }
//        });
//        ImageButton search = (ImageButton) view.findViewById(R.id.create_barcode_search);
//        search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                builder.setTitle(R.string.create_barcode_search);
//                final EditText editText = new EditText(activity);
//                builder.setView(editText);
//                builder.setPositiveButton(R.string.search, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String searchString = editText.getText().toString();
//                        if(!searchString.equals("")){
//                            DialogDisplayDBQuery displayDBQuery = new DialogDisplayDBQuery();
//                            Bundle bundle = new Bundle();
//                            bundle.putString(DialogDisplayDBQuery.SEARCH_VALUE_KEY, searchString);
//                            displayDBQuery.setArguments(bundle);
//                            displayDBQuery.show(activity.getFragmentManager(), "1");
//                        }
////                        else Toast.makeText(activity, R.string.error_empty_search_string, Toast.LENGTH_SHORT).show();
//                    }
//                });
//                builder.setNegativeButton(R.string.cancel, null);
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//                final Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//                button.setEnabled(false);
//                button.setAlpha(0.4f);
//                editText.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable s) {
//                        if(s.toString().equals("")){
//                            button.setEnabled(false);
//                            button.setAlpha(0.4f);
//                        }
//                        else{
//                            button.setEnabled(true);
//                            button.setAlpha(1);
//                        }
//                    }
//                });
//            }
//        });
//        ImageButton clearList = (ImageButton) view.findViewById(R.id.create_barcode_clear_list);
//        clearList.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                if (mainArrayAdapter.getCount() > 0) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                    builder.setTitle(R.string.warning);
//                    builder.setMessage(R.string.create_barcode_clear_list);
//                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            mainArrayAdapter.clear();
//                            LIGHT_YELLOW += LIGHT_GREEN;
//                            LIGHT_GREEN = 0;
//                            LIGHT_BLUE = 0;
//                            LIGHT_RED = 0;
//                            DARK_RED = 0;
//
//                            lightYellow.setText(String.valueOf(LIGHT_YELLOW));
//                            lightGreen.setText(String.valueOf(LIGHT_GREEN));
//                            ;
//                            lightBlue.setText(String.valueOf(LIGHT_BLUE));
//                            lightRed.setText(String.valueOf(LIGHT_RED));
//                            darkRed.setText(String.valueOf(DARK_RED));
//
//                            all.setText(String.format(getString(R.string.create_barcode_color_all), 0.0f));
//                        }
//                    });
//                    builder.setNegativeButton(R.string.no, null);
//                    builder.create().show();
//                }
//                else Toast.makeText(activity,R.string.create_barcode_warning_list_empty, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        return view;
//    }
//
//    private void add(final String value, String quantity) {
//        if (value.equals("") || quantity.equals(""))
//            Toast.makeText(activity, activity.getString(R.string.create_barcode_warning_fields_are_not_filled), Toast.LENGTH_SHORT).show();
//        else {
//            int size = mainArrayAdapter.getCount();
//            boolean isInList = false;
//            float q = Float.parseFloat(quantity);
//            if (areData && areSearchTags && quantityTag != null) {
//                for (int i = 0; i < size; i++) {
//                    BarcodeRecord record = mainArrayAdapter.requestData(i);
//                    if (record.getDefaultValue().equals(value)) {
//                        record.addQuantity(q);
//                        int oldColor = record.getColor();
//                        Container container = getColor(value, record.getDbQuantity());
//                        int newColor = container.getColor();
//                        if (oldColor != newColor) {
//                            if(ADAPTER_STATE != -1 && newColor == 0){
//                                int id = record.getDataId();
//                                List<BarcodeRecord> list = supportArrayAdapter.getAll();
//                                int listSize = list.size();
//                                for(int j = 0; j < listSize; j ++){
//                                    BarcodeRecord barcodeRecord = list.get(j);
//                                    if(barcodeRecord.getDataId() == id){
//                                        supportArrayAdapter.remove(barcodeRecord);
//                                        break;
//                                    }
//                                }
//                            }
//                            else if(ADAPTER_STATE != -1 && oldColor == 0){
//                                int id = record.getDataId();
//                                Cursor cursor = databaseManager.search(value);
//                                cursor.moveToFirst();
//                                supportArrayAdapter.add(new BarcodeRecord(id, value, cursor.getFloat(cursor.getColumnIndex(quantityTag)), 4));
//                                cursor.close();
//                            }
//                            defineCategory(oldColor, record.getDbQuantity() - q, false);
//                            defineCategory(newColor, record.getDbQuantity() - q, true);
//                            record.setColor(newColor);
//                        }
//                        defineCategory(newColor, q, true);
//                        mainArrayAdapter.notifyDataSetChanged();
//                        isInList = true;
//                        break;
//                    }
//                }
//                if (!isInList){
//                    Container container = getColor(value, q);
//                    int color = container.getColor(), id = container.getDataId();
//                    defineCategory(color, q, true);
//                    if(ADAPTER_STATE != -1 && color == 0){
//                        List<BarcodeRecord> list = supportArrayAdapter.getAll();
//                        int listSize = list.size();
//                        for(int i = 0; i < listSize; i++){
//                            BarcodeRecord barcodeRecord = list.get(i);
//                            if(barcodeRecord.getDataId() == id){
//                                supportArrayAdapter.remove(barcodeRecord);
//                                break;
//                            }
//                        }
//                    }
//                    mainArrayAdapter.add(new BarcodeRecord(id, value, q, color));
//                }
//            }
//            else {
//                for (int i = 0; i < size; i++) {
//                    BarcodeRecord record = mainArrayAdapter.requestData(i);
//                    if (record.getDefaultValue().equals(value)) {
//                        record.addQuantity(q);
//                        mainArrayAdapter.notifyDataSetChanged();
//                        isInList = true;
//                        break;
//                    }
//                }
//                if (!isInList) mainArrayAdapter.add(new BarcodeRecord(value, q));
//            }
//
//            barcodeEdit.setText("");
//            quantityEdit.setText("1");
//            barcodeEdit.post(new Runnable() {
//                @Override
//                public void run() {
//                    barcodeEdit.requestFocus();
//                }
//            });
//        }
//    }
//
//    public void delete(BarcodeRecord record){
//        int color = record.getColor();
//        defineCategory(color, record.getDbQuantity(), false);
//        mainArrayAdapter.remove(record);
//        if(ADAPTER_STATE != -1 && color == 0){
//            record.setColor(4);
//            supportArrayAdapter.add(record);
//        }
//    }
//
//    private Container getColor(String value, float quantity){
//        Cursor dataCursor = databaseManager.search(value);
//        int id = -1, color, size = dataCursor.getCount();
//        if (size == 0) color = 3;
//        else {
//            dataCursor.moveToFirst();
//            int databaseQuantity = dataCursor.getInt(dataCursor.getColumnIndex(quantityTag));
//            if (Math.abs(databaseQuantity - quantity) < 0.0001) color = 0;
//            else if (databaseQuantity > quantity) color = 1;
//            else color = 2;
//            id = dataCursor.getInt(dataCursor.getColumnIndex(DatabaseManager.ID));
//        }
//        dataCursor.close();
//        return new Container(id, color);
//    }
//
//    private void defineCategory(int color, float adjustment, boolean isAddition){
//        float increment = (isAddition ? 1 : -1) * adjustment;
//        switch (color){
//            case 0:
//                LIGHT_GREEN += increment;
//                LIGHT_YELLOW -= increment;
//                lightGreen.setText(String.valueOf(LIGHT_GREEN));
//                lightYellow.setText(String.valueOf(LIGHT_YELLOW));
//                all.setText(String.format(getString(R.string.create_barcode_color_all), LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
//                break;
//            case 1:
//                LIGHT_BLUE += increment;
//                lightBlue.setText(String.valueOf(LIGHT_BLUE));
//                all.setText(String.format(getString(R.string.create_barcode_color_all), LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
//                break;
//            case 2:
//                LIGHT_RED += increment;
//                lightRed.setText(String.valueOf(LIGHT_RED));
//                all.setText(String.format(getString(R.string.create_barcode_color_all), LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
//                break;
//            case 3:
//                DARK_RED += increment;
//                darkRed.setText(String.valueOf(DARK_RED));
//                all.setText(String.format(getString(R.string.create_barcode_color_all), LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
//                break;
//        }
//    }
//
//    public DatabaseManager getDatabaseManager(){
//        return databaseManager;
//    }
//
//    public boolean isDisplayPattern() {
//        return isDisplayPattern;
//    }
//
//    public boolean areSearchTags(){
//        return areSearchTags;
//    }
//
//    public boolean areData(){
//        return areData;
//    }
//
//    public List<Integer> getAllowedColor(){
//        return allowedColor;
//    }
//
//    public int getAdapterState(){
//        return ADAPTER_STATE;
//    }
//
//    private static class Container{
//        private int id, color;
//
//        public Container(int id, int color){
//            this.id = id;
//            this.color = color;
//        }
//
//        public int getDataId(){
//            return id;
//        }
//
//        public int getColor(){
//            return color;
//        }
//    }
//}
