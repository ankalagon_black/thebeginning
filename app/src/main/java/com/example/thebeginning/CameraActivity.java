package com.example.thebeginning;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

/**
 * Created by Константин on 02.07.2016.
 */
public class CameraActivity extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.PictureCallback{

    private static final int MODE_PHOTO = 0, MODE_VIDEO = 1, MODE_SCANNER = 2, MODE_NONE = -1;

    private static final String MODE_KEY = "ModeKey", FILE_STORAGE_KEY = "FileStorageKey";

    private Camera camera;
    private CameraZoomBar cameraZoomBar;

    private OrientationEventListener orientationEventListener;

    private SurfaceHolder surfaceHolder;
    private boolean isPreviewDisplaySet = false;

    private int mode = MODE_NONE;

    private DrawerLayout drawerLayout;
    private Button modeButton, flashButton, exitButton, fileListButton, actionButton, scannerModeSwitchButton;

    private FileManager fileStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_preview);

        drawerLayout = findViewById(R.id.preview_drawer_layout);
        final ListView listView = findViewById(R.id.preview_drawer_list);
        final String[] activityModes = getResources().getStringArray(R.array.camera_activity_modes);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, activityModes));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String string = getString(R.string.camera_activity_mode_message);
                setMode(position);
                Toast.makeText(CameraActivity.this, string + activityModes[position],Toast.LENGTH_SHORT).show();
                drawerLayout.closeDrawer(listView);
            }
        });

        SurfaceView surfaceView = findViewById(R.id.preview_surface_view);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);

        cameraZoomBar = findViewById(R.id.preview_zoom_bar);
        Display display = getWindowManager().getDefaultDisplay();
        cameraZoomBar.setSurfaceView(surfaceView);
        cameraZoomBar.setScreenLenght(display.getWidth(), display.getHeight());

        modeButton = findViewById(R.id.preview_mode);
        flashButton = findViewById(R.id.preview_flash);
        exitButton = findViewById(R.id.preview_exit);
        fileListButton = findViewById(R.id.preview_file_list);
        actionButton = findViewById(R.id.preview_action);
        scannerModeSwitchButton = findViewById(R.id.preview_scanner_mode_switch);
//        Bundle bundle = getIntent().getExtras();
//        setMode(bundle.getInt(MODE_KEY));
//        fileStorage = bundle.getParcelable(FILE_STORAGE_KEY);
        setMode(MODE_PHOTO);
        fileStorage = new FileManager("/storage/sdcard1/TBF", "");

    }

    public static Bundle createBundle(int mode, FileManager fileStorage){
        Bundle bundle = new Bundle();
        bundle.putInt(MODE_KEY, mode);
        bundle.putParcelable(FILE_STORAGE_KEY, fileStorage);
        return bundle;
    }
    
    private void setMode(int mode){
        if(this.mode != mode){
            onBeforeModeChanged(this.mode);
            this.mode = mode;
            onModeChanged(mode);
        }
    }

    private void onBeforeModeChanged(int mode){
        switch (mode){
            case MODE_NONE:
                modeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawerLayout.openDrawer(Gravity.START);
                    }
                });
                //// TODO: 05.09.2016 set flash button
                exitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //// TODO: 05.09.2016 save data when exit and change method for closing actvity
                        CameraActivity.this.finish();
                    }
                });
                //// TODO: 05.09.2016 set file list button
                orientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {

                    private int lastOrientation = -1;

                    @Override
                    public void onOrientationChanged(int orientation) {
                        //// TODO: 06.09.2016 delete later 
                        if (orientation == ORIENTATION_UNKNOWN || camera == null) return;
                        orientation = (orientation + 45) / 90 * 90;
                        //// TODO: 06.09.2016 add onresume and onpause disable
                        if(orientation != lastOrientation){
                            Camera.CameraInfo info = new Camera.CameraInfo();
                            Camera.getCameraInfo(0, info);
                            Camera.Parameters parameters = camera.getParameters();
                            parameters.setRotation((info.orientation + orientation) % 360);
                            camera.setParameters(parameters);

                            lastOrientation = orientation;
                        }
                    }
                };
                break;
            case MODE_PHOTO:
                break;
            case MODE_VIDEO:
                break;
            case MODE_SCANNER:
                break;
        }
    }
    
    private void onModeChanged(int mode){
        switch (mode){
            case MODE_PHOTO:
                actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       camera.takePicture(null, null, CameraActivity.this);
                    }
                });
                scannerModeSwitchButton.setVisibility(View.INVISIBLE);

                orientationEventListener.enable();
                break;
            case MODE_VIDEO:
                actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //record a video
                    }
                });
                scannerModeSwitchButton.setVisibility(View.INVISIBLE);

                orientationEventListener.disable();
                break;
            case MODE_SCANNER:
                actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //set scanner
                    }
                });
                scannerModeSwitchButton.setVisibility(View.VISIBLE);

                //// TODO: 06.09.2016 set changing of orientation listener depending accountedRecordFrom switch
                break;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        //// TODO: 05.09.2016 save picture and rotate it
        PhotoData photoData = new PhotoData();
        photoData.setJpegBytes(data);
        fileStorage.writeToFile(String.valueOf(fileStorage.getFilesCount()) + "." + photoData.getClass().getSimpleName(), photoData);
        //
        camera.startPreview();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
    }

    private void initializeCameraPreview(){
        try {
            int rotation = this.getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0: 
                    degrees = 0;
                    break;
                case Surface.ROTATION_90:
                    degrees = 90;
                    break;
                case Surface.ROTATION_180: 
                    degrees = 180;
                    break;
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(0, info);
            camera.setDisplayOrientation((info.orientation - degrees + 360) % 360);

            //// TODO: 06.09.2016 think about dimension settings
            Camera.Parameters parameters = camera.getParameters();
            List<Camera.Size> sizeList = parameters.getSupportedPictureSizes();
            int size = sizeList.size();
            Camera.Size selectedSize = sizeList.get(0);
            for(int i = 1; i < size; i++){
                Camera.Size cameraSize = sizeList.get(i);
                if(cameraSize.width == 1920){
                    selectedSize = cameraSize;
                    break;
                }
                else if(cameraSize.width > selectedSize.width) selectedSize = cameraSize;
            }

            parameters.setPictureSize(selectedSize.width, selectedSize.height);
            parameters.setJpegQuality(100);

            camera.setParameters(parameters);
            //end
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            Log.v(CameraActivity.class.getSimpleName(), e.getMessage());
        }
        camera.startPreview();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();

        if(isPreviewDisplaySet = surfaceHolder.getSurface().isValid()) initializeCameraPreview();

        cameraZoomBar.setCamera(camera);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(camera != null) {
            if(!isPreviewDisplaySet) initializeCameraPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }
}
