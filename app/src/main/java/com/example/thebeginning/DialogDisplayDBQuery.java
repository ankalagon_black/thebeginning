package com.example.thebeginning;

import android.app.DialogFragment;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Константин on 30.07.2016.
 */
public class DialogDisplayDBQuery extends DialogFragment {

    public static final String SEARCH_VALUE_KEY = "SearchValue";
    public static final String FILE_MANAGER_KEY = "FileManager";
    public static final String ID_SEARCH_KEY = "IdSearch";
    public static final String FILE_NAME_KEY = "FileName";

    private String searchValue;
    private FileManager fileManager;
    private boolean idSearch;
    private String filename;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Holo_Light_DialogWhenLarge);
        Bundle bundle = getArguments();
        searchValue = bundle.getString(SEARCH_VALUE_KEY);
        fileManager = bundle.getParcelable(FILE_MANAGER_KEY);
        idSearch = bundle.getBoolean(ID_SEARCH_KEY);
        filename = bundle.getString(FILE_NAME_KEY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context context = getActivity();
        getDialog().setTitle(R.string.display_db_query_dialog_title);
        View view = inflater.inflate(R.layout.dialog_display_db_query, null);
        LinearLayout layout = view.findViewById(R.id.display_db_query_layout);
        DatabaseManager databaseManager = new DatabaseManager(filename);
        databaseManager.open(fileManager);
        Cursor displayPattern = databaseManager.getDisplayPattern();
        int diplayPatternSize = displayPattern.getCount();
        Cursor searchResult = databaseManager.search(searchValue, idSearch);
        int searchResultSize = searchResult.getCount();
        if(searchResultSize > 0){
            searchResult.moveToFirst();
            for(int i = 0; i < searchResultSize; i++){
                View layoutItem = inflater.inflate(R.layout.dialog_display_db_query_item, null);
                TextView itemTitleView = layoutItem.findViewById(R.id.display_db_query_layout_text);
                itemTitleView.setText(String.format(context.getString(R.string.display_db_query_item_title), i+1, searchResultSize));
                LinearLayout tableLayout = layoutItem.findViewById(R.id.display_db_query_layout_table);
                LinearLayout tableRow = (LinearLayout) inflater.inflate(R.layout.dialog_display_db_query_row, null);
                displayPattern.moveToFirst();
                int indexRow = displayPattern.getColumnIndex(DatabaseManager.ROW_FIELD),
                        indexColumn = displayPattern.getColumnIndex(DatabaseManager.COLUMN_FIELD);
                int row = displayPattern.getInt(indexRow), lastRow = row,
                        column = displayPattern.getInt(indexColumn), currentRow = 0, currentColumn = 0,
                        show = displayPattern.getColumnIndex(DatabaseManager.SHOW_FIELD);
                while(row != currentRow){
                    LinearLayout layout1 = (LinearLayout) inflater.inflate(R.layout.dialog_display_db_query_row, null);
                    layout1.addView(inflater.inflate(R.layout.display_db_text_placeholder, null));
                    tableLayout.addView(layout1);
                    ++currentRow;
                }
                while (column != currentColumn) {
                    tableRow.addView(inflater.inflate(R.layout.display_db_text_placeholder, null));
                    ++currentColumn;
                }
                if(displayPattern.getInt(show) != 0) addRowItem(inflater, displayPattern, searchResult, tableRow, context);
                ++currentColumn;
                displayPattern.moveToNext();
                for(int j = 1; j < diplayPatternSize; j++){
                    row = displayPattern.getInt(indexRow);
                    column = displayPattern.getInt(indexColumn);

                    if(lastRow != row){
                        ++currentRow;
                        while(row != currentRow){
                            LinearLayout layout1 = (LinearLayout) inflater.inflate(R.layout.dialog_display_db_query_row, null);
                            layout1.addView(inflater.inflate(R.layout.display_db_text_placeholder, null));
                            tableLayout.addView(layout1);
                            ++currentRow;
                        }
                        currentColumn = 0;
                        tableLayout.addView(tableRow);
                        tableRow = (LinearLayout) inflater.inflate(R.layout.dialog_display_db_query_row, null);
                    }
                    lastRow = row;
                    while (column != currentColumn) {
                        tableRow.addView(inflater.inflate(R.layout.display_db_text_placeholder, null));
                        ++currentColumn;
                    }
                    if(displayPattern.getInt(show) != 0) addRowItem(inflater, displayPattern, searchResult, tableRow, context);
                    ++currentColumn;
                    displayPattern.moveToNext();
                }
                tableLayout.addView(tableRow);
                layout.addView(layoutItem);
                searchResult.moveToNext();
            }
        }
        else showMessage(R.string.display_db_query_error_no_result, layout, context);
        searchResult.close();
        displayPattern.close();
        return view;
    }

    private void addRowItem(LayoutInflater inflater, Cursor patternCursor, Cursor dataCursor,LinearLayout linearLayout, Context context){
        String content = patternCursor.getString(patternCursor.getColumnIndex(DatabaseManager.CONTENT_FIELD));
        String isTitle = patternCursor.getString(patternCursor.getColumnIndex(DatabaseManager.IS_TITLE_FIELD));
        View view = inflater.inflate(R.layout.dialog_display_db_query_text_view, null);
        TextView textView = view.findViewById(R.id.tot_id);
        //
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);
        //
        if(isTitle.equals("1")){
            textView.setText(content);
        }
        else{
            textView.setText(dataCursor.getString(dataCursor.getColumnIndex(content)));
        }
        int index = patternCursor.getColumnIndex(DatabaseManager.BACKGROUND_COLOR_FIELD);
        if(!patternCursor.isNull(index)){
            String value = patternCursor.getString(index);
            textView.setBackgroundColor(Color.parseColor(value));
        }
        index = patternCursor.getColumnIndex(DatabaseManager.TEXT_COLOR_FIELD);
        if(!patternCursor.isNull(index)){
            String value = patternCursor.getString(index);
            textView.setTextColor(Color.parseColor(value));
        }
        index = patternCursor.getColumnIndex(DatabaseManager.TEXT_WIDTH_FIELD);
        if(!patternCursor.isNull(index)){
            int value = patternCursor.getInt(index);
            textView.setEms(value);
        }
        index = patternCursor.getColumnIndex(DatabaseManager.IS_FONT_BOLD_FIELD);
        if(!patternCursor.isNull(index)){
            int value = patternCursor.getInt(index);
            if(value == 1) textView.setTypeface(null, Typeface.BOLD);
        }
        index = patternCursor.getColumnIndex(DatabaseManager.TEXT_SIZE_FIELD);
        if(!patternCursor.isNull(index)){
            int value = patternCursor.getInt(index);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, value);
        }
        linearLayout.addView(view);
    }

    private void showMessage(int resId, LinearLayout layout, Context context){
        //for a while
        TextView textView = new TextView(context);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        textView.setLayoutParams(params);
        textView.setText(resId);
        layout.addView(textView);
    }
}
