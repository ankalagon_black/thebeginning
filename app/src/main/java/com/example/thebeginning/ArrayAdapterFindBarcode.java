package com.example.thebeginning;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 22.10.2016.
 */

public class ArrayAdapterFindBarcode extends ArrayAdapter<BarcodeRecord>{

    private List<BarcodeRecord> records;
    private ArrayAdapterBarcodeRecord adapterBarcodeRecord;
    private BarcodeRecord record;
    private AlertDialog dialog;

    public ArrayAdapterFindBarcode(Context context, int resource,
                                   List<BarcodeRecord> objects,
                                   ArrayAdapterBarcodeRecord adapterBarcodeRecord,
                                   BarcodeRecord record) {
        super(context, resource, objects);
        records = objects;
        this.adapterBarcodeRecord = adapterBarcodeRecord;
        this.record = record;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            ViewHolder viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View view = layoutInflater.inflate(R.layout.find_barcode_dialog_list_item, null);
            viewHolder.barcode = view.findViewById(R.id.find_barcode_dialog_list_item_barcode);
            viewHolder.quantity = view.findViewById(R.id.find_barcode_dialog_list_item_quantity);
            viewHolder.extra = view.findViewById(R.id.find_barcode_dialog_list_item_extra);
            viewHolder.search = view.findViewById(R.id.find_barcode_dialog_list_item_find);
            viewHolder.ok = view.findViewById(R.id.find_barcode_dialog_list_item_ok);
            convertView = view;
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        final BarcodeRecord barcodeRecord = records.get(position);
        viewHolder.barcode.setText(barcodeRecord.getValue());
        viewHolder.quantity.setText(String.format(Locale.US, "%.2f", barcodeRecord.getDbQuantity()));
        viewHolder.extra.setText(adapterBarcodeRecord.getInfoString(String.valueOf(barcodeRecord.getId()), true));
        viewHolder.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCreateBacode activity = adapterBarcodeRecord.getActivity();
                if(activity.isSearchAvailable()){
                    DialogDisplayDBQuery dialog = new DialogDisplayDBQuery();
                    Bundle bundle = new Bundle();
                    bundle.putString(DialogDisplayDBQuery.SEARCH_VALUE_KEY ,barcodeRecord.getValue());
                    bundle.putParcelable(DialogDisplayDBQuery.FILE_MANAGER_KEY, activity.getFileManager());
                    bundle.putBoolean(DialogDisplayDBQuery.ID_SEARCH_KEY, true);
                    bundle.putString(DialogDisplayDBQuery.FILE_NAME_KEY, activity.getDatabaseManager().getFileName());
                    dialog.setArguments(bundle);
                    dialog.show(activity.getFragmentManager(), "1");
                }
            }
        });
        viewHolder.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int size = adapterBarcodeRecord.getCount();
                boolean flag = false;
                List<BarcodeRecord> list = adapterBarcodeRecord.getAll();
                for(int i = 0; i < size; i++){
                    BarcodeRecord barcodeRecord1 = list.get(i);
                    if(barcodeRecord.getId() == barcodeRecord1.getId()){
                        flag = true;
                        break;
                    }
                }

                if(flag){
                    if(record.getValue().equals(barcodeRecord.getValue())){
                        Toast.makeText(adapterBarcodeRecord.getContext(), R.string.warning_this_action_cannot_be_performed_on_already_existing_object, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        FragmentCreateBarcode fragment = adapterBarcodeRecord.getFragment();
                        fragment.delete(record);
                        fragment.add(barcodeRecord.getValue(), String.valueOf(record.getQuantity()), false);
                        Toast.makeText(adapterBarcodeRecord.getContext(), R.string.warning_this_object_already_exists, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    FragmentCreateBarcode fragment = adapterBarcodeRecord.getFragment();
                    fragment.delete(record);
                    fragment.add(barcodeRecord.getValue(), String.valueOf(record.getQuantity()), false);
                }

                dialog.dismiss();
            }
        });
        return convertView;
    }

    public void setRecords(List<BarcodeRecord> records){
        this.records = records;
        this.clear();
        this.addAll(records);
    }

    public void setDialog(AlertDialog dialog){
        this.dialog = dialog;
    }

    public static class ViewHolder{
        public TextView barcode;
        public TextView quantity;
        public TextView extra;
        public ImageButton search;
        public ImageButton ok;
    }
}
