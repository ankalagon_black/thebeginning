package com.example.thebeginning.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thebeginning.data_types.GlobalSettings;

import java.util.List;

/**
 * Created by Константин on 20.01.2018.
 */

public class ChooseDialogAdapter extends ArrayAdapter<String> {
    private String[] modes;

    public ChooseDialogAdapter(@NonNull Context context, @NonNull List<String> objects, String[] modes) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.modes = modes;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);

        TextView textView = (TextView) convertView;
        String item = getItem(position);
        if(item != null) textView.setText(GlobalSettings.getModeTitle(item, modes));

        return convertView;
    }
}
