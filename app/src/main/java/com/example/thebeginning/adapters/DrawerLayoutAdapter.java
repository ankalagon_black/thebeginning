package com.example.thebeginning.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thebeginning.data_types.GlobalSettings;

import java.util.List;

/**
 * Created by Константин on 20.01.2018.
 */

public class DrawerLayoutAdapter extends ArrayAdapter<String> {
    public static final String SETTINGS = "Settings";

    private String[] modes;
    private String settings;

    public DrawerLayoutAdapter(@NonNull Context context, @NonNull List<String> objects, String[] modes, String settings) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.modes = modes;
        this.settings = settings;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
        TextView textView = (TextView) convertView;
        String item = getItem(position);
        if(item != null) {
            String title = GlobalSettings.getModeTitle(item, modes);
            textView.setText(title == null ? settings : title);
        }

        return textView;
    }
}
