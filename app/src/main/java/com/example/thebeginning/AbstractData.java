package com.example.thebeginning;

import android.graphics.Bitmap;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by Константин on 03.04.2016.
 */
public abstract class AbstractData {

    public abstract void write(FileOutputStream outputStream);
    public abstract void read(FileInputStream inputStream);

    public Bitmap getBitmap(){
        return null;
    }
    public byte[] getJpegBytes(){
        return null;
    }
    public void setJpegBytes(byte[] bytes){

    }

    public String getBarcode(){
        return null;
    }
    public byte[] getBarcodeBytes(){
        return null;
    }
    public void setBarcode(String barcode){
    }

    public List<String> getList(){
        return null;
    }
    public void add(String string){
    }

    public void setContent(String content){
    }

    public String getContent(){
        return null;
    }


    public String getText(){
        return null;
    }
}
