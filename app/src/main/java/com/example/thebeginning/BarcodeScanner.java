package com.example.thebeginning;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

/**
 * Created by Константин on 12.01.2016.
 */
public class BarcodeScanner {

    private static final String TAG = "BarcodeScanner";

    private MultiFormatReader reader;

    public BarcodeScanner(){
        reader = new MultiFormatReader();
    }

    public Result decode(byte[] data, Rect rect){
        PlanarYUVLuminanceSource source = null;
        try{
            source = new PlanarYUVLuminanceSource(data, rect.right, rect.bottom, rect.left, rect.top, rect.width(), rect.height(), false);
        }
        catch (Exception e){
            Log.v(TAG,"Decoding error: " + e.toString());
        }

        Result result = null;
        if(source != null){
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try{
                result = reader.decode(bitmap);
            }
            catch(NotFoundException e) {
            }
            finally {
                reader.reset();
            }
        }
        return result;
    }

    public Result decode(Bitmap bitmap){
        int width = bitmap.getWidth(), height = bitmap.getHeight();
        int[] array = new int[width * height];
        bitmap.getPixels(array, 0, width, 0, 0, width, height);

        LuminanceSource luminanceSource = new RGBLuminanceSource(width, height, array);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));

        Result result = null;
        try{
            result = reader.decode(binaryBitmap);
        }
        catch(NotFoundException e) {
        }
        finally {
            reader.reset();
        }
        return result;
    }
}
