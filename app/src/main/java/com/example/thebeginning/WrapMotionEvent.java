package com.example.thebeginning;

import android.view.MotionEvent;

/**
 * Created by Константин on 15.04.2016.
 */
public class WrapMotionEvent {
    protected MotionEvent event;

    protected WrapMotionEvent(MotionEvent event) {
        this.event = event;
    }

    static public WrapMotionEvent wrap(MotionEvent event) {
        try {
            return new EclairMotionEvent(event);
        } catch (VerifyError e) {
            return new WrapMotionEvent(event);
        }
    }


    public int getAction() {
        return event.getAction();
    }

    public float getX() {
        return event.getX();
    }

    public float getX(int pointerIndex) {
        return getX();
    }

    public float getY() {
        return event.getY();
    }

    public float getY(int pointerIndex) {
        return getY();
    }
}
