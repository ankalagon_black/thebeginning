package com.example.thebeginning;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by apple on 12.08.16.
 */
public class FragmentCreateBarcode extends Fragment {

    public static final String POSITION_KEY = "PositionKey";

    private int FRAGMENT_POSITION;

    private ActivityCreateBacode activity;

    private DatabaseManager databaseManager;

    private int ADAPTER_ID = 0;

    private ArrayAdapterBarcodeRecord allAdapter, yellowAdapter;

    private int COUNT_LG = 0, COUNT_LB = 0, COUNT_LR = 0, COUNT_DR, COUNT_LY = 0;
    private float LIGHT_GREEN = 0, LIGHT_BLUE = 0, LIGHT_RED = 0, DARK_RED = 0, LIGHT_YELLOW = 0;
    private List<Integer> allowedColors = new ArrayList<>();
    private List<Integer> allowedColorsCopy;
    private Button all, lightYellow;
    private CheckBox lightGreen, lightBlue, lightRed, darkRed;

    private EditText barcodeEdit, quantityEdit;
    private ImageButton add;
    private ImageButton clearList;
    private long startTime;

    private List<String> resultsTitles;
    private List<Float> greenResults, blueResults, redResults, yellowResults;
    private List<String> resultTags = new ArrayList<>();

    //second page

    private List<TreeViewListHolder> allList, greenList, blueList, lightRedList, darkRedList, yellowList;

    public static Fragment zero, one;

    public static Fragment getInstance(int position){
        if(zero == null || one == null){
            FragmentCreateBarcode fragmentCreateBarcode = new FragmentCreateBarcode();
            Bundle bundle = new Bundle();
            bundle.putInt(POSITION_KEY, position);
            fragmentCreateBarcode.setArguments(bundle);
            if(zero == null) zero = fragmentCreateBarcode;
            else one = fragmentCreateBarcode;
            return fragmentCreateBarcode;
        }
        else{
            if(position == 0) return zero;
            else return one;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        FRAGMENT_POSITION = bundle.getInt(POSITION_KEY);
        activity = (ActivityCreateBacode) getActivity();
        databaseManager = activity.getDatabaseManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(FRAGMENT_POSITION == 0){
            View view = inflater.inflate(R.layout.create_barcode_dialog_layout, null);

            allAdapter = new ArrayAdapterBarcodeRecord(activity, R.layout.create_barcode_list_item, this);
            final ListView listview = view.findViewById(R.id.create_barcode_dialog_list_view);
            listview.setAdapter(allAdapter);

            allowedColors.add(0);
            allowedColors.add(1);
            allowedColors.add(2);
            allowedColors.add(3);

            barcodeEdit = view.findViewById(R.id.create_barcode_dialog_barcode);
            barcodeEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (after == 1 || (start == 0 && after > 5)) startTime = System.currentTimeMillis();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            barcodeEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        long diff = System.currentTimeMillis() - startTime;
                        if (diff < 300 && barcodeEdit.getText().length() > 5) {
//                            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) info(barcodeEdit.getText().toString());
                           add(barcodeEdit.getText().toString(), quantityEdit.getText().toString(), false);
                        }
                    }
                }
            });
            quantityEdit = view.findViewById(R.id.create_barcode_dialog_number);
            quantityEdit.setText("1");

            all = view.findViewById(R.id.create_barcode_summary_all);
            lightGreen = view.findViewById(R.id.create_barcode_summary_light_green);
            lightBlue = view.findViewById(R.id.create_barcode_summary_light_blue);
            lightRed = view.findViewById(R.id.create_barcode_summary_light_red);
            darkRed = view.findViewById(R.id.create_barcode_summary_dark_red);
            lightYellow = view.findViewById(R.id.create_barcode_summary_light_yellow);

            if(activity.isColorIdentificationAvailable()){
                Cursor resultCursor = databaseManager.getResultTable();
                int resultCursorSize = resultCursor.getCount();
                if(resultCursorSize > 0) {
                    resultCursor.moveToFirst();
                    greenResults = new ArrayList<>(resultCursorSize);
                    blueResults = new ArrayList<>(resultCursorSize);
                    redResults = new ArrayList<>(resultCursorSize);
                    yellowResults = new ArrayList<>(resultCursorSize);
                    int index = resultCursor.getColumnIndex(DatabaseManager.TAG_FIELD);
                    resultsTitles = new ArrayList<>();
                    resultsTitles.add(getString(R.string.records_count));
                    resultsTitles.add(getString(R.string.sum_count));
                    for(int i = 0; i < resultCursorSize; i++){
                        String tag = resultCursor.getString(index);
                        Cursor title = databaseManager.getTagTitle(tag);
                        if(title.getCount() > 0){
                            title.moveToFirst();
                            resultsTitles.add(title.getString(title.getColumnIndex(DatabaseManager.IS_TITLE_FIELD)));
                        }
                        else resultsTitles.add(getString(R.string.value));

                        resultTags.add(tag);
                        greenResults.add(0.0f);
                        blueResults.add(0.0f);
                        redResults.add(0.0f);
                        yellowResults.add(0.0f);
                        resultCursor.moveToNext();
                    }
                }
                resultCursor.close();

                Cursor yellowRecordsSum = databaseManager.getDataQuantitySum(activity.getQuantityTag());
                yellowRecordsSum.moveToFirst();
                LIGHT_YELLOW = yellowRecordsSum.getInt(yellowRecordsSum.getColumnIndex(DatabaseManager.AGR_CNT_FIELD));
                yellowRecordsSum.close();
                yellowAdapter = new ArrayAdapterBarcodeRecord(activity, R.layout.create_barcode_list_item, this);

                Cursor searchTagCursor = databaseManager.getFirstSerachTag();
                searchTagCursor.moveToFirst();
                String searchTag = searchTagCursor.getString(searchTagCursor.getColumnIndex(DatabaseManager.TAG_FIELD));
                searchTagCursor.close();

                String quantityTag = activity.getQuantityTag();
                Cursor cursor = databaseManager.getAllMainTableRecords();
                cursor.moveToFirst();
                int size = cursor.getCount(), idColumn = cursor.getColumnIndex(DatabaseManager.ID),
                        valueColumn = cursor.getColumnIndex(searchTag), quantityColumn = cursor.getColumnIndex(quantityTag);
                COUNT_LY = size;
                for(int i = 0; i < size; i++){
                    int id = cursor.getInt(idColumn);
                    String value = cursor.getString(valueColumn);
                    float quantity = cursor.getFloat(quantityColumn);
                    if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) yellowAdapter.add(new BarcodeRecord(id, value, quantity, quantity, -1));
                    else yellowAdapter.add(new BarcodeRecord(id, value, quantity, quantity, 4));
                    for(int j = 0; j < resultCursorSize; j++){
                        Float f = cursor.getFloat(cursor.getColumnIndex(resultTags.get(j)));
                        yellowResults.set(j, yellowResults.get(j) + f);
                    }
                    cursor.moveToNext();
                }
                cursor.close();
            }
            else{
                all.setEnabled(false);
                lightGreen.setEnabled(false);
                lightBlue.setEnabled(false);
                lightRed.setEnabled(false);
                darkRed.setEnabled(false);
                lightYellow.setEnabled(false);
            }

            final TextView textViewState = view.findViewById(R.id.create_barcode_dialog_text_state);
            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);

            all.setText(String.format(Locale.US, getString(R.string.create_barcode_color_all), 0.0f));
            all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ADAPTER_ID == 1){
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                    lightGreen.setChecked(true);
                    lightBlue.setChecked(true);
                    lightRed.setChecked(true);
                    darkRed.setChecked(true);
                }
            });
            lightGreen.setText(String.valueOf(LIGHT_GREEN));
            lightGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(ADAPTER_ID == 1){
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                    if(isChecked) allowedColors.add(0);
                    else allowedColors.remove(Integer.valueOf(0));
                    allAdapter.notifyDataSetChanged();
                }
            });
            lightBlue.setText(String.valueOf(LIGHT_BLUE));
            lightBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(ADAPTER_ID == 1){
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                    if(isChecked) allowedColors.add(1);
                    else allowedColors.remove(Integer.valueOf(1));
                    allAdapter.notifyDataSetChanged();
                }
            });
            lightRed.setText(String.valueOf(LIGHT_RED));
            lightRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(ADAPTER_ID == 1){
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                    if(isChecked) allowedColors.add(2);
                    else allowedColors.remove(Integer.valueOf(2));
                    allAdapter.notifyDataSetChanged();
                }
            });
            darkRed.setText(String.valueOf(DARK_RED));
            darkRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(ADAPTER_ID == 1){
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) textViewState.setText(R.string.barcode_state_0_alter);
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                    if(isChecked) allowedColors.add(3);
                    else allowedColors.remove(Integer.valueOf(3));
                    allAdapter.notifyDataSetChanged();
                }
            });
            lightYellow.setText(String.format(Locale.US, "%.2f" ,LIGHT_YELLOW));
            lightYellow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ADAPTER_ID == 0){
                        allowedColorsCopy = new ArrayList<>(allowedColors);
                        lightGreen.setChecked(false);
                        lightBlue.setChecked(false);
                        lightRed.setChecked(false);
                        darkRed.setChecked(false);
                        ADAPTER_ID = 1;
                        listview.setAdapter(yellowAdapter);
                        setControlEnabled(false);

                        Cursor databaseCreationDateCursor = databaseManager.getDownloadDate();
                        if(databaseCreationDateCursor != null){
                            databaseCreationDateCursor.moveToFirst();
                            String databaseCreationDate = databaseCreationDateCursor.getString(databaseCreationDateCursor.getColumnIndex(DatabaseManager.TAG_FIELD));
                            textViewState.setText(getString(R.string.barcode_state_1) + " " + databaseCreationDate);
                            databaseCreationDateCursor.close();
                        }
                        else textViewState.setText(R.string.barcode_state_1);
//                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
//                            barcodeEdit.setEnabled(false);
//                            add.setEnabled(true);
//                        }
                    }
                    else{
                        ADAPTER_ID = 0;
                        listview.setAdapter(allAdapter);
                        setControlEnabled(true);
                        for(Integer i: allowedColorsCopy){
                            switch (i){
                                case 0:
                                    lightGreen.setChecked(true);
                                    break;
                                case 1:
                                    lightBlue.setChecked(true);
                                    break;
                                case 2:
                                    lightRed.setChecked(true);
                                    break;
                                case 3:
                                    darkRed.setChecked(true);
                                    break;
                            }
                        }
                        textViewState.setText(R.string.barcode_state_0);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
//                            barcodeEdit.setEnabled(false);
//                            quantityEdit.setEnabled(false);
//                            add.setEnabled(false);
                            textViewState.setText(R.string.barcode_state_0_alter);
                        }
                        else textViewState.setText(R.string.barcode_state_0);
                    }
                }
            });

            StringListData data = activity.getData();
            final String inputName = activity.getInputName();
            if(inputName != null || data.getList().size() > 0){
                if(data.getList().size() == 0) activity.getFileManager().readFromFile(inputName, data);
                List<String> values = data.getList();
                for(String i: values){
                    int commaPosition = i.lastIndexOf(",");
                    String barcode = i.substring(0, commaPosition), quantity = i.substring(commaPosition + 1);
                    add(barcode, quantity, true);
                }
                activity.setIsSaved(true);
                data.clear();
            }

            add = view.findViewById(R.id.create_barcode_dialog_add_barcode);
            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) add.setImageResource(R.drawable.ic_action_search);
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) info(barcodeEdit.getText().toString());
                    add(barcodeEdit.getText().toString(), quantityEdit.getText().toString(), false);
                }
            });
            final ImageButton save = view.findViewById(R.id.create_barcode_save);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int size = allAdapter.getCount();
                    if(size > 0) {
                        save(size);
                        Toast.makeText(activity, R.string.data_saved, Toast.LENGTH_SHORT).show();
                    }
                    else Toast.makeText(activity, R.string.create_barcode_warning_list_empty, Toast.LENGTH_SHORT).show();
                }
            });
            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1])) save.setEnabled(false);
            clearList = view.findViewById(R.id.create_barcode_clear_list);
            clearList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (allAdapter.getCount() > 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(R.string.warning);
                        builder.setMessage(R.string.create_barcode_clear_list);
                        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int size = allAdapter.getCount();
                                for(int i = size - 1; i >= 0; i--){
                                    delete(allAdapter.getItem(i));
                                }
                                if(activity.isColorIdentificationAvailable()){
                                    updateAllColors();
                                    updateYellowColor();
                                }
                            }
                        });
                        builder.setNegativeButton(R.string.no, null);
                        builder.create().show();
                    }
                    else Toast.makeText(activity,R.string.create_barcode_warning_list_empty, Toast.LENGTH_SHORT).show();
                }
            });

            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
//                lightGreen.setEnabled(false);
//                lightBlue.setEnabled(false);
//                lightRed.setEnabled(false);
//                darkRed.setEnabled(false);
//                lightYellow.setEnabled(false);
                save.setEnabled(false);
//                all.setEnabled(View.GONE);
//                barcodeEdit.setEnabled(false);
//                quantityEdit.setEnabled(false);
//                add.setEnabled(false);
//                clearList.setVisibility(View.GONE);
//                view.findViewById(R.id.create_barcode_quantity_text).setVisibility(View.GONE);

//                ADAPTER_ID = 1;
//                listview.setAdapter(yellowAdapter);
            }


            return view;
        }
        else{
            if(activity.isColorIdentificationAvailable()){
                Cursor cursor = databaseManager.getResultTableCount();
                cursor.moveToFirst();
                int resultSize = cursor.getInt(cursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD));
                cursor.close();

                View returnView = inflater.inflate(R.layout.create_barcode_summary, null);
                LinearLayout resultView = returnView.findViewById(R.id.summary_container_layout);

                TreeNode root = TreeNode.root();
                allList = new ArrayList<>();
                TreeNode mainNode = new TreeNode(getString(R.string.summary_all_description)).setViewHolder(new TreeViewListHolder(activity));
                resultSize += 2;
                AndroidTreeView tView;
                greenList = new ArrayList<>();
                blueList = new ArrayList<>();
                lightRedList = new ArrayList<>();
                darkRedList = new ArrayList<>();
                yellowList = new ArrayList<>();

                if(!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) {
                    for (int i = 0; i < resultSize; i++) {
                        TreeViewListHolder holder = new TreeViewListHolder(activity);
                        TreeNode node = new TreeNode("").setViewHolder(holder);
                        mainNode.addChild(node);
                        allList.add(holder);
                    }
                    root.addChild(mainNode);
                    tView= new AndroidTreeView(getActivity(), root);
                    tView.setDefaultContainerStyle(R.style.tree_all, true);
                    resultView.addView(tView.getView());

                    root = TreeNode.root();

                    mainNode = new TreeNode(getString(R.string.summary_lignt_green_description)).setViewHolder(new TreeViewListHolder(activity));
                    for (int i = 0; i < resultSize; i++) {
                        TreeViewListHolder holder = new TreeViewListHolder(activity);
                        TreeNode node = new TreeNode("").setViewHolder(holder);
                        mainNode.addChild(node);
                        greenList.add(holder);
                    }
                    root.addChild(mainNode);
                    tView = new AndroidTreeView(getActivity(), root);
                    tView.setDefaultContainerStyle(R.style.tree_light_green, true);
                    resultView.addView(tView.getView());

                    root = TreeNode.root();

                    mainNode = new TreeNode(getString(R.string.summary_lignt_blue_description)).setViewHolder(new TreeViewListHolder(activity));
                    for (int i = 0; i < resultSize; i++) {
                        TreeViewListHolder holder = new TreeViewListHolder(activity);
                        TreeNode node = new TreeNode("").setViewHolder(holder);
                        mainNode.addChild(node);
                        blueList.add(holder);
                    }
                    root.addChild(mainNode);
                    tView = new AndroidTreeView(getActivity(), root);
                    tView.setDefaultContainerStyle(R.style.tree_light_blue, true);
                    resultView.addView(tView.getView());

                    root = TreeNode.root();

                    mainNode = new TreeNode(getString(R.string.summary_lignt_red_description)).setViewHolder(new TreeViewListHolder(activity));
                    for (int i = 0; i < resultSize; i++) {
                        TreeViewListHolder holder = new TreeViewListHolder(activity);
                        TreeNode node = new TreeNode("").setViewHolder(holder);
                        mainNode.addChild(node);
                        lightRedList.add(holder);
                    }
                    root.addChild(mainNode);
                    tView = new AndroidTreeView(getActivity(), root);
                    tView.setDefaultContainerStyle(R.style.tree_light_red, true);
                    resultView.addView(tView.getView());

                    root = TreeNode.root();

                    mainNode = new TreeNode(getString(R.string.summary_dark_red_description)).setViewHolder(new TreeViewListHolder(activity));
                    for (int i = 0; i < 2; i++) {
                        TreeViewListHolder holder = new TreeViewListHolder(activity);
                        TreeNode node = new TreeNode("").setViewHolder(holder);
                        mainNode.addChild(node);
                        darkRedList.add(holder);
                    }
                    root.addChild(mainNode);
                    tView = new AndroidTreeView(getActivity(), root);
                    tView.setDefaultContainerStyle(R.style.tree_dark_red, true);
                    resultView.addView(tView.getView());
                }

                root = TreeNode.root();
                if(!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) mainNode = new TreeNode(getString(R.string.summary_lignt_yellow_description)).setViewHolder(new TreeViewListHolder(activity));
                else mainNode = new TreeNode(getString(R.string.summary_search)).setViewHolder(new TreeViewListHolder(activity));
                for(int i = 0; i < resultSize; i++){
                    TreeViewListHolder holder = new TreeViewListHolder(activity);
                    TreeNode node = new TreeNode("").setViewHolder(holder);
                    mainNode.addChild(node);
                    yellowList.add(holder);
                }
                root.addChild(mainNode);
                tView = new AndroidTreeView(getActivity(), root);
                if(!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) tView.setDefaultContainerStyle(R.style.tree_light_yellow, true);
                resultView.addView(tView.getView());

                return returnView;
            }
            else return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    private void save(int size){
        if(!activity.isSaved()) {
            StringListData stringListData = activity.getData();
            for (int i = 0; i < size; i++) {
                stringListData.add(allAdapter.getItem(i).mergeData());
            }
            FileManager fileManager = activity.getFileManager();
            String in = activity.getInputName();
            String newName = in == null ? String.valueOf(fileManager.getFilesCount()) + ", " +
                    new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime())
                    + "." + StringListData.class.getSimpleName() : in;
            activity.setInputName(newName);
            fileManager.writeToFile(newName, stringListData);
            String currentFolderName = fileManager.getCurrentFolderName();
            fileManager.goBack();
            HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
            hashMapManager.getHashMap();
            hashMapManager.put(fileManager.getChildLocalPath(currentFolderName), false);
            hashMapManager.saveChanges();
            fileManager.goAhead(currentFolderName);
            stringListData.clear();
            activity.setIsSaved(true);
        }
    }

    public void fillTreeView(List<String> allList, List<String> greenList, List<String> blueList, List<String> lightRedList,
                             List<String> darkRedList, List<String> yellowList){
        int size = allList.size();
        if(this.allList != null) {

            if (!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
                for (int i = 0; i < size; i++) {
                    TreeViewListHolder holder = this.allList.get(i);
                    TextView textView = holder.getTextView();
                    if (textView == null) holder.setV(allList.get(i));
                    else textView.setText(allList.get(i));
                }

            for (int i = 0; i < size; i++) {
                TreeViewListHolder holder = this.greenList.get(i);
                TextView textView = holder.getTextView();
                if (textView == null) holder.setV(greenList.get(i));
                else textView.setText(greenList.get(i));
            }

            for (int i = 0; i < size; i++) {
                TreeViewListHolder holder = this.blueList.get(i);
                TextView textView = holder.getTextView();
                if (textView == null) holder.setV(blueList.get(i));
                else textView.setText(blueList.get(i));
            }

            for (int i = 0; i < size; i++) {
                TreeViewListHolder holder = this.lightRedList.get(i);
                TextView textView = holder.getTextView();
                if (textView == null) holder.setV(lightRedList.get(i));
                else textView.setText(lightRedList.get(i));
            }

            for (int i = 0; i < 2; i++) {
                TreeViewListHolder holder = this.darkRedList.get(i);
                TextView textView = holder.getTextView();
                if (textView == null) holder.setV(darkRedList.get(i));
                else textView.setText(darkRedList.get(i));
            }
        }
            for (int i = 0; i < size; i++) {
                TreeViewListHolder holder = this.yellowList.get(i);
                TextView textView = holder.getTextView();
                if (textView == null) holder.setV(yellowList.get(i));
                else textView.setText(yellowList.get(i));
            }
        }
    }

    public ArrayAdapterBarcodeRecord getAllAdapter(){
        return allAdapter;
    }

//    private void info(String value){
//        if(!activity.isSearchAvailable()){
//            Toast.makeText(activity, R.string.warning_no_search, Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(!value.equals("")) {
//            Cursor cursor = databaseManager.info(value, false);
//            if (cursor.getCount() > 0) {
//                cursor.moveToFirst();
//
//
//                int size = allAdapter.getCount();
//                boolean isInList = false;
//                if(activity.isColorIdentificationAvailable()) {
//
//                    for (int i = 0; i < size; i++) {
//                        BarcodeRecord record = allAdapter.requestData(i);
//                        if (record.getDataId() = id) {
//                            if (activity.isDuplicate()) {
//                                //add to top
//                                allAdapter.remove(record);
//                                allAdapter.insert(record);
//
//                                if(!record.getDefaultValue().equals(value)) record.setValue(value);
//
//                                if (activity.isSaved()) activity.setIsSaved(false);
//
//                                isInList = true;
//                            } else {
//                                Toast.makeText(activity, R.string.warning_no_duplicates, Toast.LENGTH_SHORT).show();
//                                isInList = true;
//                            }
//                            break;
//                        }
//                    }
//
//                    if (!isInList) {
//                        BarcodeRecord record = new BarcodeRecord(id, barcodeEdit.getText().toString(),);
//                        if (activity.isUnidentified()) {
//                            moveTo(record.getColor(), record.getColor() == 3 ? record.getDbQuantity() : record.getDbQuantity());
//                            updateAllColors();
//                            if (add) allAdapter.add(record);
//                            else allAdapter.insert(record);
//
//                            if (activity.isSaved()) activity.setIsSaved(false);
//                            if (activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add)
//                                save(allAdapter.getCount());
//                        } else {
//                            if (record.getColor() != 3) {
//                                moveTo(record.getColor(), record.getDbQuantity());
//                                updateAllColors();
//                                if (add) allAdapter.add(record);
//                                else allAdapter.insert(record);
//
//                                if (activity.isSaved()) activity.setIsSaved(false);
//                                if (activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add)
//                                    save(allAdapter.getCount());
//                            } else
//                                Toast.makeText(activity, R.string.warning_no_unidentified_records, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//
////                int id = cursor.getInt(cursor.getColumnIndex(DatabaseManager.ID));
////                int size = yellowAdapter.getCount();
////                for (int i = 0; i < size; i++) {
////                    BarcodeRecord record = yellowAdapter.requestData(i);
////                    if (record.getDataId() == id) {
//////                        yellowAdapter.remove(record);
////                        if(!record.getDefaultValue().equals(value)) record.setValue(value);
////                        allAdapter.insert(record);
////                        break;
////                    }
////                }
//            } else Toast.makeText(activity, R.string.warning_search, Toast.LENGTH_SHORT).show();
//            cursor.close();
//        }
//        else Toast.makeText(activity, R.string.warning_search, Toast.LENGTH_SHORT).show();
//
//        barcodeEdit.setText("");
//        barcodeEdit.post(new Runnable() {
//            @Override
//            public void run() {
//                barcodeEdit.requestFocus();
//            }
//        });
//    }

    public void add(String value, String quantity, boolean add){
        if (value.equals("") || quantity.equals(""))
            Toast.makeText(activity, activity.getString(R.string.create_barcode_warning_fields_are_not_filled), Toast.LENGTH_SHORT).show();
        else{
            int size = allAdapter.getCount();
            boolean isInList = false;
            float q = Float.parseFloat(quantity);
            if(activity.isColorIdentificationAvailable()){
                int id ;
                Cursor idCursor = databaseManager.search(value, false);
                if(idCursor.getCount() > 0){
                    idCursor.moveToFirst();
                    id = idCursor.getInt(idCursor.getColumnIndex(DatabaseManager.ID));
                    idCursor.close();
                }
                else id = -1;
                for (int i = 0; i < size; i++) {
                    BarcodeRecord record = allAdapter.getItem(i);
                    int itemId = record.getId();
                    if ((id != -1 && itemId != -1 && id == itemId) || record.getValue().equals(value)) {
                        if(activity.isDuplicate()) {
                            if(!record.getValue().equals(value)) record.setValue(value);
                            //add to top
                            allAdapter.remove(record);
                            allAdapter.insert(record);

                            if(activity.isSaved()) activity.setIsSaved(false);

                            record.addQuantity(q);
                            getUpdated(record, q);
                            allAdapter.notifyDataSetChanged();
                            isInList = true;
                            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add) save(allAdapter.getCount());
                        }
                        else{
                            Toast.makeText(activity, R.string.warning_no_duplicates, Toast.LENGTH_SHORT).show();
                            isInList = true;
                        }
                        break;
                    }
                }

                if (!isInList){
                    BarcodeRecord record = getNew(value, q);
                    if(activity.isUnidentified()){
                        moveTo(record.getColor(), record.getColor() == 3 ? record.getQuantity() : record.getDbQuantity());
                        updateAllColors();
                        if(add) allAdapter.add(record);
                        else allAdapter.insert(record);

                        if(activity.isSaved()) activity.setIsSaved(false);
                        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add) save(allAdapter.getCount());
                    }
                    else{
                        if(record.getColor() != 3){
                            moveTo(record.getColor(), record.getDbQuantity());
                            updateAllColors();
                            if(add) allAdapter.add(record);
                            else allAdapter.insert(record);

                            if(activity.isSaved()) activity.setIsSaved(false);
                            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add) save(allAdapter.getCount());
                        }
                        else Toast.makeText(activity, R.string.warning_no_unidentified_records, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else{
                for (int i = 0; i < size; i++) {
                    BarcodeRecord record = allAdapter.getItem(i);
                    if (record.getValue().equals(value)) {
                        if(activity.isDuplicate()) {
                            allAdapter.remove(record);
                            allAdapter.insert(record);

                            if(activity.isSaved()) activity.setIsSaved(false);

                            record.addQuantity(q);
                            allAdapter.notifyDataSetChanged();
                            isInList = true;
                            if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add) save(allAdapter.getCount());
                        }
                        else{
                            Toast.makeText(activity, R.string.warning_no_duplicates, Toast.LENGTH_SHORT).show();
                            isInList = true;
                        }
                        break;
                    }
                }
                if(!isInList){
                    if(add) allAdapter.add(new BarcodeRecord(value, q));
                    else allAdapter.insert(new BarcodeRecord(value, q));

                    if(activity.isSaved()) activity.setIsSaved(false);
                    if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1]) && !add) save(allAdapter.getCount());
                }
            }

            barcodeEdit.setText("");
            quantityEdit.setText("1");
            barcodeEdit.post(new Runnable() {
                @Override
                public void run() {
                    barcodeEdit.requestFocus();
                }
            });
        }
    }

    public BarcodeRecord getNew(String value, float q){
        BarcodeRecord barcodeRecord = new BarcodeRecord(value, q);
        Cursor dataCursor = databaseManager.search(value, false);
        int id = -1, color, size = dataCursor.getCount();
        int resultTagsSize = resultTags.size();
        if(size == 0) color = 3;
        else{
            dataCursor.moveToFirst();
            if(dataCursor.getCount() > 1) barcodeRecord.setSome(true);
            float databaseQuantity = dataCursor.getFloat(dataCursor.getColumnIndex(activity.getQuantityTag()));
            barcodeRecord.setDbQuantity(databaseQuantity);
            if (Math.abs(databaseQuantity - q) < 0.0001){
                color = 0;
                for(int i = 0; i < resultTagsSize; i++){
                    Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                    greenResults.set(i, greenResults.get(i) + f);
                }
            }
            else if (databaseQuantity > q){
                color = 1;
                for(int i = 0; i < resultTagsSize; i++){
                    Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                    blueResults.set(i, blueResults.get(i) + f);
                }
            }
            else{
                color = 2;
                for(int i = 0; i < resultTagsSize; i++){
                    Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                    redResults.set(i, redResults.get(i) + f);
                }
            }
            id = dataCursor.getInt(dataCursor.getColumnIndex(DatabaseManager.ID));
        }
        barcodeRecord.setColor(color);
        barcodeRecord.setId(id);

        if((color == 0 || color == 1 || color == 2) && !activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])){
            int yellowAdapterCount = yellowAdapter.getCount();
            for(int i = 0; i < yellowAdapterCount; i++){
                BarcodeRecord record = yellowAdapter.getItem(i);
                if(record.getId() == id){
                    yellowAdapter.remove(record);
                    break;
                }
            }
            moveFrom(4, barcodeRecord.getDbQuantity());
            updateYellowColor();
            for(int i = 0; i < resultTagsSize; i++){
                Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                yellowResults.set(i, yellowResults.get(i) - f);
            }
        }
        dataCursor.close();
        return barcodeRecord;
    }

    public void getUpdated(BarcodeRecord record, float adjustment){
        Cursor dataCursor = databaseManager.search(record.getValue(), false);
        int color, size  = dataCursor.getCount();
        float q = record.getQuantity();
        if(size == 0) color = 3;
        else{
            dataCursor.moveToFirst();
            float databaseQuantity = record.getDbQuantity();
            if (Math.abs(databaseQuantity - q) < 0.0001) color = 0;
            else if (databaseQuantity > q) color = 1;
            else color = 2;
        }

        int oldColor = record.getColor();
        if(oldColor != color){
            int resultTagsSize = resultTags.size();
//            if(oldColor == 0){
//                BarcodeRecord barcodeRecord = new BarcodeRecord(record);
//                barcodeRecord.setColor(4);
//                barcodeRecord.addQuantity(-adjustment);
//                yellowAdapter.insert(barcodeRecord);
//                moveTo(4, barcodeRecord.getDbQuantity());
//                updateYellowColor();
//                for(int i = 0; i < resultTagsSize; i++){
//                    Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
//                    yellowResults.set(i, yellowResults.get(i) + f);
//                }
//            }
//            else if(color == 0){
//                int yellowAdapterCount = yellowAdapter.getCount();
//                int id = record.getDataId();
//                for(int i = 0; i < yellowAdapterCount; i++){
//                    BarcodeRecord barcodeRecord = yellowAdapter.requestData(i);
//                    if(barcodeRecord.getDataId() == id){
//                        yellowAdapter.remove(barcodeRecord);
//                        break;
//                    }
//                }
//                moveFrom(4, q);
//                updateYellowColor();
//                for(int i = 0; i < resultTagsSize; i++){
//                    Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
//                    yellowResults.set(i, yellowResults.get(i) - f);
//                }
//            }
            switch (oldColor){
                case 0:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        greenResults.set(i, greenResults.get(i) - f);
                    }
                    break;
                case 1:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        blueResults.set(i, blueResults.get(i) - f);
                    }
                    break;
                case 2:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        redResults.set(i, redResults.get(i) - f);
                    }
                    break;
            }
            switch (color){
                case 0:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        greenResults.set(i, greenResults.get(i) + f);
                    }
                    break;
                case 1:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        blueResults.set(i, blueResults.get(i) + f);
                    }
                    break;
                case 2:
                    for(int i = 0; i < resultTagsSize; i++){
                        Float f = dataCursor.getFloat(dataCursor.getColumnIndex(resultTags.get(i)));
                        redResults.set(i, redResults.get(i) + f);
                    }
                    break;
            }

            moveFrom(oldColor, record.getDbQuantity());
            moveTo(color, record.getDbQuantity());
            updateAllColors();
            record.setColor(color);
        }
        else if (color == 3){
            moveTo(3, adjustment);
            updateAllColors();
        }
        dataCursor.close();
    }

    public void delete(BarcodeRecord record){
        if(activity.isSaved()) activity.setIsSaved(false);

        int color = record.getColor();
        float quantity = color == 3 ? record.getQuantity() : record.getDbQuantity();
        moveFrom(color, quantity);
        allAdapter.remove(record);
        updateAllColors();
        Cursor cursor = databaseManager.search(record.getValue(), false);
        cursor.moveToFirst();
        int resultTagsSize = resultTags.size();
        if(!activity.getCurrentMode().equals(ActivityCreateBacode.modes[2])) {
            switch (color) {
                case 0:
                    for (int i = 0; i < resultTagsSize; i++) {
                        Float f = cursor.getFloat(cursor.getColumnIndex(resultTags.get(i)));
                        greenResults.set(i, greenResults.get(i) - f);
                        yellowResults.set(i, yellowResults.get(i) + f);
                    }
                    moveTo(4, quantity);
                    updateYellowColor();
                    record.setColor(4);
                    yellowAdapter.insert(record);
                    break;
                case 1:
                    for (int i = 0; i < resultTagsSize; i++) {
                        Float f = cursor.getFloat(cursor.getColumnIndex(resultTags.get(i)));
                        blueResults.set(i, blueResults.get(i) - f);
                        yellowResults.set(i, yellowResults.get(i) + f);
                    }
                    moveTo(4, quantity);
                    updateYellowColor();
                    record.setColor(4);
                    yellowAdapter.insert(record);
                    break;
                case 2:
                    for (int i = 0; i < resultTagsSize; i++) {
                        Float f = cursor.getFloat(cursor.getColumnIndex(resultTags.get(i)));
                        redResults.set(i, redResults.get(i) - f);
                        yellowResults.set(i, yellowResults.get(i) + f);
                    }
                    moveTo(4, quantity);
                    updateYellowColor();
                    record.setColor(4);
                    yellowAdapter.insert(record);
                    break;
            }
        }
        cursor.close();
        if(activity.getCurrentMode().equals(ActivityCreateBacode.modes[1])) save(allAdapter.getCount());
    }

    private void moveFrom(int color, float quantity){
        switch (color){
            case 0:
                LIGHT_GREEN -= quantity;
                --COUNT_LG;
                break;
            case 1:
                LIGHT_BLUE -= quantity;
                --COUNT_LB;
                break;
            case 2:
                LIGHT_RED -= quantity;
                --COUNT_LR;
                break;
            case 3:
                DARK_RED -= quantity;
                --COUNT_DR;
                break;
            case 4:
                LIGHT_YELLOW -= quantity;
                --COUNT_LY;
                break;
        }
    }

    private void moveTo(int color, float quantity){
        switch (color){
            case 0:
                LIGHT_GREEN += quantity;
                ++COUNT_LG;
                break;
            case 1:
                LIGHT_BLUE += quantity;
                ++COUNT_LB;
                break;
            case 2:
                LIGHT_RED += quantity;
                ++COUNT_LR;
                break;
            case 3:
                DARK_RED += quantity;
                ++COUNT_DR;
                break;
            case 4:
                LIGHT_YELLOW += quantity;
                ++COUNT_LY;
                break;
        }
    }

    private void updateAllColors(){
        lightGreen.setText(String.format(Locale.US, "%.2f" ,LIGHT_GREEN));
        lightBlue.setText(String.format(Locale.US, "%.2f" ,LIGHT_BLUE));
        lightRed.setText(String.format(Locale.US, "%.2f" ,LIGHT_RED));
        darkRed.setText(String.format(Locale.US, "%.2f" ,DARK_RED));
        all.setText(String.format(Locale.US, getString(R.string.create_barcode_color_all), LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
    }

    private void updateYellowColor(){
        lightYellow.setText(String.format(Locale.US, "%.2f" ,LIGHT_YELLOW));
    }

    public int getAdapterId(){
        return ADAPTER_ID;
    }

    private void setControlEnabled(boolean value){
        barcodeEdit.setEnabled(value);
        quantityEdit.setEnabled(value);
        add.setEnabled(value);
        clearList.setEnabled(value);
    }

    public DatabaseManager getDatabaseManager(){
        return databaseManager;
    }

    public List<Integer> getAllowedColors(){
        return allowedColors;
    }

    public List<String> getAllResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_LG + COUNT_LB + COUNT_LR + COUNT_DR));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,LIGHT_GREEN + LIGHT_BLUE + LIGHT_RED + DARK_RED));
        int size = resultTags.size();
        for(int i = 0; i < size; i++){
            result.add(resultsTitles.get(i + 2) + ": " + String.format(Locale.US, "%.3f" ,greenResults.get(i) + blueResults.get(i) + redResults.get(i)));
        }
        return result;
    }

    public List<String> getGreenResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_LG));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,LIGHT_GREEN));
        int size = resultTags.size();
        for(int i = 0; i < size; i++){
            result.add(resultsTitles.get(i + 2) + ": " + String.format(Locale.US, "%.3f" ,greenResults.get(i)));
        }
        return result;
    }

    public List<String> getBlueResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_LB));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,LIGHT_BLUE));
        int size = resultTags.size();
        for(int i = 0; i < size; i++){
            result.add(resultsTitles.get(i + 2) + ": " + String.format(Locale.US, "%.3f" ,blueResults.get(i)));
        }
        return result;
    }

    public List<String> getLightRedResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_LR));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,LIGHT_RED));
        int size = resultTags.size();
        for(int i = 0; i < size; i++){
            result.add(resultsTitles.get(i + 2) + ": " + String.format(Locale.US, "%.3f" ,redResults.get(i)));
        }
        return result;
    }

    public List<String> getDarkResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_DR));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,DARK_RED));
        return result;
    }

    public List<String> getYellowResults(){
        List<String> result = new ArrayList<>();
        result.add(resultsTitles.get(0) + ": " + String.valueOf(COUNT_LY));
        result.add(resultsTitles.get(1) + ": " + String.format(Locale.US, "%.3f" ,LIGHT_YELLOW));
        int size = resultTags.size();
        for(int i = 0; i < size; i++){
            result.add(resultsTitles.get(i + 2) + ": " + String.format(Locale.US, "%.3f" ,yellowResults.get(i)));
        }
        return result;
    }

    public ArrayList<String> getMergedStrings(){
        ArrayList<String> result = new ArrayList<>();
        int size = allAdapter.getCount();
        for(int i = 0; i < size; i++){
            BarcodeRecord record = allAdapter.getItem(i);
            result.add(record.getValue() + "," + String.valueOf(record.getQuantity()));
        }
        return result;
    }
}
