package com.example.thebeginning;

import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Константин on 04.04.2016.
 */
public class PhotoBarcodeData extends PhotoData{

    protected String barcode;

    public PhotoBarcodeData(){
    }

    public void write(FileOutputStream outputStream){
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(jpegBytes);
            objectOutputStream.writeUTF(barcode);
            objectOutputStream.close();
        } catch (IOException e) {
            Log.v(PhotoBarcodeData.class.toString(), e.toString());
        }
    }

    public void read(FileInputStream inputStream){
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            jpegBytes = (byte[]) objectInputStream.readObject();
            barcode = objectInputStream.readUTF();
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            Log.v(PhotoData.class.toString(), e.toString());
        }
    }

    public String getBarcode(){
        return barcode;
    }

    public byte[] getBarcodeBytes(){
        return barcode.getBytes();
    }

    public void setBarcode(String barcode){
        this.barcode = barcode;
    }
}
