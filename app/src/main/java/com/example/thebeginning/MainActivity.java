package com.example.thebeginning;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thebeginning.activities.BaseAppCompatActivity;
import com.example.thebeginning.adapters.ChooseDialogAdapter;
import com.example.thebeginning.adapters.DrawerLayoutAdapter;
import com.example.thebeginning.data_types.GlobalSettings;
import com.example.thebeginning.repository.IniFileParser;
import com.example.thebeginning.settings.SettingsActivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends BaseAppCompatActivity {

    public static final int REQUEST_CODE_CameraPreview = 1;
    public static final int REQUEST_CODE_SettingsActivity = 2;
    private static final int GET_PERMISSIONS = 0;

    public static GlobalSettings globalSettings;

    public static String externalStoragePath;

    private String currentMode;
    private MenuItem programModeItem;

    private DrawerLayout drawerLayout;
    private ListView drawerLayoutList;

    private ContentManager contentManager;

    private boolean isInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissionsToGrant = new ArrayList<>();

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                permissionsToGrant.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                permissionsToGrant.add(Manifest.permission.CAMERA);

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                permissionsToGrant.add(Manifest.permission.RECORD_AUDIO);

            List<String> toGrant = new ArrayList<>();
            for (String i: permissionsToGrant){
                if(!ActivityCompat.shouldShowRequestPermissionRationale(this, i)){
                    toGrant.add(i);
                }
            }

            if(toGrant.size() != 0) ActivityCompat.requestPermissions(this, toGrant.toArray(new String[toGrant.size()]), GET_PERMISSIONS);
            else initialize();
        }
        else{
            initialize();
        }
    }

    private void initialize(){
        externalStoragePath = getStoragePath();
        Toolbar toolbar = findViewById(R.id.main_activity_toolbar);
        LinearLayout fabLayout = findViewById(R.id.fab_layout);
        setSupportActionBar(toolbar);

        FileManager fileManager = new FileManager(externalStoragePath + SettingsActivity.getPathOnSdCard(this), "");
        fileManager.createFolder();
        contentManager = new ContentManager(this, fileManager, findViewById(R.id.main_activity_list_view), toolbar, fabLayout, externalStoragePath);

//        File file = new File(externalStoragePath, "mode.ini");
//        try {
//            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
//
//            bufferedWriter.write("modes=Full,Inventory,Photo");
//            bufferedWriter.write("\r\n");
//            bufferedWriter.write("languages=Ru,Ch,En");
//            bufferedWriter.write("\r\n");
//            bufferedWriter.write("jpegQuality=6");
//            bufferedWriter.write("\r\n");
//
//            bufferedWriter.close();
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }


        FileManager ini = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(this), "/mode");

        IniFileParser iniFileParser = new IniFileParser(new File(ini.getChildFullPath(IniFileParser.INI_FILE_NAME)));
        externalStoragePath = fileManager.getRootParent();
        globalSettings = iniFileParser.getGlobalSettings();

        String[] modes = getResources().getStringArray(R.array.program_modes);

        if(globalSettings.getModes().size() != 1){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.choose_mode);
            builder.setCancelable(false);

            ChooseDialogAdapter chooseDialogAdapter = new ChooseDialogAdapter(MainActivity.this, globalSettings.getModes(), modes);
            builder.setAdapter(chooseDialogAdapter, (dialog, which) -> {
                changeCurrentMode(chooseDialogAdapter.getItem(which), GlobalSettings.getModeTitle(chooseDialogAdapter.getItem(which), modes));
                dialog.dismiss();
            });

            builder.create().show();
        }
        else currentMode = globalSettings.getModes().get(0);

        drawerLayout = findViewById(R.id.main_activity_drawer_layout);
        drawerLayoutList = findViewById(R.id.main_activity_drawer_layout_list);

        updateDrawerLayout();

        isInitialized = true;
    }

    private void updateDrawerLayout(){
        String[] modes = getResources().getStringArray(R.array.program_modes);
        List<String> items = new ArrayList<>(globalSettings.getModes());
        items.add(DrawerLayoutAdapter.SETTINGS);

        String settings = getString(R.string.settings);

        drawerLayoutList.setAdapter(new DrawerLayoutAdapter(this, items, modes, settings));
        drawerLayoutList.setOnItemClickListener((parent, view, position, id) -> {
            String mode = (String) parent.getItemAtPosition(position);
            String modeTitle = GlobalSettings.getModeTitle(mode, modes);
            if(modeTitle != null){
                changeCurrentMode(mode, modeTitle);
                contentManager.update();
            }
            else action_setting();

            drawerLayout.closeDrawer(drawerLayoutList);
        });
    }

    private String getStoragePath(){
        com.example.thebeginning.extended.repository.FileManager fileManager = new com.example.thebeginning.extended.repository.FileManager();

        List<String> externalDirectories = fileManager.getExternalDirectories(this);

        if (externalDirectories.size() == 0) {
            return fileManager.getDefaultDirectory();
        }
        else {
            return externalDirectories.get(0);
        }
    }

    private void changeCurrentMode(String newMode, String modeTitle){
        currentMode = newMode;
        Toast.makeText(MainActivity.this, String.format(Locale.US, getString(R.string.current_mode), modeTitle), Toast.LENGTH_SHORT).show();
        programModeItem.setTitle(modeTitle);
    }

    public String getCurrentMode() {
        return currentMode;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GET_PERMISSIONS:
                boolean success = true;

                for (Integer i: grantResults){
                    if(i == PackageManager.PERMISSION_DENIED){
                        success = false;
                        break;
                    }
                }

                if(!success){
                    Toast.makeText(this, R.string.permissions_not_given, Toast.LENGTH_SHORT).show();
                    finish();
                }
                else initialize();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        contentManager.onCreateToolbarMenu(getMenuInflater(), menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        programModeItem = menu.findItem(R.id.action_program_mode);
        if(currentMode != null) programModeItem.setTitle(GlobalSettings.getModeTitle(currentMode, getResources().getStringArray(R.array.program_modes)));

        contentManager.onPrepareToolbarMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.action_program_mode:
                drawerLayout.openDrawer(drawerLayoutList);
                return true;
            case R.id.action_select_items:
                contentManager.startSelectingItems(-1);
                return true;
            case R.id.action_transfer_to_server:
                contentManager.sendItems(true);
                return true;
            case R.id.action_catalog_properties:
                FileManager fileManager = new FileManager(contentManager.getRoot(), contentManager.getLocalPath());
                String name = fileManager.getCurrentFolderName();
                fileManager.goBack();
                FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(this) + "/mode",
                        fileManager.getLocalPath());
                Bundle bundle = new Bundle();
                bundle.putParcelable(ContentManager.xmlSettingsKey, xmlSettings);
                fileManager.goAhead(name);
                bundle.putParcelable(ContentManager.fileManagerKey, fileManager);
                CatalogProperties catalogProperties = new CatalogProperties();
                catalogProperties.setArguments(bundle);
                catalogProperties.show(this.getFragmentManager(), CatalogProperties.class.toString());
                return true;
            case R.id.action_transmit:
                contentManager.sendItems(false);
//                contentManager.sendItems(ContentManager.SD_CARD);
                return true;
//            case R.id.action_transfer_to_server_https:
//                contentManager.sendItems(ContentManager.HTTPS);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void action_setting(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SettingsActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        if(requestCode == REQUEST_CODE_CameraPreview){
            if(resultCode == RESULT_OK){
                String prefix = null;
                if(intent != null) prefix = intent.getStringExtra(CameraPreview.FILE_NAME_PREFIX);

                if(prefix != null)
                        contentManager.addFiles(new FileManager(externalStoragePath + "/TBF", "").getFiles(), prefix);
                else contentManager.addFiles(new FileManager(externalStoragePath + "/TBF", "").getFiles());


                String name = contentManager.getCurrentFolderName();
                contentManager.goBack();
                HashMapManager hashMapManager = new HashMapManager(new FileManager(contentManager));
                hashMapManager.getHashMap();
                hashMapManager.put(contentManager.getChildLocalPath(name), false);
                hashMapManager.saveChanges();
                contentManager.goAhead(name);
            }
            System.gc();
        }
        else if (requestCode == REQUEST_CODE_SettingsActivity){
            updateDrawerLayout();
            invalidateOptionsMenu();
            contentManager.update();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isInitialized) contentManager.update();
    }

    public void updateDataSet(){
        contentManager.notifyDataSetChanged();
    }

    public void updateContent(String name, String state){
        contentManager.addListItem(new ListItem(name, state));
    }

    public void updateContent(){
        contentManager.update();
    }
}