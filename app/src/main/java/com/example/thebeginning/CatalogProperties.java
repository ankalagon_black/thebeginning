package com.example.thebeginning;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.pinball83.maskededittext.MaskedEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 30.04.2016.
 */
public class CatalogProperties extends DialogFragment {
    private FileManager fileManager;
    private FileManager xmlManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
        xmlManager = bundle.getParcelable(ContentManager.xmlSettingsKey);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Dialog dialog = getDialog();

        final MainActivity mainActivity = (MainActivity) getActivity();
        View mainView = inflater.inflate(R.layout.create_folder_main, null);
        LinearLayout linearLayout = mainView.findViewById(R.id.main_view);
        Toolbar toolbar = mainView.findViewById(R.id.create_folder_toolbar);
        toolbar.setTitle(R.string.folder_creation);

        List<XmlRecord> records = ((XmlRecordData) xmlManager.readFromFile("mode.xml", new XmlRecordData())).getRecords();
        final List<MaskedEditText> inputFields = new ArrayList<>();
        final XmlRecordData data = (XmlRecordData) fileManager.readFromFile("mode.xml", new XmlRecordData());
        final XmlRecord mainData = data.getRecordByParentTag("SharedData");
        final XmlRecord paketsData = data.getRecordByParentTag("PaketsData");

        boolean isContent = false;
        if(mainData != null){
            List<TagRecord> mainDataTags = mainData.getTagRecords();
            List<TagRecord> paketsDataTags;
            if(paketsData == null) paketsDataTags = new ArrayList<>();
            else paketsDataTags = paketsData.getTagRecords();

            if(mainDataTags.size() > 0) {
                isContent = true;
                for (XmlRecord i : records) {
                    final String parentTag = i.getParentTag();
                    if (parentTag.equals("Field")) {
                        String tag = i.getTagValueByTag("Tag");
                        if (tag != null) {
                            String type = i.getTagValueByTag("Type"), title = i.getTagValueByTag("Title");
                            if (type == null) type = "String";
                            if (title == null) title = getString(R.string.input_a_value);
                            View view;
                            View titleView;
                            final MaskedEditText maskedEditText;
                            String editableStr = i.getTagValueByTag("Editable");
                            boolean editable = editableStr != null && editableStr.equals("1");
                            switch (type) {
                                case "String":
                                    view = inflater.inflate(R.layout.create_folder_edit_text, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_text);
                                    if(!editable) maskedEditText.setFocusable(false);
                                    addField(linearLayout, inputFields, view, maskedEditText,
                                            i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title,
                                            findValue(tag, mainDataTags, paketsDataTags));
                                    break;
                                case "Date":
                                    view = inflater.inflate(R.layout.create_folder_edit_date, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_date);
                                    maskedEditText.setFocusable(false);
                                    if(editable){
                                        ImageButton imageButton = view.findViewById(R.id.create_folder_edit_date_button);
                                        imageButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String date = maskedEditText.getUnmaskedText();
                                                if (date.equals(""))
                                                    date = new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime());
                                                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                                                final DatePicker datePicker = new DatePicker(mainActivity);
                                                datePicker.updateDate(Integer.parseInt(date.substring(4, 8)), Integer.parseInt(date.substring(2, 4)) - 1, Integer.parseInt(date.substring(0, 2)));
                                                datePicker.setCalendarViewShown(false);
                                                builder.setTitle(R.string.choose_a_date);
                                                builder.setView(datePicker);
                                                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        int year = datePicker.getYear(), month = datePicker.getMonth() + 1, day = datePicker.getDayOfMonth();
                                                        maskedEditText.setMaskedText((day < 10 ? "0" + String.valueOf(day) : String.valueOf(day)) +
                                                                (month < 10 ? "0" + String.valueOf(month) : String.valueOf(month)) + String.valueOf(year));
                                                        dialog.dismiss();
                                                    }
                                                });
                                                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                AlertDialog alertDialog = builder.create();
                                                alertDialog.show();
                                            }
                                        });
                                        ImageButton currentDateButton = view.findViewById(R.id.create_folder_edit_date_current);
                                        currentDateButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                maskedEditText.setMaskedText(new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime()));
                                            }
                                        });
                                    }
                                    addField(linearLayout, inputFields, view, maskedEditText,
                                            i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title,
                                            findValue(tag, mainDataTags, paketsDataTags));
                                    break;
                                case "DateTime":
                                    view = inflater.inflate(R.layout.create_folder_edit_date_time, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_date);
                                    maskedEditText.setFocusable(false);
                                    if(editable) {
                                        ImageButton imageButton = view.findViewById(R.id.create_folder_edit_date_button);
                                        imageButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String date = maskedEditText.getUnmaskedText();
                                                if (date.equals(""))
                                                    date = new SimpleDateFormat("ddMMyyyyHHmm").format(Calendar.getInstance().getTime());
                                                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                                                View dateTimeView = inflater.inflate(R.layout.date_time, null);
                                                final DatePicker datePicker = dateTimeView.findViewById(R.id.date_picker);
                                                final TimePicker timePicker = dateTimeView.findViewById(R.id.time_picker);
                                                timePicker.setIs24HourView(true);
                                                datePicker.updateDate(Integer.parseInt(date.substring(4, 8)), Integer.parseInt(date.substring(2, 4)) - 1, Integer.parseInt(date.substring(0, 2)));
                                                timePicker.setCurrentHour(Integer.parseInt(date.substring(8, 10)));
                                                timePicker.setCurrentHour(Integer.parseInt(date.substring(10, 12)));
                                                builder.setTitle(R.string.choose_date_and_time);
                                                builder.setView(dateTimeView);
                                                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        int year = datePicker.getYear(), month = datePicker.getMonth() + 1, day = datePicker.getDayOfMonth(),
                                                                hour = timePicker.getCurrentHour(), minute = timePicker.getCurrentMinute();
                                                        maskedEditText.setMaskedText((day < 10 ? "0" + String.valueOf(day) : String.valueOf(day)) +
                                                                (month < 10 ? "0" + String.valueOf(month) : String.valueOf(month)) + String.valueOf(year) +
                                                                (hour < 10 ? "0" + String.valueOf(hour) : String.valueOf(hour)) +
                                                                (minute < 10 ? "0" + String.valueOf(minute) : String.valueOf(minute)));
                                                        dialog.dismiss();
                                                    }
                                                });
                                                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                AlertDialog alertDialog = builder.create();
                                                alertDialog.show();
                                            }
                                        });
                                        ImageButton currentDateButton = view.findViewById(R.id.create_folder_edit_date_current);
                                        currentDateButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                maskedEditText.setMaskedText(new SimpleDateFormat("ddMMyyyyHHmm").format(Calendar.getInstance().getTime()));
                                            }
                                        });
                                    }
                                    addField(linearLayout, inputFields, view, maskedEditText,
                                            i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title,
                                            findValue(tag, mainDataTags, paketsDataTags));
                                    break;
                                case "Integer":
                                    view = inflater.inflate(R.layout.create_folder_edit_integer, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_integer);
                                    if(!editable) maskedEditText.setFocusable(false);
                                    addField(linearLayout, inputFields, view, maskedEditText,
                                            i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title,
                                            findValue(tag, mainDataTags, paketsDataTags));
                                    break;
                                case "Double":
                                    view = inflater.inflate(R.layout.create_folder_edit_double, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_double);
                                    if(!editable) maskedEditText.setFocusable(false);
                                    addField(linearLayout, inputFields, view, maskedEditText,
                                            i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title,
                                            findValue(tag, mainDataTags, paketsDataTags));
                                    break;
                                case "List":
                                    view = null;
                                    titleView = null;
                                    ImageButton imageButton = null;
                                    setList(linearLayout, view, titleView, title, i, inflater, imageButton, mainActivity, tag, inputFields, mainDataTags, editable, paketsDataTags);
                                    break;
                                case "Boolean":
                                    view = inflater.inflate(R.layout.create_folder_edit_text_boolean, null);
                                    titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                                    maskedEditText = view.findViewById(R.id.create_folder_edit_text_boolean);
                                    maskedEditText.setEnabled(false);
                                    final CheckBox checkBox = view.findViewById(R.id.create_folder_check_box_boolean);
                                    String value = findValue(tag, mainDataTags, paketsDataTags);
                                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                            if(isChecked){
                                                maskedEditText.setText(getString(R.string.logic_yes));
                                                checkBox.setText(getString(R.string.yes));
                                            }
                                            else{
                                                maskedEditText.setText(getString(R.string.logic_no));
                                                checkBox.setText(getString(R.string.no));
                                            }

                                        }
                                    });
                                    if(value.equals("1")) checkBox.setChecked(true);
                                    else checkBox.setChecked(false);
                                    addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                            tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, value);
                                    break;
                                default:
                                    Toast.makeText(mainActivity, String.format(Locale.US, getString(R.string.error_in_settings_file_type_is_not_recognized), type), Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                    } else if (parentTag.equals("Common")) {
                        View view = inflater.inflate(R.layout.create_folder_text_view, null);
                        TextView textView = view.findViewById(R.id.create_folder_text_view);
                        String title = i.getTagValueByTag("Title");
                        if (title == null) textView.setText(R.string.input_data);
                        else textView.setText(title);
                        textView.setTextSize(18);
                        textView.setTextColor(Color.BLUE);
                        linearLayout.addView(view, 0);
                    }
                }
            }
        }

        if(isContent){
            View buttonsView = inflater.inflate(R.layout.create_folder_buttons, null);
            Button cancel = buttonsView.findViewById(R.id.create_folder_cancel);
            Button confirm = buttonsView.findViewById(R.id.create_folder_confirm);
            confirm.setText(R.string.save);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean dataChanged = false;
                    for (MaskedEditText i : inputFields) {
                        String string = i.getText().toString();
                        TagRecord tagRecord = mainData.getTagRecordByTag((String) i.getTag());
                        if(tagRecord != null) {
                            string = string.equals("") ? " " : string;
                            if(!dataChanged && !tagRecord.getValue().equals(string)) dataChanged = true;
                            tagRecord.setValue(string);
                        }
                        else{
                            if(paketsData != null) {
                                TagRecord paketsTag = paketsData.getTagRecordByTag((String) i.getTag());
                                if (paketsTag != null) {
                                    string = string.equals("") ? " " : string;
                                    if (!dataChanged && !paketsTag.getValue().equals(string))
                                        dataChanged = true;
                                    paketsTag.setValue(string);
                                }
                            }
                        }
                    }
                    fileManager.writeToFile("mode.xml", data);
                    if(dataChanged){
                        String name = fileManager.getCurrentFolderName();
                        fileManager.goBack();
                        HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
                        hashMapManager.getHashMap();
                        hashMapManager.put(fileManager.getChildLocalPath(name), false);
                        hashMapManager.saveChanges();
                        fileManager.goAhead(name);
                        mainActivity.updateContent();
                    }
                    dialog.dismiss();
                }
            });
            linearLayout.addView(buttonsView);
        }
        else{
            View view = inflater.inflate(R.layout.catalog_properties_no_view, null);
            Button button = view.findViewById(R.id.catalog_properties_no_view_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            linearLayout.addView(view);
        }
        return mainView;
    }

    private void addField(LinearLayout root, List<MaskedEditText> inputFields,View view, MaskedEditText maskedEditText, String required, String tag,
                          View titleView, TextView textView, String title, String value){
        maskedEditText.setTag(tag);
        boolean flag = required != null && required.equals("1");
        maskedEditText.setRequired(flag);
        maskedEditText.setText(value);
        if(flag){
            textView.setTextColor(Color.RED);
            textView.setText("*" + title);
        }
        else textView.setText(title);
        inputFields.add(maskedEditText);
        root.addView(titleView);
        root.addView(view);
    }

    private String findValue(String tag, List<TagRecord> list, List<TagRecord> list2){
        String result = "";
        List<TagRecord> someList = new ArrayList<>();
        someList.addAll(list);
        someList.addAll(list2);
        for(TagRecord i: someList){
            if(i.getTag().equals(tag)){
                result = i.getValue();
                break;
            }
        }
        return result;
    }

    private void setList(LinearLayout linearLayout, View view,View titleView, String title, XmlRecord record, final LayoutInflater inflater, ImageButton imageButton, final MainActivity mainActivity,
                         String tag, List<MaskedEditText> inputFields, List<TagRecord> mainDataTags, boolean editable, List<TagRecord> paketsDataTags){
        view = inflater.inflate(R.layout.create_folder_edit_list, null);
        LinearLayout fieldsLayout = view.findViewById(R.id.create_folder_edit_list_fields);
        titleView = inflater.inflate(R.layout.create_folder_text_view, null);
        TextView textView = titleView.findViewById(R.id.create_folder_text_view);
        title = record.getTagValueByTag("Title");
        textView.setText(title == null ? getString(R.string.list) : title);
        textView.setTextColor(Color.BLUE);
        fieldsLayout.addView(titleView);
        final List<TagRecord> listTags = record.getTagRecordByMask("Tag");
        XmlRecordData infoData = (XmlRecordData) xmlManager.readFromFile(record.getTagValueByTag("FileList"), new XmlRecordData());
        XmlRecord commonData = infoData.getRecordByParentTag("Common");
        List<TagRecord> allTags = commonData.getTagRecordByMask("Tag");
        List<XmlRecord> allRecords = infoData.getRecords();
        final List<String> titles = new ArrayList<>();
        final List<MaskedEditText> editTexts = new ArrayList<>();
        List<String> finalTags = new ArrayList<>();
        for(TagRecord i: listTags){
            for(TagRecord j: allTags){
                if(i.getTag().equals(j.getTag())){
                    for(XmlRecord k: allRecords){
                        if(k.getParentTag().equals("Field") && k.getTagValueByTag("Tag").equals(j.getValue())){
                            tag = i.getValue();
                            View innerLayout = inflater.inflate(R.layout.create_folder_edit_list_item, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            title = k.getTagValueByTag("Title");
                            final MaskedEditText maskedEditText1 = innerLayout.findViewById(R.id.create_folder_edit_list);
                            maskedEditText1.setFocusable(false);
                            addField(fieldsLayout, inputFields, innerLayout, maskedEditText1, k.getTagValueByTag("Required"), tag, titleView,
                                    titleView.findViewById(R.id.create_folder_text_view), title,
                                    findValue(tag, mainDataTags, paketsDataTags));
                            titles.add(title == null ? "Поле" : title);
                            editTexts.add(maskedEditText1);
                            finalTags.add(j.getValue());
                        }
                    }
                }
            }
        }
        XmlRecordData listItemData = (XmlRecordData) xmlManager.readFromFile(commonData.getTagValueByTag("FileList"), new XmlRecordData());
        List<XmlRecord> listItemRecords = listItemData.getRecords();
        for(XmlRecord i: listItemRecords){
            if(i.getParentTag().equals("Field")){
                for(String j: finalTags){
                    String string = i.getTagValueByTag(j);
                    titles.add(string == null ? "-" : string);
                }
            }
        }
        imageButton = view.findViewById(R.id.create_folder_edit_list_button);
        if(editable) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final GridView gridView = (GridView) inflater.inflate(R.layout.create_folder_list_gridview, null);
                    int elementsPerRow = editTexts.size();
                    gridView.setNumColumns(elementsPerRow);
                    gridView.setAdapter(new ArrayAdapter<>(mainActivity, android.R.layout.simple_list_item_1, titles));
                    AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                    builder.setTitle(R.string.choose_a_value);
                    builder.setView(gridView);
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    String value = editTexts.get(0).getText().toString();
                    int startPosition = -1;
                    if (!value.equals("")) {
                        int size = titles.size();
                        for (int o = elementsPerRow; o < size; o += elementsPerRow) {
                            if (titles.get(o).equals(value)) {
                                startPosition = o;
                                break;
                            }
                        }
                    }
                    if (startPosition != -1) gridView.smoothScrollToPosition(startPosition);
                    final AlertDialog alertDialog = builder.create();
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            int elementsPerRow = editTexts.size();
                            if (position < elementsPerRow) return;
                            position -= position % elementsPerRow;
                            for (MaskedEditText editText : editTexts) {
                                editText.setText((String) parent.getItemAtPosition(position));
                                ++position;
                            }
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });
        }
        linearLayout.addView(view);
    }

//    private static class TagInfo{
//        private String tag;
//        private String type;
//        private boolean isEditable;
//        private String title;
//        private String source;
//
//        public TagInfo(String tag, String type, boolean isEditable, String title){
//            this.tag = tag;
//            this.type = type;
//            this.isEditable = isEditable;
//            this.title = title;
//        }
//
//        public TagInfo(String tag, String type, boolean isEditable, String title, String source){
//            this.tag = tag;
//            this.type = type;
//            this.isEditable = isEditable;
//            this.title = title;
//            this.source = source;
//        }
//
//        public String getTag() {
//            return tag;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public boolean isEditable() {
//            return isEditable;
//        }
//
//        public String getColumn(){
//            return title;
//        }
//
//        public String getSource() {
//            return source;
//        }
//    }
}
