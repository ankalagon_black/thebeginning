package com.example.thebeginning;

import android.app.Activity;
import android.app.FragmentManager;
import android.util.Log;

import java.io.File;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/**
 * Created by Константин on 27.03.2016.
 */
public class WifiTransmissionManager {
    private ExecutorService executorService;

    private SmbFileManager smbFileManager;
    private FileManager fileManager;

    private Activity activity;

    public WifiTransmissionManager(String ip, String path_on_server, String user_name, String user_password, FileManager fileManager, Activity activity){
        smbFileManager = new SmbFileManager("smb://" + ip + path_on_server, "", new NtlmPasswordAuthentication("", user_name, user_password));
        this.fileManager = fileManager;
        this.activity = activity;
    }

    public boolean synchronize(){
        executorService = Executors.newFixedThreadPool(1);
        Boolean result;
        try{
            Future<Boolean> future = executorService.submit(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    SmbFile[] smbFiles = smbFileManager.getFiles();
                    for (SmbFile i: smbFiles){
                        fileManager.createFolder(i.getName());
                    }
                    return true;
                }
            });
            result = future.get();
        }
        catch(Exception e){
            Log.v(WifiTransmissionManager.class.toString(), e.toString());
            result = false;
        }
        shutdown(executorService);
        return result;
    }

    public boolean isServerAvailable(){
        executorService = Executors.newFixedThreadPool(1);
        Boolean result;
        try{
            Future<Boolean> future = executorService.submit(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    smbFileManager.goTo(fileManager.getLocalPath());
                    return smbFileManager.createFolder();
                }
            });
            result = future.get();
        }
        catch(Exception e){
            Log.v(WifiTransmissionManager.class.toString(), e.toString());
            result = false;
        }
        shutdown(executorService);
        return result;
    }

    public void transmit(final List<ListItem> listItems, final ProgressBarDialog progressBarDialog){
        executorService = Executors.newFixedThreadPool(1);

        executorService.execute(new Runnable() {
            @Override
            public void run() {

                progressBarDialog.waitForCreation(true);
                progressBarDialog.initializeFolderBar(listItems.size());
                for (ListItem i : listItems) {
                    String name = i.getName();
                    String destinationName = smbFileManager.prepareXml(fileManager, name, activity);
                    if (smbFileManager.createFolder(destinationName)) {
                        smbFileManager.goAhead(destinationName);
                        fileManager.goAhead(name);
                        progressBarDialog.setCurrentFolder(name);
                        File[] files = fileManager.getFiles();
                        progressBarDialog.initializeFileBar(files.length);
                        for (File j : files) {
                            progressBarDialog.setCurrentFile(j.getName());
                            smbFileManager.writeFile(fileManager, j, progressBarDialog);
                            progressBarDialog.incrementFileBar();
                        }
                        fileManager.goBack();
                        if(!name.equals(destinationName)) smbFileManager.goBack(4);
                        else smbFileManager.goBack();

                        HashMapManager hashMapManager = new HashMapManager(fileManager);
                        hashMapManager.getHashMap();
                        hashMapManager.put(fileManager.getChildLocalPath(name), true);
                        hashMapManager.saveChanges();
                        i.setState(activity.getString(R.string.already_sent));
                        progressBarDialog.incrementFolderBar();
                    } else break;
                }
                progressBarDialog.finish();
                shutdown(executorService);
            }
        });
    }

    public void transmit(final ProgressBarDialog progressBarDialog){
        executorService = Executors.newFixedThreadPool(1);

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                progressBarDialog.waitForCreation(false);

                fileManager.createFolder();
                fileManager.deleteAll();
                try {
                    SmbFile[] smbFiles = smbFileManager.getFiles();
                    List<BSContainer> list = new ArrayList<>();
                    BSContainer container = new BSContainer();
                    container.setPath(fileManager.getChildLocalPath(fileManager.getCurrentFolderName()));
                    for(SmbFile i: smbFiles) copyRecursiveCall(i, container,list);
                    if(container.getFiles().size() > 0) list.add(container);

                    int size = list.size();
                    progressBarDialog.initializeFolderBar(size);
                    for(int i = size - 1; i >= 0; i--){
                        List<String> files = list.get(i).getFiles();
                        String path = list.get(i).getPath();
                        progressBarDialog.setCurrentFolder(path.substring(path.lastIndexOf("/") + 1));
                        progressBarDialog.initializeFileBar(files.size());
                        for(String j: files){
                            progressBarDialog.setCurrentFile(j);
                            fileManager.goTo(path);
                            smbFileManager.goTo(path);
                            SmbFile smbFile = smbFileManager.getFile(j);
                            if(smbFile != null) fileManager.writeToFile(j, smbFile, progressBarDialog);
                            progressBarDialog.incrementFileBar();
                        }
                        progressBarDialog.incrementFolderBar();
                    }

                } catch (SmbException | MalformedURLException | UnknownHostException e) {
                    Log.v(WifiTransmissionManager.class.toString(), e.toString());
                }

                progressBarDialog.finish();

                shutdown(executorService);
            }
        });
    }

    public void transmit(final ProgressBarDialog progressBarDialog, final String dbMode, final String dbData, final FileManager databaseFileManager,
                         final FragmentManager fragmentManager){
        executorService = Executors.newFixedThreadPool(1);

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                progressBarDialog.waitForCreation(false);

                fileManager.delete(dbMode);
                fileManager.delete(dbData);

                List<BSContainer> list = new ArrayList<>();
                BSContainer container = new BSContainer();
                container.setPath(fileManager.getLocalPath());
                container.addFile(dbMode);
                container.addFile(dbData);
                list.add(container);

                int size = list.size();
                progressBarDialog.initializeFolderBar(size);
                for(int i = size - 1; i >= 0; i--){
                    List<String> files = list.get(i).getFiles();
                    String path = list.get(i).getPath();
                    progressBarDialog.setCurrentFolder(path.substring(path.lastIndexOf("/") + 1));
                    progressBarDialog.initializeFileBar(files.size());
                    for(String j: files){
                        progressBarDialog.setCurrentFile(j);
                        fileManager.goTo(path);
                        smbFileManager.goTo(path);
                        SmbFile smbFile = smbFileManager.getFile(j);
                        if(smbFile != null) fileManager.writeToFile(j, smbFile, progressBarDialog);
                        progressBarDialog.incrementFileBar();
                    }
                    progressBarDialog.incrementFolderBar();
                }


                progressBarDialog.finish();

                DialogDatabaseUpdateProgressBar dialogDatabaseUpdateProgressBar = new DialogDatabaseUpdateProgressBar();
                dialogDatabaseUpdateProgressBar.show(fragmentManager, "1");
                dialogDatabaseUpdateProgressBar.waitForCreation();
                DatabaseManager.updateFromXmlFile(databaseFileManager, fileManager, dialogDatabaseUpdateProgressBar, dbMode, activity);
                dialogDatabaseUpdateProgressBar.finish();

                shutdown(executorService);
            }
        });
    }

    private void copyRecursiveCall(SmbFile smbFile, BSContainer container, List<BSContainer> list) throws SmbException {
        if(smbFile.isDirectory()){
            String name = smbFile.getName();
            fileManager.createFolder(smbFile.getName());
            SmbFile[] smbFiles = smbFile.listFiles();
            fileManager.goAhead(name.substring(0, name.lastIndexOf("/")));
            BSContainer bsContainer = new BSContainer();
            bsContainer.setPath(fileManager.getLocalPath());
            for(SmbFile i: smbFiles){
                copyRecursiveCall(i, bsContainer, list);
            }
            if(bsContainer.getFiles().size() > 0) list.add(bsContainer);
            fileManager.goBack();
        }
        else container.addFile(smbFile.getName());
    }

    private void shutdown(ExecutorService service){
        try{
            service.shutdown();
            service.awaitTermination(3, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
           Log.v(WifiTransmissionManager.class.toString(), e.toString());
        }
        finally {
            service.shutdownNow();
        }
    }

    private static class BSContainer{
        private String path;
        private List<String> files;

        public BSContainer(){
            files = new ArrayList<>();
        }

        public void setPath(String path){
            this.path = path;
        }

        public void addFile(String file){
            files.add(file);
        }

        public String getPath(){
            return path;
        }

        public List<String> getFiles(){
            return files;
        }
    }
}
