package com.example.thebeginning;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

/**
 * Created by Константин on 21.04.2016.
 */
public class XmlData extends AbstractData {

    private String content;

    public XmlData(){
    }

    @Override
    public void write(FileOutputStream outputStream) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(content);
            writer.close();

        } catch (IOException e) {
            Log.v(XmlData.class.toString(), e.toString());
        }
    }

    @Override
    public void read(FileInputStream inputStream) {
        try {
            StringWriter writer = new StringWriter();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null){
                writer.append(line);
                writer.append("\n");
            }
            content = writer.toString();
            reader.close();
            writer.close();
        }
        catch (IOException e) {
            Log.v(XmlData.class.toString(), e.toString());
        }
    }

    public void setContent(String content){
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }
}
