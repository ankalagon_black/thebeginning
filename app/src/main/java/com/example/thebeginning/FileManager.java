package com.example.thebeginning;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;


/**
 * Created by Константин on 03.04.2016.
 */
public class FileManager implements Parcelable {

    private String root, localPath;

    public FileManager(String root, String localPath){
        this.root = root;
        this.localPath = localPath;
    }

    public FileManager(FileManager fileManager){
        this(fileManager.getRoot(), fileManager.getLocalPath());
    }

    protected FileManager(Parcel in) {
        root = in.readString();
        localPath = in.readString();
    }

    public static final Creator<FileManager> CREATOR = new Creator<FileManager>() {
        @Override
        public FileManager createFromParcel(Parcel in) {
            return new FileManager(in);
        }

        @Override
        public FileManager[] newArray(int size) {
            return new FileManager[size];
        }
    };

    public void goAhead(String name){
        localPath = getChildLocalPath(name);
    }

    public void goBack(){
        localPath = getParentLocalPath();
    }

    public void goTo(String path){
        localPath = path;
    }

    public String getLocalPath(){
        return localPath;
    }

    public String getChildLocalPath(String name){
        return localPath + "/" + name;
    }

    public String getParentLocalPath(){
        return localPath.substring(0, localPath.lastIndexOf("/"));
    }

    public String getRoot(){
        return root;
    }

    public String getRootParent(){
        return root.substring(0, root.lastIndexOf("/"));
    }

    public String getFullPath(){
        return root + localPath;
    }

    public String getChildFullPath(String name){
        return root + getChildLocalPath(name);
    }

    public String getCurrentFolderName() {
        if(localPath.equals("")) return root.substring(root.lastIndexOf("/") + 1, root.length());
        else return localPath.substring(localPath.lastIndexOf("/") + 1, localPath.length());
    }

    public boolean createFolder(String name){
        File file = new File(getFullPath(), name);
        return file.mkdir();
    }

    public boolean createFolder(){
        File file = new File(getFullPath());
        return file.mkdirs();
    }

    public int getFilesCount(){
        File file = new File(getFullPath());
        return file.listFiles().length;
    }

    public File[] getFiles(){
        File file = new File(getFullPath());
        return file.listFiles();
    }

    public boolean exists(String name){
        File file = new File(getFullPath(), name);
        return file.exists();
    }

    public boolean isEmpty(){
        return getFilesCount() == 0;
    }

    public void delete(String name){
        deleteRecursiveCall(new File(getFullPath(), name));
    }

    public void delete(File file){
        deleteRecursiveCall(file);
    }

    public void deleteAll(){
        File[] files = getFiles();
        if(files != null) for(File i: files) deleteRecursiveCall(i);
    }

    private void deleteRecursiveCall(File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            for(File i: files) this.deleteRecursiveCall(i);
        }
        file.delete();
    }

    public void writeToFile(String name, AbstractData abstractData){
        File file = new File(getFullPath(), name);
        try{
            FileOutputStream outputStream = new FileOutputStream(file);
            abstractData.write(outputStream);
            outputStream.close();
        } catch(IOException e){
            Log.v(FileManager.class.toString(), e.toString());
        }
    }

    public void writeToFile(String name, SmbFile smbFile, ProgressBarDialog progressBarDialog){
        File file = new File(getFullPath(), name);
        try {
            progressBarDialog.initializeByteBar((int) smbFile.length() / 1024);
            int incrementValue = 8192 / 1024;
            byte[] buffer = new byte[8192];
            int bytesRead;
            SmbFileInputStream inputStream = new SmbFileInputStream(smbFile);
            FileOutputStream outputStream = new FileOutputStream(file);
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(incrementValue);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e){
            Log.v(FileManager.class.toString(), e.toString());
        }
    }

    public void writeFile(final FileManager fileManager, final File file, final ProgressBarDialog progressBarDialog){
        final ExtensionManager extensionManager = new ExtensionManager(file.getName());
        extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
            @Override
            public void onPhotoDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoData());
                byte[] jpegBytes = data.getJpegBytes();
                progressBarDialog.initializeByteBar(jpegBytes.length / 1024);
                writeBytes(extensionManager.getNameWithoutExtension(), ".jpeg", 8192, jpegBytes, progressBarDialog);
            }

            @Override
            public void onPhotoBarcodeDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoBarcodeData());
                String nameWithoutExtension = extensionManager.getNameWithoutExtension();
                byte[] jpegBytes = data.getJpegBytes(), barcodeBytes = data.getBarcodeBytes();
                progressBarDialog.initializeByteBar((jpegBytes.length + barcodeBytes.length) / 1024);
                writeBytes(nameWithoutExtension, ".jpeg", 8192, jpegBytes, progressBarDialog);
                writeBytes(nameWithoutExtension, ".tbf", 1024, barcodeBytes, progressBarDialog);
            }

            @Override
            public void onStringListDataEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".tbf", progressBarDialog);
            }

            @Override
            public void onHashMapDataEvent() {
            }

            @Override
            public void onFolderEvent() {

            }

            @Override
            public void onXmlDataEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".xml", progressBarDialog);
            }

            @Override
            public void on3gpEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeBytes(extensionManager.getNameWithoutExtension(), ".mp4", 8192, file, progressBarDialog);
            }

            @Override
            public void onTxtEvent() {
                progressBarDialog.initializeByteBar((int) file.length() / 1024);
                writeFile(extensionManager.getNameWithoutExtension(), file, ".txt", progressBarDialog);
            }
        });
        extensionManager.run();
    }

    public void writeFile(final FileManager fileManager, final File file){
        final ExtensionManager extensionManager = new ExtensionManager(file.getName());
        extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
            @Override
            public void onPhotoDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoData());
                byte[] jpegBytes = data.getJpegBytes();
                writeBytes(extensionManager.getNameWithoutExtension(), ".jpeg", 8192, jpegBytes);
            }

            @Override
            public void onPhotoBarcodeDataEvent() {
                AbstractData data = fileManager.readFromFile(file, new PhotoBarcodeData());
                String nameWithoutExtension = extensionManager.getNameWithoutExtension();
                byte[] jpegBytes = data.getJpegBytes(), barcodeBytes = data.getBarcodeBytes();
                writeBytes(nameWithoutExtension, ".jpeg", 8192, jpegBytes);
                writeBytes(nameWithoutExtension, ".tbf", 1024, barcodeBytes);
            }

            @Override
            public void onStringListDataEvent() {
                writeFile(extensionManager.getNameWithoutExtension(), file, ".tbf");
            }

            @Override
            public void onHashMapDataEvent() {
            }

            @Override
            public void onFolderEvent() {

            }

            @Override
            public void onXmlDataEvent() {
                writeFile(extensionManager.getNameWithoutExtension(), file, ".xml");
            }

            @Override
            public void on3gpEvent() {
                writeBytes(extensionManager.getNameWithoutExtension(), ".mp4", 8192, file);
            }

            @Override
            public void onTxtEvent() {
                writeFile(extensionManager.getNameWithoutExtension(), file, ".txt");
            }
        });
        extensionManager.run();
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, byte[] bytes, ProgressBarDialog progressBarDialog){
        try{
            File file = new File(getChildFullPath(nameWithoutExtension + extensionWithDot));
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            FileOutputStream outputStream = new FileOutputStream(file);
            int bytesRead;
            int incrementValue = volume / 1024;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(incrementValue);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, File file, ProgressBarDialog progressBarDialog){
        try{
            InputStream inputStream = new FileInputStream(file);
            File outputFile = new File(getChildFullPath(nameWithoutExtension + extensionWithDot));
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            int bytesRead;
            int incrementValue = volume / 1024;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(incrementValue);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }


    private void writeFile(String nameWithoutExtension, File file, String extension, ProgressBarDialog progressBarDialog){
        try{
            FileInputStream inputStream = new FileInputStream(file);
            File outputFile = new File(getChildFullPath(nameWithoutExtension + extension));
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
                progressBarDialog.incrementByteBar(1);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, byte[] bytes){
        try{
            File file = new File(getChildFullPath(nameWithoutExtension + extensionWithDot));
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            FileOutputStream outputStream = new FileOutputStream(file);
            int bytesRead;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }

    private void writeBytes(String nameWithoutExtension, String extensionWithDot, int volume, File file){
        try{
            InputStream inputStream = new FileInputStream(file);
            File outputFile = new File(getChildFullPath(nameWithoutExtension + extensionWithDot));
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            int bytesRead;
            byte[] buffer = new byte[volume];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }


    private void writeFile(String nameWithoutExtension, File file, String extension){
        try{
            FileInputStream inputStream = new FileInputStream(file);
            File outputFile = new File(getChildFullPath(nameWithoutExtension + extension));
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.v(SmbFileManager.class.toString(), e.toString());
        }
    }


    public AbstractData readFromFile(String name, AbstractData abstractData){
        File file = new File(getFullPath(), name);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            abstractData.read(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            Log.v(FileManager.class.toString(), e.toString());
        }
        return abstractData;
    }

    public AbstractData readFromFile(File file, AbstractData abstractData){
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            abstractData.read(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            Log.v(FileManager.class.toString(), e.toString());
        }
        return abstractData;
    }

    public String prepareXml(FileManager fileManager, String name, int level, Activity activity){
        if(level != 2) fileManager.goAhead(name);
        AbstractData abstractData = fileManager.readFromFile("mode.xml", new XmlData());
        String content = abstractData.getContent();
        String result = name;
        if(content != null) {
            int index = content.indexOf("<DivideByDates>") + 15;
            String divideByDates = content.substring(index, index + 1);
            if (divideByDates.equals("1")) {
                index = content.indexOf("<" + activity.getString(R.string.date_of_creation) + ">") + 14;

                String dateOfCreationStr = content.substring(index, index + 16);
                try {
                    Date dateOfCreation = new SimpleDateFormat("dd-MM-yyyy HH-mm").parse(dateOfCreationStr);
                    String formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateOfCreation);
                    result = formattedDate + "/" + result;
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            //StringBuilder stringBuilder = new StringBuilder(content);
            //if(content.contains("ДатаСинхронизации")){
            //    stringBuilder.replace(stringBuilder.indexOf("<ДатаСинхронизации>") + 19, stringBuilder.indexOf("</ДатаСинхронизации>"), strDate);
            //    stringBuilder.replace(stringBuilder.indexOf("<КаталогСинхронизации>") + 22, stringBuilder.indexOf("</КаталогСинхронизации>"), result);
            //}
            //else{
            //    index = stringBuilder.indexOf("</SystemData>") - 1;
            //    stringBuilder.insert(index, "<ДатаСинхронизации>" + strDate + "</ДатаСинхронизации>" + "\n" + "<КаталогСинхронизации>" + result + "</КаталогСинхронизации>" + "\n");
            //}
            //abstractData.setContent(stringBuilder.toString());
            //fileManager.writeToFile("mode.xml", abstractData);

            if (content.contains("<RewriteCatalogMode>")) {
                index = content.indexOf("RewriteCatalogMode") + 19;
                String rewrite = content.substring(index, index + 1);
                if(rewrite.equals("1")) delete(result);
            }
        }
        if(level != 2) fileManager.goBack();
        return result;
    }

    public String getUploadDir(String name){
        AbstractData abstractData = readFromFile("mode.xml", new XmlData());
        String content = abstractData.getContent();

        String result = name;

        if(content != null) {
            int index = content.indexOf("<DivideByDates>") + 15;
            String divideByDates = content.substring(index, index + 1);
            if (divideByDates.equals("1")) {
                index = content.indexOf("<ДатаСоздания>") + 14;

                String dateOfCreationStr = content.substring(index, index + 16);
                try {
                    Date dateOfCreation = new SimpleDateFormat("dd-MM-yyyy HH-mm").parse(dateOfCreationStr);
                    String formattedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateOfCreation);
                    result = formattedDate + "/" + result;
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public void duplicateModeXmlAsSent(File cacheDir, String result, Activity activity){
        AbstractData abstractData = readFromFile("mode.xml", new XmlData());
        String content = abstractData.getContent();
        String strDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(Calendar.getInstance().getTime());

        String syncDate = activity.getString(R.string.sync_date), syncCatalog = activity.getString(R.string.sync_catalog);

        StringBuilder stringBuilder = new StringBuilder(content);
        if(content.contains(syncDate)){
            stringBuilder.replace(stringBuilder.indexOf("<" + syncDate + ">") + 19, stringBuilder.indexOf("</" + syncDate + ">"), strDate);
            stringBuilder.replace(stringBuilder.indexOf("<" + syncCatalog + ">") + 22, stringBuilder.indexOf("</" + syncCatalog + ">"), result);
        }
        else{
            int index = stringBuilder.indexOf("</SystemData>") - 1;
            stringBuilder.insert(index, "<" + syncDate + ">" + strDate + "</" + syncDate + ">" + "\n" +
                    "<" + syncCatalog + ">" + result + "</" + syncCatalog + ">" + "\n");
        }
        abstractData.setContent(stringBuilder.toString());


        File file = new File(cacheDir, "mode.xml");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            abstractData.write(fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(root);
        dest.writeString(localPath);
    }
}
