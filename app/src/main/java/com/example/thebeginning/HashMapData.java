package com.example.thebeginning;

import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

/**
 * Created by Константин on 06.04.2016.
 */
public class HashMapData extends AbstractData {

    private HashMap<String, Boolean> hashMap;

    public HashMapData(){
    }

    @Override
    public void write(FileOutputStream outputStream) {
        try{
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(hashMap);
            objectOutputStream.close();
        }
        catch (IOException e){
            Log.v(HashMapData.class.toString(), e.toString());
        }
    }

    @Override
    public void read(FileInputStream inputStream){
        try{
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            hashMap = (HashMap<String, Boolean>) objectInputStream.readObject();
            objectInputStream.close();
        }
        catch (IOException | ClassNotFoundException e){
            Log.v(HashMapData.class.toString(), e.toString());
        }
    }

    public Boolean get(String key){
        return hashMap.get(key);
    }

    public void put(String key, Boolean value){
        hashMap.put(key, value);
    }

    public void remove(String key){
        hashMap.remove(key);
    }

    public void setHashMap(HashMap<String, Boolean> hashMap){
        this.hashMap = hashMap;
    }

    public boolean isHashMapNull(){
        return hashMap == null;
    }
}
