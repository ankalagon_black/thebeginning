package com.example.thebeginning;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thebeginning.data_types.GlobalSettings;
import com.example.thebeginning.extended.SendDataHttps;
import com.example.thebeginning.extended_v2.data_types.FilesToMove;
import com.example.thebeginning.extended_v2.dialogs.SelectStorageDialog;
import com.example.thebeginning.settings.SettingsActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Константин on 17.04.2016.
 */
public class ContentManager extends  FileManager {
    public static final String SMB = "0", HTTPS = "1", SD_CARD = "2";

    public static final String xmlSettingsKey = "ContentManager.xmlSettingsKey";
    public static final String fileManagerKey = "ContentManager.fileManager";
    public static final String listItemsNamesKey = "ContentManager.listItemsNamesKey";
    public static final String listItemPositionKey= "ContentManager.listItemPositionKey";
    public static final String listItemNameKey = "ContentManager.listItemNameKey";
    public static final String modeKey = "ContentManager.modeKey";
    public static final String changeHashMapKey = "ContentManager.changeHashMapKey";

    private ListView listView;
    private ListItemAdapter listItemAdapter;
    private Toolbar toolbar;
    private int level;
    private ActionMode actionMode;
    private boolean inSelectMode;
    private MainActivity activity;
    private FloatingActionButton fab, dbFab, addPhoto;
    private boolean lvlOneSelected;

    public ContentManager(final MainActivity activity, final FileManager fileManager, final ListView listView, Toolbar toolbar, LinearLayout fabLayout, final String externalStoragePath) {
        super(fileManager);
        level = 0;
        actionMode = null;
        inSelectMode = false;
        this.listView = listView;
        listItemAdapter = new ListItemAdapter(activity, createListItems(), R.drawable.ic_action_folder_tabs);
        this.listView.setAdapter(listItemAdapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!inSelectMode) {
                    ListItem listItem = (ListItem) parent.getItemAtPosition(position);
                    if (level == 2) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(fileManagerKey, ContentManager.this);
                        bundle.putStringArrayList(listItemsNamesKey, getListItemsNames());
                        bundle.putInt(listItemPositionKey, position);
                        Intent intent = new Intent(activity, ViewFileActivity.class);
                        intent.putExtras(bundle);
                        activity.startActivity(intent);
                    }
                    else {
                        ++level;
                        goAhead(listItem.getName());
                        update();
                        activity.invalidateOptionsMenu();
                    }
                } else {
                    view.setSelected(!view.isSelected());
                    lvlOneSelected = listView.getCheckedItemCount() == 1;
                }

            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (inSelectMode) {
                    listView.setItemChecked(position, !listView.isItemChecked(position));
                    lvlOneSelected = listView.getCheckedItemCount() == 1;
                }
                else {
                    lvlOneSelected = true;
                    startSelectingItems(position);
                }
                return true;
            }
        });
        this.listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        this.listView.setLongClickable(false);
        this.toolbar = toolbar;
        this.toolbar.setNavigationIcon(null);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                --level;
                goBack();
                update();
                activity.invalidateOptionsMenu();
            }
        });
        this.activity = activity;

        FloatingActionButton addFab = fabLayout.findViewById(R.id.main_activity_fab);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(level == 1){
                    FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", getLocalPath());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(xmlSettingsKey, xmlSettings);
                    bundle.putParcelable(fileManagerKey, ContentManager.this);
                    CreateFolderDialog createFolderDialog = new CreateFolderDialog();
                    createFolderDialog.setArguments(bundle);
                    createFolderDialog.show(ContentManager.this.activity.getFragmentManager(), CreateFolderDialog.class.toString());
                }
                else if(level == 2){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ContentManager.this.activity);
                    builder.setTitle(R.string.choose_file_type);
                    ListView modeList = new ListView(ContentManager.this.activity);
                    final String[] types = activity.getResources().getStringArray(R.array.file_options);
                    modeList.setAdapter(new ArrayAdapter<>(ContentManager.this.activity, android.R.layout.simple_list_item_1, types));
                    builder.setView(modeList);
                    builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = builder.create();
                    modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String type = (String) parent.getItemAtPosition(position);
                            String[] modes = activity.getResources().getStringArray(R.array.camera_activity_modes);
                            if (type.equals(types[0])) {
                                Intent intent = new Intent(ContentManager.this.activity, CameraPreview.class);
                                intent.putExtra(modeKey, modes[0]);
                                ContentManager.this.activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_CameraPreview);
                                alertDialog.dismiss();
                            } else if (type.equals(types[1])) {
                                Intent intent = new Intent(ContentManager.this.activity, CameraPreview.class);
                                intent.putExtra(modeKey, modes[1]);
                                ContentManager.this.activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_CameraPreview);
                                alertDialog.dismiss();
                            } else if (type.equals(types[2])) {
                                Intent intent = new Intent(ContentManager.this.activity, CameraPreview.class);
                                intent.putExtra(modeKey, modes[2]);
                                ContentManager.this.activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_CameraPreview);
                                alertDialog.dismiss();
                            } else if (type.equals(types[3])) {
                                CreateCommentaryDialog createCommentaryDialog = new CreateCommentaryDialog();
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putString(listItemNameKey, null);
                                createCommentaryDialog.setArguments(bundle);
                                createCommentaryDialog.show(activity.getFragmentManager(), CreateCommentaryDialog.class.toString());
                                alertDialog.dismiss();
                            } else if(type.equals(types[4])){
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putParcelable(xmlSettingsKey, xmlSettings);
                                bundle.putBoolean(changeHashMapKey, true);
                                bundle.putString(listItemNameKey, null);
                                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[0]);
                                Intent intent = new Intent(activity, ActivityCreateBacode.class);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                                alertDialog.dismiss();
                            }
                        }
                    });
                    alertDialog.show();
                }
            }
        });
        //check mode
        FloatingActionButton dbFab = fabLayout.findViewById(R.id.db_activity_fab);
        dbFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(level == 2){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ContentManager.this.activity);
                    builder.setTitle(R.string.choose_an_operation);
                    ListView modeList = new ListView(ContentManager.this.activity);
                    final String[] types = activity.getResources().getStringArray(R.array.accounting_operations);
                    modeList.setAdapter(new ArrayAdapter<>(ContentManager.this.activity, android.R.layout.simple_list_item_1, types));
                    builder.setView(modeList);
                    builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = builder.create();
                    modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String type = (String) parent.getItemAtPosition(position);
                            if (type.equals(types[3])) {
//                                File[] files = getFiles();
//                                String fileName = null;
//                                for(File i: files){
//                                    ExtensionManager extensionManager = new ExtensionManager(i.getColumn());
//                                    if(extensionManager.getExtension().equals(StringListData.class.getSimpleName())){
//                                        fileName = i.getColumn();
//                                        break;
//                                    }
//                                }
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putParcelable(xmlSettingsKey, xmlSettings);
                                bundle.putBoolean(changeHashMapKey, true);
//                                bundle.putString(listItemNameKey, fileName);
                                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[2]);
                                Intent intent = new Intent(activity, ActivityCreateBacode.class);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                                alertDialog.dismiss();
                            } else if (type.equals(types[0])) {
                                File[] files = getFiles();
                                String fileName = null;
                                for(File i: files){
                                    ExtensionManager extensionManager = new ExtensionManager(i.getName());
                                    if(extensionManager.getExtension().equals(StringListData.class.getSimpleName())){
                                        fileName = i.getName();
                                        break;
                                    }
                                }
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putParcelable(xmlSettingsKey, xmlSettings);
                                bundle.putBoolean(changeHashMapKey, true);
                                bundle.putString(listItemNameKey, fileName);
                                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[0]);
                                Intent intent = new Intent(activity, ActivityCreateBacode.class);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                                alertDialog.dismiss();

                            } else if (type.equals(types[1])) {
                                File[] files = getFiles();
                                String fileName = null;
                                for(File i: files){
                                    ExtensionManager extensionManager = new ExtensionManager(i.getName());
                                    if(extensionManager.getExtension().equals(StringListData.class.getSimpleName())){
                                        fileName = i.getName();
                                        break;
                                    }
                                }
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putParcelable(xmlSettingsKey, xmlSettings);
                                bundle.putBoolean(changeHashMapKey, true);
                                bundle.putString(listItemNameKey, fileName);
                                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[1]);
                                Intent intent = new Intent(activity, ActivityCreateBacode.class);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                                alertDialog.dismiss();
                            }
                            else if(type.equals(types[5])){
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                final FileManager xmlSettings = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                XmlRecordData xmlRecordData = (XmlRecordData) ContentManager.this.readFromFile("mode.xml", new XmlRecordData());
                                String prefix = null;
                                if(xmlRecordData != null){
                                    XmlRecord commomRecord = xmlRecordData.getRecordByParentTag("AdditionalData");
                                    if(commomRecord != null) {
                                        TagRecord prefixRecord = commomRecord.getTagRecordByTag("DBPatternFile");
                                        if (prefixRecord != null) prefix = prefixRecord.getValue();
                                    }
                                }
                                if(prefix == null) prefix = "pak_mode.xml";

                                final String finalPrefix = prefix;
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        DialogDatabaseUpdateProgressBar dialogDatabaseUpdateProgressBar = new DialogDatabaseUpdateProgressBar();
                                        dialogDatabaseUpdateProgressBar.show(activity.getFragmentManager(), "1");
                                        dialogDatabaseUpdateProgressBar.waitForCreation();
                                        DatabaseManager.updateFromXmlFile(ContentManager.this, xmlSettings, dialogDatabaseUpdateProgressBar, finalPrefix, activity);
                                        dialogDatabaseUpdateProgressBar.finish();
                                    }
                                }).start();
                            }
                            else if(type.equals(types[6])){
                                if(isWifiEnabled()) {
                                    FileManager tempFileManager = new FileManager(ContentManager.this);
                                    tempFileManager.goBack();
                                    FileManager mobileFileManager = new FileManager(externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity),
                                            "/mode" + "/" + tempFileManager.getCurrentFolderName());
                                    WifiTransmissionManager manager = new WifiTransmissionManager(SettingsActivity.getIp(activity),
                                            SettingsActivity.getServer_backwards_synchronization_path(activity),
                                            SettingsActivity.getUserName(activity),
                                            SettingsActivity.getUserPassword(activity),
                                            mobileFileManager, activity);
                                    if(manager.isServerAvailable()){
                                        XmlRecordData xmlRecordData = (XmlRecordData) ContentManager.this.readFromFile("mode.xml", new XmlRecordData());
                                        String prefix = null;
                                        if(xmlRecordData != null){
                                            XmlRecord commomRecord = xmlRecordData.getRecordByParentTag("AdditionalData");
                                            if(commomRecord != null) {
                                                TagRecord prefixRecord = commomRecord.getTagRecordByTag("DBPatternFile");
                                                if (prefixRecord != null) prefix = prefixRecord.getValue();
                                            }
                                        }

                                        if(prefix == null) prefix = "pak_mode.xml";
                                        String dbData = null;
                                        xmlRecordData = (XmlRecordData) mobileFileManager.readFromFile(prefix, new XmlRecordData());
                                        if(xmlRecordData != null){
                                            XmlRecord commomRecord = xmlRecordData.getRecordByParentTag("Common");
                                            if(commomRecord != null) {
                                                TagRecord prefixRecord = commomRecord.getTagRecordByTag("DBDataFile");
                                                if (prefixRecord != null) dbData = prefixRecord.getValue();
                                            }
                                        }
                                        if(dbData == null) dbData = "pak.xml";
                                        ProgressBarDialog progressBarDialog = new ProgressBarDialog();
                                        progressBarDialog.show(activity.getFragmentManager(), ProgressBarDialog.class.getSimpleName());
                                        manager.transmit(progressBarDialog, prefix, dbData, ContentManager.this, activity.getFragmentManager());
                                    }
                                    else{
                                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                        builder.setTitle(R.string.message).setMessage(R.string.unable_to_connect_to_the_server);
                                        builder.setPositiveButton(R.string.ok, null);
                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }
                                }
                                else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                    builder.setTitle(R.string.message).setMessage(R.string.no_wifi_connection);
                                    builder.setPositiveButton(R.string.ok, null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                            else if(type.equals(types[7])){
                                String name = ContentManager.this.getCurrentFolderName();
                                ContentManager.this.goBack();
                                ContentManager.this.delete(DatabaseManager.DATABASE_NAME);
                                ContentManager.this.delete(DatabaseManager.DATABASE_NAME + "-journal");
                                ContentManager.this.goAhead(name);
                                Toast.makeText(activity, R.string.DB_was_deleted, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    alertDialog.show();
                }
            }
        });


        addPhoto = fabLayout.findViewById(R.id.add_photo);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ContentManager.this.activity);

            final XmlRecordData baseData = ((XmlRecordData) ContentManager.this.readFromFile("mode.xml", new XmlRecordData()));

            List<XmlRecord> records = baseData.getRecordsByParentTag("PhotoField");

            int size = records.size();
                final  String[] titles = new String[records.size()];
                final String[] modes = new String[records.size()];

                for (int i = 0; i < size; i++){
                    XmlRecord xmlrecord = records.get(i);
                    String description = xmlrecord.getTagValueByTag("Description");
                    String prefix = xmlrecord.getTagValueByTag("Prefix");

                    titles[i] = description == null ? "Undefined" : description;
                    modes[i] = prefix == null ? "" : prefix;
                }

                builder.setTitle(R.string.add_photo).
                        setItems(titles, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(activity, CameraPreview.class);

                                intent.putExtra(modeKey, activity.getResources().getStringArray(R.array.camera_activity_modes)[0]);
                                intent.putExtra(CameraPreview.FILE_NAME_PREFIX, modes[i]);

                                activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_CameraPreview);
                            }
                        }).
                        setNegativeButton(R.string.cancel, null).
                        create().
                        show();
            }
        });

        fab = addFab;
        this.dbFab = dbFab;
        fab.setVisibility(View.INVISIBLE);
        this.dbFab.setVisibility(View.INVISIBLE);
        addPhoto.setVisibility(View.GONE);
    }

    public void addListItem(ListItem listItem){
        listItemAdapter.add(listItem);
    }

    public void addFiles(File[] files){
        final String strDate = new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime());
        Arrays.sort(files);
        String currentName = "", currentNewName = null;
        for(final File i: files){
            ExtensionManager extensionManager = new ExtensionManager(i.getName());
            String name = extensionManager.getNameWithoutExtension();
            if(!name.equals(currentName)){
                currentName = name;
                currentNewName = String.valueOf(getFilesCount()) + ", " + strDate;
            }
            final File file = new File(getFullPath(), currentNewName + "." + extensionManager.getExtension());
            extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
                @Override
                public void onPhotoDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onPhotoBarcodeDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onStringListDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onHashMapDataEvent() {

                }

                @Override
                public void onFolderEvent() {

                }

                @Override
                public void onXmlDataEvent() {

                }

                @Override
                public void on3gpEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onTxtEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }


            });
            extensionManager.run();
        }
    }

    public void addFiles(File[] files, String prefix){
        final String strDate = new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime());
        Arrays.sort(files);
        String currentName = "", currentNewName = null;
        for(final File i: files){
            ExtensionManager extensionManager = new ExtensionManager(i.getName());
            String name = extensionManager.getNameWithoutExtension();
            if(!name.equals(currentName)){
                currentName = name;
                if(!prefix.equals("")) currentNewName = String.valueOf(prefix + " ," + getFilesCount()) + ", " + strDate;
                else currentNewName = String.valueOf(getFilesCount()) + ", " + strDate;
            }
            final File file = new File(getFullPath(), currentNewName + "." + extensionManager.getExtension());
            extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
                @Override
                public void onPhotoDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onPhotoBarcodeDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onStringListDataEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onHashMapDataEvent() {

                }

                @Override
                public void onFolderEvent() {

                }

                @Override
                public void onXmlDataEvent() {

                }

                @Override
                public void on3gpEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }

                @Override
                public void onTxtEvent() {
                    i.renameTo(file);
                    listItemAdapter.add(new ListItem(file.getName(), null));
                }


            });
            extensionManager.run();
        }
    }

    public ArrayList<String> getListItemsNames(){
        ArrayList<String> listItemsNames = new ArrayList<>();
        int size = listItemAdapter.getCount();
        for(int i = 0; i < size; i++) listItemsNames.add(listItemAdapter.getItem(i).getName());
        return listItemsNames;
    }

    public void startSelectingItems(final int position){
        toolbar.startActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.multiple_choice_menu, menu);
                listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                actionMode = mode;
                inSelectMode = true;
                if (position != -1) listView.setItemChecked(position, true);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                MenuItem action_transfer_to_server = menu.findItem(R.id.action_transfer_to_server),
                        action_catalog_properties = menu.findItem(R.id.action_catalog_properties),
                        action_create_edit_commentary = menu.findItem(R.id.action_create_edit_commentary),
                        action_view_content = menu.findItem(R.id.action_view_content),
                        action_edit_barcodes = menu.findItem(R.id.action_edit_barcodes),
                        action_transmit = menu.findItem(R.id.action_transmit);
                       // action_transfer_to_sd_card = menu.findItem(R.id.load_to_sd_card),
                        //action_send_to_server_https = menu.findItem(R.id.action_transfer_to_server_https);
                switch (level) {
                    case 1:
                        action_create_edit_commentary.setVisible(false);
                        action_catalog_properties.setEnabled(lvlOneSelected);
                        action_view_content.setEnabled(lvlOneSelected);
                        action_edit_barcodes.setVisible(false);
                        action_transmit.setVisible(true);
                        //action_transfer_to_sd_card.setVisible(true);
                        //action_send_to_server_https.setVisible(true);
                        return true;
                    case 2:
                        action_catalog_properties.setVisible(false);
                        action_transfer_to_server.setVisible(false);
                        action_view_content.setVisible(false);
                        action_create_edit_commentary.setEnabled(lvlOneSelected);
                        action_edit_barcodes.setEnabled(lvlOneSelected);
                        action_transmit.setVisible(false);
                        //action_transfer_to_sd_card.setVisible(false);
                        //action_send_to_server_https.setVisible(false);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        deleteSelectedItems();
                        return true;
                    case R.id.action_transfer_to_server:
                        sendSelectedItems(true);
                        return true;
                    case R.id.action_catalog_properties:
                        if(lvlOneSelected){
                            FileManager xmlSettings = new FileManager(MainActivity.externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode",
                                    getLocalPath());
                            Bundle bundle = new Bundle();
                            bundle.putParcelable(xmlSettingsKey, xmlSettings);
                            FileManager fileManager = new FileManager(getRoot(), getLocalPath());
                            SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
                            fileManager.goAhead(listItemAdapter.getItem(sparseBooleanArray.keyAt(sparseBooleanArray.indexOfValue(true))).getName());
                            bundle.putParcelable(fileManagerKey, fileManager);
                            CatalogProperties catalogProperties = new CatalogProperties();
                            catalogProperties.setArguments(bundle);
                            catalogProperties.show(ContentManager.this.activity.getFragmentManager(), CatalogProperties.class.toString());
                        }
                        actionMode.finish();
                        return true;
                    case R.id.action_create_edit_commentary:
                        if(lvlOneSelected){
                            SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
                            String name = listItemAdapter.getItem(sparseBooleanArray.keyAt(sparseBooleanArray.indexOfValue(true))).getName();
                            if(name.equals("mode.xml")) Toast.makeText(activity, R.string.you_cannot_create_a_comment_for_mode_xml, Toast.LENGTH_SHORT).show();
                            else {
                                CreateCommentaryDialog createCommentaryDialog = new CreateCommentaryDialog();
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                bundle.putString(listItemNameKey, name);
                                createCommentaryDialog.setArguments(bundle);
                                createCommentaryDialog.show(activity.getFragmentManager(), CreateCommentaryDialog.class.toString());
                            }
                        }
                        actionMode.finish();
                        return true;
                    case R.id.action_view_content:
                        if(lvlOneSelected){
                            Bundle bundle = new Bundle();
                            FileManager fileManager = new FileManager(getRoot(), getLocalPath());
                            SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
                            fileManager.goAhead(listItemAdapter.getItem(sparseBooleanArray.keyAt(sparseBooleanArray.indexOfValue(true))).getName());
                            bundle.putParcelable(fileManagerKey, fileManager);
                            File[] files = fileManager.getFiles();
                            ArrayList<String> arrayList = new ArrayList<>(files.length);
                            for(File i: files) arrayList.add(i.getName());
                            bundle.putStringArrayList(listItemsNamesKey, arrayList);
                            bundle.putInt(listItemPositionKey, 0);
                            Intent intent = new Intent(activity, ViewFileActivity.class);
                            intent.putExtras(bundle);
                            activity.startActivity(intent);
                        }
                        actionMode.finish();
                        return true;
                    case R.id.action_edit_barcodes:
                        if(lvlOneSelected){
                            SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
                            String name = listItemAdapter.getItem(sparseBooleanArray.keyAt(sparseBooleanArray.indexOfValue(true))).getName();
                            ExtensionManager extensionManager = new ExtensionManager(name);
                            if(extensionManager.getExtension().equals(StringListData.class.getSimpleName())){
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(fileManagerKey, ContentManager.this);
                                FileManager highLevelFolder = new FileManager(getRoot(), getLocalPath());
                                highLevelFolder.goBack();
                                FileManager xmlSettings = new FileManager(MainActivity.externalStoragePath + SettingsActivity.getBackwards_synchronization_path(activity) + "/mode", "/" + highLevelFolder.getCurrentFolderName());
                                bundle.putParcelable(xmlSettingsKey, xmlSettings);
                                bundle.putBoolean(changeHashMapKey, true);
                                bundle.putString(listItemNameKey, name);
                                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[0]);
                                Intent intent = new Intent(activity, ActivityCreateBacode.class);
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                            }
                            else Toast.makeText(activity, R.string.this_file_doesnt_contain_barcodes, Toast.LENGTH_SHORT).show();
                        }
                        actionMode.finish();
                        return true;
                    case R.id.action_transmit:
                        sendSelectedItems(false);
                        return true;
//                    case R.id.load_to_sd_card:
//                        sendSelectedItems(SD_CARD);
//                        return true;
//                    case R.id.action_transfer_to_server_https:
//                        sendSelectedItems(HTTPS);
//                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                lvlOneSelected = false;
                int size = listView.getCount();
                for (int i = 0; i < size; i++) listView.setItemChecked(i, false);
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
                    }
                });
                actionMode = null;
                inSelectMode = false;
            }
        });
    }

    public void onCreateToolbarMenu(MenuInflater menuInflater, Menu menu){
        menuInflater.inflate(R.menu.main_activity_menu, menu);
        toolbar.setTitle(getCurrentFolderName());
    }

    public void onPrepareToolbarMenu(Menu menu){
        MenuItem action_select_items = menu.findItem(R.id.action_select_items),
                action_transfer_to_server = menu.findItem(R.id.action_transfer_to_server),
                action_catalog_properties = menu.findItem(R.id.action_catalog_properties),
                action_transmit = menu.findItem(R.id.action_transmit);
                //action_load_to_sd_card = menu.findItem(R.id.load_to_sd_card),
                //action_transfer_https = menu.findItem(R.id.action_transfer_to_server_https);
        switch (level){
            case 0:
                action_select_items.setVisible(false);
                action_transfer_to_server.setVisible(false);
                action_catalog_properties.setVisible(false);
                action_transmit.setVisible(false);
                //action_load_to_sd_card.setVisible(false);
                //action_transfer_https.setVisible(false);
                break;
            case 1:
                action_catalog_properties.setVisible(false);
                action_transmit.setVisible(true);
                //action_load_to_sd_card.setVisible(true);
                //action_transfer_https.setVisible(true);
                break;
            case 2:
                action_catalog_properties.setVisible(true);
                action_transmit.setVisible(true);
                //action_load_to_sd_card.setVisible(true);
                //action_transfer_https.setVisible(true);
                break;
        }
    }

    public List<ListItem> createListItems(){
        File[] files = getFiles();
        int length = 0;
        if(files != null) length = files.length;
        else{
            Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, R.string.unable_to_connect_external_storage, Toast.LENGTH_SHORT).show();
                }
            });
        }
        final List<ListItem> listItems = new ArrayList<>(length);
        if(length == 0) return listItems;

        if(level == 1){
            final HashMapManager hashMapManager = new HashMapManager(this);
            hashMapManager.getHashMap();
            for (File i: files){
                final String name = i.getName();
                ExtensionManager extensionManager = new ExtensionManager(name);
                extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
                    @Override
                    public void onPhotoDataEvent() {
                    }

                    @Override
                    public void onPhotoBarcodeDataEvent() {
                    }

                    @Override
                    public void onStringListDataEvent() {
                    }

                    @Override
                    public void onHashMapDataEvent() {
                    }

                    @Override
                    public void onFolderEvent() {
                        listItems.add(new ListItem(name, hashMapManager.get(getChildLocalPath(name)) ? activity.getString(R.string.already_sent) : activity.getString(R.string.not_sent_yet)));
                    }

                    @Override
                    public void onXmlDataEvent() {

                    }

                    @Override
                    public void on3gpEvent() {

                    }

                    @Override
                    public void onTxtEvent() {

                    }

                });
                extensionManager.run();
            }
            hashMapManager.saveChanges();
        }
        else for(File i: files) listItems.add(new ListItem(i.getName(), null));
        return listItems;
    }

    public void update(){
        int icon;
        switch (level){
            case 0:
                icon = R.drawable.ic_action_folder_tabs;
                listView.setLongClickable(false);
                toolbar.setNavigationIcon(null);
                fab.setVisibility(View.INVISIBLE);
                dbFab.setVisibility(View.INVISIBLE);
                addPhoto.setVisibility(View.GONE);
                break;
            case 1:
                icon = R.drawable.ic_action_folder_tabs;
                listView.setLongClickable(true);
                toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
                fab.setVisibility(View.VISIBLE);
                if(dbFab.getVisibility() == View.VISIBLE) dbFab.setVisibility(View.INVISIBLE);

                addPhoto.setVisibility(View.GONE);
                break;
            case 2:
                icon = R.drawable.ic_action_document;
                if(activity.getCurrentMode().equals(GlobalSettings.MODE_INVENTORY)) dbFab.setVisibility(View.VISIBLE);
                else if(dbFab.getVisibility() == View.VISIBLE) dbFab.setVisibility(View.INVISIBLE);

                if(!activity.getCurrentMode().equals(GlobalSettings.MODE_INVENTORY)) addPhoto.setVisibility(View.VISIBLE);
                break;
            default:
                icon = -1;
                break;
        }

        listItemAdapter.setIcon(icon);
        listItemAdapter.setListItems(createListItems());
    }

    public void notifyDataSetChanged(){
        listItemAdapter.notifyDataSetChanged();
        if(actionMode != null) actionMode.finish();
    }

    public void sendItems(boolean useSettings){
        switch (level){
            case 1:
                List<ListItem> listItems = new ArrayList<>();
                int size = listItemAdapter.getCount();
                for(int i = 0; i < size; i++){
                    ListItem listItem = listItemAdapter.getItem(i);
                    if(listItem.getState().equals(activity.getString(R.string.not_sent_yet))) listItems.add(listItem);
                }
                sendItems(listItems, useSettings);
                break;
            case 2:
                listItems = new ArrayList<>();

                FileManager fileManager = new FileManager(this);
                String folderName = fileManager.getCurrentFolderName();

                fileManager.goBack();

                final HashMapManager hashMapManager = new HashMapManager(this);
                hashMapManager.getHashMap();
                Boolean state = hashMapManager.get(getChildLocalPath(folderName));

                fileManager.goAhead(folderName);

                listItems.add(new ListItem(getCurrentFolderName(), state ? activity.getString(R.string.already_sent) : activity.getString(R.string.not_sent_yet)));
                sendItems(listItems, useSettings);
                break;
        }
    }

    private void deleteSelectedItems(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if(level == 1) builder.setTitle(R.string.deletion_of_folders).setMessage(R.string.delete_folders_confirmation);
        else builder.setTitle(R.string.deletion_of_files).setMessage(R.string.delete_files_confirmation);
        builder.setPositiveButton(R.string.action_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SparseBooleanArray array = listView.getCheckedItemPositions();
                int size = array.size();
                for (int i = size - 1; i >= 0; i--) {
                    if (array.valueAt(i)) {
                        int key = array.keyAt(i);
                        listView.setItemChecked(key, false);
                        final ListItem listItem = listItemAdapter.getItem(key);
                        String name = listItem.getName();
                        if(!name.equals("mode.xml")) {
                            if (level == 1) {
                                HashMapManager hashMapManager = new HashMapManager(ContentManager.this);
                                hashMapManager.getHashMap();
                                hashMapManager.remove(getChildLocalPath(name));
                                hashMapManager.saveChanges();
                            }
                            delete(name);
                            listView.post(new Runnable() {
                                @Override
                                public void run() {
                                    listItemAdapter.remove(listItem);
                                }
                            });
                        } else {
                            Toast.makeText(activity, R.string.you_dont_have_permission_to_delete_mode_xml, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                actionMode.finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendSelectedItems(boolean useSettings){
        SparseBooleanArray array = listView.getCheckedItemPositions();
        int size = array.size();
        List<ListItem> listItems = new ArrayList<>();
        for(int i = 0; i < size; i++) if(array.valueAt(i)) listItems.add(listItemAdapter.getItem(array.keyAt(i)));
        sendItems(listItems, useSettings);
    }

    private void sendItems(final List<ListItem> listItems, boolean useSettings){
        if(useSettings) {
            String type = PreferenceManager.getDefaultSharedPreferences(activity).getString(activity.getString(R.string.upload_type_key), activity.getString(R.string.upload_type_default));
            switch (type) {
                case SMB:
                    transferUsingSmb(listItems);
                    break;
                case HTTPS:
                    transferUsingHttps(listItems);
                    break;
                case SD_CARD:
                    transferToSdCard(listItems);
                    break;
            }
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.choose_upload_type).
                    setItems(R.array.upload_types_v2, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i){
                        case 0:
                            transferUsingSmb(listItems);
                            break;
                        case 1:
                            transferUsingHttps(listItems);
                            break;
                        case 2:
                            transferToSdCard(listItems);
                            break;
                    }
                }
            }).setNegativeButton(R.string.cancel, null).
                    create().
                    show();
        }
    }

    private void transferUsingSmb(final List<ListItem> listItems){
        if (isWifiEnabled()) {
            FileManager fileManager = new FileManager(this);
            if (level == 2) fileManager.goBack();
            final WifiTransmissionManager manager = new WifiTransmissionManager(SettingsActivity.getIp(activity),
                    SettingsActivity.getPathOnServer(activity),
                    SettingsActivity.getUserName(activity),
                    SettingsActivity.getUserPassword(activity),
                    fileManager, activity);
            if (manager.isServerAvailable()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(R.string.confirmation).setMessage(R.string.send_data_to_server_confirmation);
                builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProgressBarDialog progressBarDialog = new ProgressBarDialog();
                        progressBarDialog.show(activity.getFragmentManager(), ProgressBarDialog.class.getSimpleName());
                        manager.transmit(listItems, progressBarDialog);
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(R.string.message).setMessage(R.string.cannot_establish_connection);
                builder.setPositiveButton(R.string.ok, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.message).setMessage(R.string.no_wifi_connection);
            builder.setPositiveButton(R.string.ok, null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void transferUsingHttps(final List<ListItem> listItems){
        SendDataHttps sendDataHttps = SendDataHttps.get(new ArrayList<>(listItems), new FileManager(this), level);

        sendDataHttps.show(activity.getSupportFragmentManager(), sendDataHttps.getTag());

        if(actionMode != null) actionMode.finish();
    }

    private void transferToSdCard(final List<ListItem> listItems){

        ArrayList<String> secondLevelCatalogs = new ArrayList<>(listItems.size());
        for (ListItem i: listItems) secondLevelCatalogs.add(i.getName());

        String firstLevelCatalog;

        if(level == 1) firstLevelCatalog = getCurrentFolderName();
        else{
            String currentFolder = getCurrentFolderName();
            goBack();
            firstLevelCatalog = getCurrentFolderName();
            goAhead(currentFolder);
        }

        FilesToMove filesToMove = new FilesToMove(getRoot(), firstLevelCatalog, secondLevelCatalogs);
        SelectStorageDialog dialog = SelectStorageDialog.getInstance(filesToMove);
        dialog.show(activity.getSupportFragmentManager(), dialog.getTag());
//        com.example.thebeginning.extended.repository.FileManager fileManager = new com.example.thebeginning.extended.repository.FileManager();
//
//        final List<String> directories = fileManager.getDirectories(activity);
//
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
//
//        }
//        else {
//            UsbManager manager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
//
//            if (manager.getDeviceList().size() != 0) {
//
//                try {
//                    Runtime runtime = Runtime.getRuntime();
//                    Process process = runtime.exec("mount");
//                    InputStream inputStream = process.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    String line;
//
//                    List<String> list = new ArrayList<>();
//                    BufferedReader br = new BufferedReader(inputStreamReader);
//                    while ((line = br.readLine()) != null) {
//                        list.add(line);
//                        if ((line.contains("/dev/fuse") && line.toLowerCase().contains("usb")) || (line.contains("/dev/block/vold/public"))) {
//                            String columns[] = line.split(" ");
//                            if (columns.length > 1) {
//                                String mount = columns[1];
//                                File dir = new File(mount);
//                                if (dir.exists() && dir.isDirectory()) directories.add(mount);
//                            }
//                        }
//                    }
//                    list.add("");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        builder.setTitle("Выберите место для выгрузки").
//                setNegativeButton("Отмена", null).
//                setItems(directories.toArray(new String[directories.size()]), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        ProgressBarDialog progressBarDialog = new ProgressBarDialog();
//                        progressBarDialog.show(activity.getFragmentManager(), ProgressBarDialog.class.getSimpleName());
//
//                        ConversionManager conversionManager = new ConversionManager(new FileManager(directories.get(i) + "/TheBeginning_photo", ""),
//                                new FileManager(ContentManager.this));
//                        conversionManager.convert(listItems, progressBarDialog, level);
//                    }
//                }).
//                create().
//                show();
    }

    private boolean isWifiEnabled(){
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo == null) return false;
        return networkInfo.isConnected();
    }
}
