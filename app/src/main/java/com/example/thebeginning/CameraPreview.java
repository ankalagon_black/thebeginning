package com.example.thebeginning;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 21.01.2016.
 */
public class CameraPreview extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.PictureCallback{
    public static String fileManagerKey = "CameraPreview.fileManagerKey";
    public static String FILE_NAME_PREFIX = "FileNamePrefix";
    public String[] modes;

    private Camera camera;
    private MediaRecorder mediaRecorder;
    private boolean isVideoRecording = false;

    private OrientationEventListener orientationEventListener;

    private BarcodeScanner barcodeScanner;
    private boolean isDecoding = false;
    private Rect rect;
    private boolean scanningStarted = false;

    private SurfaceHolder surfaceHolder;
    private FrameLayout contentView;
    private Switch scannerType;
    private Button handInput;
    private ImageButton scannerButton;
    private TextView barcodeView;
    private View buttonsView;

    private static final int NONE = 0;
    private static final int ZOOM = 1;
    private int mode = NONE;
    private float startDir = 1f;
    private float lastDist = 1f;
    private int key = 0;
    private boolean isZooming;
    private boolean zoomSupported;
    private SeekBar seekBar;

    private FileManager fileManager;
    private AbstractData abstractData;
    private String programMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_layout);

        modes = getResources().getStringArray(R.array.camera_activity_modes);

        fileManager = new FileManager(MainActivity.externalStoragePath + "/TBF", "");
        fileManager.createFolder();
        fileManager.deleteAll();

        final SurfaceView surfaceView = findViewById(R.id.camera_preview_surface);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);

        final DrawerLayout drawerLayout = findViewById(R.id.camera_activity_drawer_layout);
        programMode = getIntent().getStringExtra(ContentManager.modeKey);
        contentView = findViewById(R.id.camera_activity_content);
        contentView.addView(prepareView(programMode), 0);
        final Button button = findViewById(R.id.camera_activity_change_mode_button);
        button.setText(programMode);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        final Button finishButton = findViewById(R.id.camera_activity_finish);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDataAndFinish();
            }
        });
        if(hasNamePrefix()) finishButton.setVisibility(View.INVISIBLE);

        final ListView listView = findViewById(R.id.camera_activity_drawer_layout_list);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, modes));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isVideoRecording || isDecoding) return;
                Object object = parent.getItemAtPosition(position);
                if (object.equals(modes[0])) {
                    if (!programMode.equals(modes[0])) {
                        camera.stopPreview();
                        if (programMode.equals(modes[2])) camera.setPreviewCallback(null);
                        else {
                            Camera.Parameters parameters = camera.getParameters();
                            List<String> focusModes = parameters.getSupportedFocusModes();
                            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                            camera.setParameters(parameters);

                        }
                        contentView.removeViewAt(0);
                        contentView.addView(prepareView(modes[0]), 0);
                        camera.startPreview();
                        programMode = modes[0];
                        button.setText(programMode);
                    }
                    Toast.makeText(CameraPreview.this, String.format(Locale.US, getString(R.string.current_mode), programMode), Toast.LENGTH_SHORT).show();
                    drawerLayout.closeDrawer(listView);
                } else if (object.equals(modes[1])) {
                    if (!programMode.equals(modes[1])) {
                        camera.stopPreview();
                        if (programMode.equals(modes[2])) camera.setPreviewCallback(null);
                        Camera.Parameters parameters = camera.getParameters();
                        List<String> focusModes = parameters.getSupportedFocusModes();
                        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                        camera.setParameters(parameters);
                        contentView.removeViewAt(0);
                        contentView.addView(prepareView(modes[1]), 0);
                        camera.startPreview();
                        programMode = modes[1];
                        button.setText(programMode);
                    }
                    Toast.makeText(CameraPreview.this, String.format(Locale.US, getString(R.string.current_mode), programMode), Toast.LENGTH_SHORT).show();
                    drawerLayout.closeDrawer(listView);
                } else if (object.equals(modes[2])) {
                    if (!programMode.equals(modes[2])) {
                        camera.stopPreview();
                        camera.setPreviewCallback(CameraPreview.this);
                        if (programMode.equals(modes[1])) {
                            Camera.Parameters parameters = camera.getParameters();
                            List<String> focusModes = parameters.getSupportedFocusModes();
                            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                            camera.setParameters(parameters);
                        }
                        contentView.removeViewAt(0);
                        contentView.addView(prepareView(modes[2]), 0);
                        camera.startPreview();
                        programMode = modes[2];
                        button.setText(programMode);
                    }
                    Toast.makeText(CameraPreview.this, String.format(Locale.US, getString(R.string.current_mode), programMode), Toast.LENGTH_SHORT).show();
                    drawerLayout.closeDrawer(listView);
                }
            }
        });

        View menuView = findViewById(R.id.camera_activity_menu);
        seekBar = menuView.findViewById(R.id.camera_activity_zooming_bar);
        menuView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!zoomSupported) return false;
                switch (event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_POINTER_DOWN:
                        float x = event.getX(0) - event.getX(1);
                        float y = event.getY(0) - event.getY(1);
                        lastDist = (float) Math.sqrt(x * x + y * y);
                        if(lastDist > 10f){
                            startDir = lastDist;
                            mode = ZOOM;
                            isZooming = true;
                        }
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        isZooming = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        ++key;
                        seekBar.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                --key;
                                if(seekBar.getVisibility() == View.VISIBLE && !isZooming && key == 0){
                                    seekBar.setVisibility(View.INVISIBLE);
                                }
                            }
                        }, 1000);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(mode == ZOOM){
                            x = event.getX(0) - event.getX(1);
                            y = event.getY(0) - event.getY(1);
                            float currentDist = (float) Math.sqrt(x * x + y * y);
                            if(Math.abs(currentDist - lastDist) > 2f) {
                                float shift = (currentDist / startDir - 1) * 6;
                                seekBar.setProgress(seekBar.getProgress() + (int) shift);
                                lastDist = currentDist;
                            }
                        }
                        break;
                }
                return true;
            }
        });

        orientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {

            private int lastOrientation = -1;

            @Override
            public void onOrientationChanged(int orientation) {

                if (orientation == ORIENTATION_UNKNOWN) return;
                orientation = (orientation + 45) / 90 * 90;

                if(orientation != lastOrientation){
                    Camera.CameraInfo info = new Camera.CameraInfo();
                    Camera.getCameraInfo(0, info);
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setRotation((info.orientation + orientation) % 360);
                    camera.setParameters(parameters);

                    lastOrientation = orientation;
                }
            }
        };

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(0, cameraInfo);
    }

    public boolean hasNamePrefix(){
        return getIntent().getStringExtra(FILE_NAME_PREFIX) != null;
    }

    public void saveDataAndFinish(){
        String prefix = getIntent().getStringExtra(FILE_NAME_PREFIX);
        if(prefix != null){
            Intent resultIntent = new Intent();
            resultIntent.putExtra(FILE_NAME_PREFIX, prefix);

            setResult(RESULT_OK, resultIntent );
            finish();
        }
        else {
            setResult(RESULT_OK);
            finish();
        }
    }

    public AbstractData getAbstractData(){
        return abstractData;
    }

    public void startPreview(){
        camera.startPreview();
    }

    public void setPreviewCallback(){
        camera.setPreviewCallback(this);
    }

    public void showResult(){
        if(hasNamePrefix()){
            String name = String.valueOf(fileManager.getFilesCount());
            fileManager.writeToFile(name + "." + abstractData.getClass().getSimpleName() ,abstractData);

            saveDataAndFinish();
        }
        else {
            ShowCameraResultFialog showCameraResultFialog = new ShowCameraResultFialog();
            Bundle bundle = new Bundle();
            bundle.putParcelable(fileManagerKey, fileManager);
            showCameraResultFialog.setArguments(bundle);
            showCameraResultFialog.show(getFragmentManager(), ShowCameraResultFialog.class.toString());
        }
    }

    private View prepareView(String mode){
        View view;
        if(mode.equals(modes[0])){
            view = getLayoutInflater().inflate(R.layout.camera_photo_layout, null);
            ImageButton imageButton = view.findViewById(R.id.camera_photo_layout_button);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abstractData = new PhotoData();
                    camera.cancelAutoFocus();
                    camera.takePicture(null, null, CameraPreview.this);
                }
            });
            Switch aSwitch = view.findViewById(R.id.camera_photo_layout_switch);
            if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
                aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Camera.Parameters parameters = camera.getParameters();
                        if(isChecked) parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        else parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameters);
                    }
                });
            }
            else aSwitch.setVisibility(View.INVISIBLE);
        }
        else if(mode.equals(modes[1])){
            view = getLayoutInflater().inflate(R.layout.camera_video_layout, null);
            final ImageButton imageButton = view.findViewById(R.id.camera_video_layout_button);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isVideoRecording){
                        mediaRecorder.stop();
                        releaseMediaRecorder();
                        isVideoRecording = false;
                        imageButton.setImageResource(R.drawable.ic_action_video);
                        camera.stopPreview();
                        showResult();
                    }
                    else{
                        abstractData = null;
                        camera.unlock();
                        mediaRecorder = new MediaRecorder();
                        mediaRecorder.setCamera(camera);
                        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
                        mediaRecorder.setOutputFile(fileManager.getFullPath() + "/" + String.valueOf(fileManager.getFilesCount()) + ".3gp");
                        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
                        try {
                            mediaRecorder.prepare();
                        } catch (IllegalStateException | IOException e) {
                            Log.v(CameraPreview.class.toString(), e.toString());
                            releaseMediaRecorder();
                        }
                        if (mediaRecorder != null) {
                            mediaRecorder.start();
                            isVideoRecording = true;
                            imageButton.setImageResource(R.drawable.ic_stop);
                        }
                    }
                }
            });
        }
        else{
            view = getLayoutInflater().inflate(R.layout.camera_scanner_layout, null);
            barcodeScanner = new BarcodeScanner();
            barcodeView = view.findViewById(R.id.camera_scanner_barcode);
            buttonsView = view.findViewById(R.id.camera_scanner_buttons);
            handInput = view.findViewById(R.id.camera_scanner_barcode_input);
            handInput.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    DialogCreateBarcode createBarcodeDialog = new DialogCreateBarcode();
//                    Bundle bundle = new Bundle();
//                    bundle.putParcelable(ContentManager.fileManagerKey, fileManager);
//                    bundle.putBoolean(ContentManager.changeHashMapKey, false);
//                    createBarcodeDialog.setArguments(bundle);
//                    createBarcodeDialog.show(getFragmentManager(), DialogCreateBarcode.class.toString());
                }
            });
            ImageButton addBarcode = buttonsView.findViewById(R.id.camera_scanner_add_barcode);
            ImageButton refresh = buttonsView.findViewById(R.id.camera_scanner_refresh);
            addBarcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(abstractData.getList().size() == 0){
                        scannerButton.setImageResource(R.drawable.ic_action_tick);
                    }
                    abstractData.add(barcodeView.getText().toString());
                    buttonsView.setVisibility(View.INVISIBLE);
                    barcodeView.setVisibility(View.INVISIBLE);
                    isDecoding = true;
                }
            });
            refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonsView.setVisibility(View.INVISIBLE);
                    barcodeView.setVisibility(View.INVISIBLE);
                    isDecoding = true;
                }
            });
            scannerType = view.findViewById(R.id.camera_scanner_switch);
            scannerButton = view.findViewById(R.id.camera_scanner_layout_button);
            scannerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(scanningStarted){
                        isDecoding = false;
                        scannerButton.setImageResource(R.drawable.ic_action_barcode_1);
                        handInput.setVisibility(View.VISIBLE);
                        scannerType.setVisibility(View.VISIBLE);
                        if (scannerType.isChecked()) {
                            if(abstractData.getList().size() > 0){
                                camera.stopPreview();
                                showResult();
                            }
                            barcodeView.setVisibility(View.INVISIBLE);
                            buttonsView.setVisibility(View.INVISIBLE);
                        }
                        scanningStarted = false;
                    }
                    else{
                        scanningStarted = true;
                        scannerType.setVisibility(View.INVISIBLE);
                        handInput.setVisibility(View.INVISIBLE);
                        if (scannerType.isChecked()) abstractData = new StringListData();
                        else abstractData = new PhotoBarcodeData();
                        isDecoding = true;
                        scannerButton.setImageResource(R.drawable.ic_action_cancel);
                    }
                }
            });
        }
        return view;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
//        int rotationAngle = (deviceOrientation + cameraOrientation) % 360;
//        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//        Matrix matrix = new Matrix();
//        matrix.postRotate(rotationAngle);
//        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 55, outputStream);
        abstractData.setJpegBytes(data);
        showResult();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(camera != null){
            camera.stopPreview();

            if(programMode.equals(modes[2])) camera.setPreviewCallback(this);
            try {
                camera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                Log.v(CameraPreview.class.toString(), e.toString());
            }

            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();
            rect = new Rect(0, 0, size.width, size.height);

            camera.startPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        parameters.setJpegQuality(MainActivity.globalSettings.getJpegQuality() * 10);
        //
        List<Camera.Size> sizeList = parameters.getSupportedPictureSizes();
        int size = sizeList.size();
        Camera.Size selectedSize = sizeList.get(0);
        int minDifference = Math.abs(selectedSize.width - 4000);
        for(int i = 1; i < size; i++){
            Camera.Size cameraSize = sizeList.get(i);
            if(cameraSize.width == 4000){
                selectedSize = cameraSize;
                break;
            }
            else {
                int difference = Math.abs(cameraSize.width - 4000);
                if(difference < minDifference){
                    minDifference = difference;
                    selectedSize = cameraSize;
                }
            }
        }

        parameters.setPictureSize(selectedSize.width, selectedSize.height);
        //
        zoomSupported = parameters.isZoomSupported();
        if(zoomSupported){
            seekBar.setMax(parameters.getMaxZoom());
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (seekBar.getVisibility() == View.INVISIBLE) seekBar.setVisibility(View.VISIBLE);
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setZoom(progress);
                    camera.setParameters(parameters);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    isZooming = true;
                }

                @Override
                public void onStopTrackingTouch(final SeekBar seekBar) {
                    isZooming = false;
                    seekBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isZooming && key == 0) seekBar.setVisibility(View.INVISIBLE);
                        }
                    }, 1000);
                }
            });
        }
        if(programMode.equals(modes[1])){
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }
        else{
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        camera.setParameters(parameters);
        camera.startPreview();

        orientationEventListener.enable();
    }

    @Override
    protected void onPause() {
        super.onPause();

        orientationEventListener.disable();

        releaseMediaRecorder();
        if(camera != null){
            if(programMode.equals(modes[2])) camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    private void releaseMediaRecorder(){
        if(mediaRecorder != null){
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if(isDecoding){
            Result result = barcodeScanner.decode(data, rect);
            if(result != null){
                if(scannerType.isChecked()){
                    buttonsView.setVisibility(View.VISIBLE);
                    barcodeView.setText(result.getText() + ",1");
                    barcodeView.setVisibility(View.VISIBLE);
                }
                else{
                    abstractData.setBarcode(result.getText() + ",1");
                    camera.cancelAutoFocus();
                    camera.takePicture(null, null, this);
                    scannerButton.setImageResource(R.drawable.ic_action_barcode_1);
                    handInput.setVisibility(View.VISIBLE);
                    scannerType.setVisibility(View.VISIBLE);
                    scanningStarted = false;
                }
                isDecoding = false;
            }
        }
    }
}
