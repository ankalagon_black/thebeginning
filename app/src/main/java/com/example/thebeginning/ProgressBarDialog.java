package com.example.thebeginning;

import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;


/**
 * Created by Константин on 09.04.2016.
 */
public class ProgressBarDialog extends DialogFragment {

    private TextProgressBar folderBar, fileBar, byteBar;
    private TextView currentFolder, currentFile;
    private Button closeButton;
    private boolean updateOnExit;

    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        setCancelable(false);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ProgressBarDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.transmission_in_progress);

        View view = inflater.inflate(R.layout.progress_bar, container);
        folderBar = view.findViewById(R.id.progress_bar_folder_bar);
        fileBar = view.findViewById(R.id.progress_bar_file_bar);
        byteBar = view.findViewById(R.id.progress_bar_byte_bar);
        currentFolder = view.findViewById(R.id.progress_bar_current_folder);
        currentFile = view.findViewById(R.id.progress_bar_current_file);
        closeButton = view.findViewById(R.id.progress_bar_close_button);
        closeButton.setAlpha(0.4f);
        closeButton.setEnabled(false);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    public void waitForCreation(boolean updateOnExit){
        this.updateOnExit = updateOnExit;
        while (!isVisible());
    }

    public void initializeFolderBar(final int size){
        handler.post(new Runnable() {
            @Override
            public void run() {
                folderBar.setProgress(0);
                folderBar.setMax(size);
                folderBar.setText(String.format(Locale.US, getString(R.string.out_of_folders), 0, size));
            }
        });
    }

    public void incrementFolderBar(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                folderBar.incrementProgressBy(1);
                folderBar.setText(String.format(Locale.US, getString(R.string.out_of_folders), folderBar.getProgress(), folderBar.getMax()));
            }
        });
    }

    public void initializeFileBar(final int size){
        handler.post(new Runnable() {
            @Override
            public void run() {
                fileBar.setProgress(0);
                fileBar.setMax(size);
                fileBar.setText(String.format(Locale.US, getString(R.string.out_of_files), 0, size));
            }
        });
    }

    public void incrementFileBar(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                fileBar.incrementProgressBy(1);
                fileBar.setText(String.format(Locale.US, getString(R.string.out_of_files), fileBar.getProgress(), fileBar.getMax()));
            }
        });
    }

    public void initializeByteBar(final int size){
        handler.post(new Runnable() {
            @Override
            public void run() {
                byteBar.setProgress(0);
                byteBar.setMax(size == 0 ? 1 : size);
                byteBar.setText(String.format(Locale.US, getString(R.string.out_of_kbytes_decimal), 0, size));
            }
        });
    }

    public void incrementByteBar(final int diff){
        handler.post(new Runnable() {
            @Override
            public void run() {
                byteBar.incrementProgressBy(diff);
                byteBar.setText(String.format(Locale.US, getString(R.string.out_of_kbytes_decimal), byteBar.getProgress(), byteBar.getMax()));
            }
        });
    }

    public void setCurrentFolder(final String name){
        handler.post(new Runnable() {
            @Override
            public void run() {
                currentFolder.setText(String.format(Locale.US, getString(R.string.current_folder), name));
            }
        });
    }

    public void setCurrentFile(final String name){
        handler.post(new Runnable() {
            @Override
            public void run() {
                currentFile.setText(String.format(Locale.US, getString(R.string.current_file), name));
            }
        });
    }

    public void finish(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                getDialog().setTitle(R.string.transmission_succesfully_finished);
                closeButton.setAlpha(1.0f);
                closeButton.setEnabled(true);
                if(updateOnExit) {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.updateDataSet();
                }
            }
        });
    }

}
