package com.example.thebeginning;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by Константин on 28.03.2016.
 */
public class TextProgressBar extends ProgressBar {
    private String text;
    private Paint paint;

    public TextProgressBar(Context context) {
        super(context);
        text = "/";
        paint = new Paint();
        paint.setColor(Color.BLACK);

    }

    public TextProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        text = "/";
        paint = new Paint();
        paint.setColor(Color.BLACK);
    }

    public TextProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        text = "/";
        paint = new Paint();
        paint.setColor(Color.BLACK);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        int x = getWidth() / 2 - rect.centerX();
        int y = getHeight() / 2 - rect.centerY();
        canvas.drawText(text, x, y, paint);
    }

    public synchronized void setText(String newText){
        text = newText;
        drawableStateChanged();
    }

    public String getText() {
        return text;
    }
}
