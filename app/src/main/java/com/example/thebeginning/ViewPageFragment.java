package com.example.thebeginning;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by Константин on 14.04.2016.
 */
public class ViewPageFragment extends Fragment {

    private static String currentListItemNameKey = "ViewPageFragment.currentListItemNameKey";

    private static FileManager fileManager;
    private File currentFile;
    private ExtensionManager extensionManager;

    public static Fragment getInstance(String name, FileManager fileManager){
        ViewPageFragment viewPageFragment = new ViewPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(currentListItemNameKey, name);
        viewPageFragment.setArguments(bundle);
        ViewPageFragment.fileManager = fileManager;
        return viewPageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        String name = bundle.getString(currentListItemNameKey);
        currentFile = new File(fileManager.getFullPath(), name);
        extensionManager = new ExtensionManager(name);
    }

    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        final ViewFileActivity activity = (ViewFileActivity) getActivity();
        final View[] finalView = new View[1];

        extensionManager.setOnExtensionEventListener(new onExtensionEventListener() {
            @Override
            public void onPhotoDataEvent() {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.view_file_content_layout, null);

                View view = inflater.inflate(R.layout.photo_data, null);
                TextView textView = view.findViewById(R.id.photo_data_file_name);
                textView.setText(currentFile.getName());
                ZoomableImageView imageView = view.findViewById(R.id.photo_data_image);
                AbstractData abstractData = fileManager.readFromFile(currentFile, new PhotoData());
                DisplayMetrics displayMetrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                imageView.setImage(abstractData.getBitmap(), displayMetrics.widthPixels, displayMetrics.heightPixels);
                linearLayout.addView(view);

                finalView[0] = linearLayout;
            }

            @Override
            public void onPhotoBarcodeDataEvent() {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.view_file_content_layout, null);

                View view = inflater.inflate(R.layout.photo_barcode_data, null);
                TextView textView = view.findViewById(R.id.photo_barcode_data_file_name);
                textView.setText(currentFile.getName());
                ZoomableImageView imageView = view.findViewById(R.id.photo_barcode_data_image);
                AbstractData abstractData = fileManager.readFromFile(currentFile, new PhotoBarcodeData());
                DisplayMetrics displayMetrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                imageView.setImage(abstractData.getBitmap(), displayMetrics.widthPixels, displayMetrics.heightPixels);
                TextView barcodeView = view.findViewById(R.id.photo_barcode_data_barcode);
                barcodeView.setText(abstractData.getBarcode());
                linearLayout.addView(view);

                finalView[0] = linearLayout;
            }

            @Override
            public void onStringListDataEvent() {
                final ScrollView scrollView = (ScrollView) inflater.inflate(R.layout.view_file_content_layout_scroll, null);

                View view = inflater.inflate(R.layout.string_list_data, null);
                TextView textView = view.findViewById(R.id.string_list_data_file_name);
                textView.setText(currentFile.getName());
                ListView listView = view.findViewById(R.id.string_list_data_list);
                AbstractData abstractData = fileManager.readFromFile(currentFile, new StringListData());
                listView.setAdapter(new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, abstractData.getList()));
                scrollView.addView(view);

                finalView[0] = scrollView;
            }

            @Override
            public void onHashMapDataEvent() {
            }

            @Override
            public void onFolderEvent() {
            }

            @Override
            public void onXmlDataEvent() {
                final ScrollView scrollView = (ScrollView) inflater.inflate(R.layout.view_file_content_layout_scroll, null);

                View view = inflater.inflate(R.layout.text_data, null);
                TextView textView = view.findViewById(R.id.xml_data_file_name);
                textView.setText(currentFile.getName());
                TextView contentView = view.findViewById(R.id.xml_data_content);
                final AbstractData abstractData = fileManager.readFromFile(currentFile, new XmlData());
                contentView.setText(abstractData.getContent());
                scrollView.addView(view);

                finalView[0] = scrollView;
            }

            @Override
            public void on3gpEvent() {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.view_file_content_layout, null);

                View view = inflater.inflate(R.layout.video_layout, null);
                TextView textView = view.findViewById(R.id.video_view_text);
                textView.setText(currentFile.getName());
                VideoView videoView = view.findViewById(R.id.videoView);
                videoView.setVideoPath(currentFile.getPath());
                videoView.setMediaController(new MediaController(activity));
                videoView.requestFocus();
                linearLayout.addView(view);

                finalView[0] = linearLayout;
            }

            @Override
            public void onTxtEvent() {
                final ScrollView scrollView = (ScrollView) inflater.inflate(R.layout.view_file_content_layout_scroll, null);

                View view = inflater.inflate(R.layout.text_data, null);
                TextView textView = view.findViewById(R.id.xml_data_file_name);
                textView.setText(currentFile.getName());
                TextView contentView = view.findViewById(R.id.xml_data_content);
                final AbstractData abstractData = fileManager.readFromFile(currentFile, new CommentaryData());
                contentView.setText(abstractData.getText());
                scrollView.addView(view);

                finalView[0] = scrollView;
            }
        });
        extensionManager.run();
        return finalView[0];
    }
}
