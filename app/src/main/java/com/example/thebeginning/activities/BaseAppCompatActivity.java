package com.example.thebeginning.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.example.thebeginning.App;
import com.example.thebeginning.MainActivity;
import com.example.thebeginning.R;
import com.example.thebeginning.data_types.GlobalSettings;

/**
 * Created by Константин on 21.01.2018.
 */

@SuppressLint("Registered")
public class BaseAppCompatActivity extends AppCompatActivity {

    public String getCurrentLanguage(){
        return ((App) getApplication()).getCurrentLanguage();
    }

    public void setCurrentLanguage(String language){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(getString(R.string.language), language);
        editor.apply();

        ((App) getApplication()).setLocale();
    }

    public String getStringLoc(int id){
        if(MainActivity.globalSettings.getLanguages() != null){
            StringBuilder stringBuilder = new StringBuilder();

            Configuration configuration = new Configuration(getApplication().getResources().getConfiguration());

            for (String i: MainActivity.globalSettings.getLanguages()){
                configuration.setLocale(GlobalSettings.getLocale(i));
                String localized = getApplication().createConfigurationContext(configuration).getResources().getString(id);
                stringBuilder.append(localized).append(" ");
            }

            return stringBuilder.toString();
        }
        else return getString(id);
    }

    public String[] getArray(int id){
        if(MainActivity.globalSettings.getLanguages() != null){
            String[] array = getResources().getStringArray(id);

            Configuration configuration = new Configuration(getApplication().getResources().getConfiguration());

            int size = MainActivity.globalSettings.getLanguages().size();
            for (int i = 1; i < size; i++){
                configuration.setLocale(GlobalSettings.getLocale(MainActivity.globalSettings.getLanguages().get(i)));

                String[] localized = getApplication().createConfigurationContext(configuration).getResources().getStringArray(id);
                for (int j = 0; j < array.length; j++){
                    array[j] += " " + localized[j];
                }
            }

            return array;
        }
        else return getResources().getStringArray(id);
    }

}
