package com.example.thebeginning;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 24.04.2016.
 */
public class XmlRecord {
    private String domain;
    private String parentTag;
    private List<TagRecord> tagRecords;

    public XmlRecord(String parentTag){
        this.parentTag = parentTag;
        tagRecords = new ArrayList<>();
    }

    public void add(String tag, String value){
        tagRecords.add(new TagRecord(tag, value));
    }

    public void setDomain(String domain){
        this.domain = domain;
    }

    public String getDomain(){
        return domain;
    }

    public String getParentTag(){
        return parentTag;
    }

    public List<TagRecord> getTagRecords(){
        return tagRecords;
    }

    public List<String> getTags(){
        List<String> list = new ArrayList<>();
        for(TagRecord i: tagRecords){
            list.add(i.getTag());
        }
        return list;
    }

    public String getTagValueByTag(String tag){
        for(TagRecord i: tagRecords){
            if(i.getTag().equals(tag)) return i.getValue();
        }
        return null;
    }

    public TagRecord getTagRecordByTag(String tag){
        for(TagRecord i: tagRecords){
            if(i.getTag().equals(tag)) return i;
        }
        return null;
    }

    public List<TagRecord> getTagRecordByMask(String mask){
        List<TagRecord> list = new ArrayList<>();
        for(TagRecord i: tagRecords){
            if(i.getTag().contains(mask)) list.add(i);
        }
        return list;
    }

    public void addAll(XmlRecord record){
        tagRecords.addAll(record.getTagRecords());
    }
}
