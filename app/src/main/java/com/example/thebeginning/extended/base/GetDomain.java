package com.example.thebeginning.extended.base;

/**
 * Created by Константин on 12.07.2017.
 */

public interface GetDomain<D> {
    D getDomain();
}
