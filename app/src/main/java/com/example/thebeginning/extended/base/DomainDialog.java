package com.example.thebeginning.extended.base;

import android.app.DialogFragment;

/**
 * Created by Константин on 11.07.2017.
 */

public abstract class DomainDialog<D> extends DialogFragment {
    public D getDomain(){
        return ((GetDomain<D>) getActivity()).getDomain();
    }
}
