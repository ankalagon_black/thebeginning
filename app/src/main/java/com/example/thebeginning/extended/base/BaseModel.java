package com.example.thebeginning.extended.base;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class BaseModel {
    private List<Disposable> disposables = new ArrayList<>();
    private boolean isFinishing = false;

    protected final  <T> void subscribe(Single<T> single, final DisposableSingleObserver<T> observer){
        disposables.add(observer);
        single.subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isFinishing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);
    }

    protected final void subscribe(Completable completable, final DisposableCompletableObserver observer){
        disposables.add(observer);
        completable.subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isFinishing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);
    }

    protected final <T> void subscribe(Observable<T> observable, final DisposableObserver<T> observer){
        disposables.add(observer);
        observable.
                subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).
                doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if(!isFinishing) disposables.remove(observer);
                    }
                }).
                subscribe(observer);
    }

    protected final void disposeAllBackgroundTasks(){
        isFinishing = true;
        for (Disposable i : disposables) {
            if (!i.isDisposed()) i.dispose();
        }

        disposables.clear();
    }
}
