package com.example.thebeginning.extended;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.thebeginning.R;
import com.example.thebeginning.extended.repository.network.GetSettingsInterface;
import com.example.thebeginning.extended_v2.data_types.HttpsInfo;
import com.example.thebeginning.extended_v2.repository.CertificateManager;
import com.example.thebeginning.extended_v2.repository.SharedPreferencesManager;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Константин on 11.07.2017.
 */

public class SettingsDomain implements GetSettingsModel.DomainInterface{
    private Context context;

    public SettingsDomain(Context context) {
        this.context = context;
    }

    @Override
    public String getSettings() throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        SharedPreferencesManager manager = new SharedPreferencesManager(context);

        HttpsInfo httpsInfo = manager.getSettingsHttpsInfo();

        OkHttpClient okHttp = new OkHttpClient.Builder()
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                })
                .sslSocketFactory(new CertificateManager().getDefaultContext().getSocketFactory())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(httpsInfo.getHttpsUrl())
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetSettingsInterface anInterface = retrofit.create(GetSettingsInterface.class);

        Response<JsonObject> response = anInterface.getSettings(httpsInfo.getGateway(),
                RequestBody.create(MediaType.parse("text/plain"), "N1=" + httpsInfo.getGatewayCode() + " \n N2=НастройкиТерминала \n N3=" + manager.getDCTNumber())).execute();

        JsonObject object = response.body();
        if(object != null) {
            JsonElement element = object.get("Результат");
            Settings settings = new Gson().fromJson(element, Settings.class);
            setSettings(settings);

            return object.toString();
        }
        else return null;
    }

    private void setSettings(Settings settings){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

        if(settings.hasTcdNumber()) editor.putString(context.getString(R.string.dct_number_key), settings.getDctNumber());
        if(settings.hasCommandsGatewayIpAddress()) editor.putString(context.getString(R.string.http_commands_ip_key), settings.getCommandsGatewayIpAddress());
        if(settings.hasCommandsGateway()) editor.putString(context.getString(R.string.commands_gateway_key), settings.getCommandsGateway());
        if(settings.hasCommandsGatewayKey())editor.putString(context.getString(R.string.commands_gateway_code_key), settings.getCommandsGatewayKey());
        if(settings.hasSmbForwardCatalog()) editor.putString(context.getString(R.string.path_on_server), settings.getSmbForwardCatalog());
        if(settings.hasSmbBackwardsCatalog()) editor.putString(context.getString(R.string.server_backwards_synchronization_path), settings.getSmbBackwardsCatalog());
        if(settings.hasSmbIpAddress()) editor.putString(context.getString(R.string.ip), settings.getSmbIpAddress());
        if(settings.hasSmbLogin()) editor.putString(context.getString(R.string.user_name), settings.getSmbLogin());
        if(settings.hasSmbPassword()) editor.putString(context.getString(R.string.user_password), settings.getSmbPassword());

        editor.apply();
    }

}
