package com.example.thebeginning.extended;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.example.thebeginning.R;
import com.example.thebeginning.settings.SettingsFragment;
import com.example.thebeginning.extended.base.ModelDialog;


/**
 * Created by Константин on 12.07.2017.
 */

public class GetSettingsDialog extends ModelDialog<SettingsDomain, GetSettingsModel> implements GetSettingsModel.ViewInterface {
    private View progressBar;
    private AlertDialog alertDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        progressBar = LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar_simple, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        alertDialog = builder.setTitle(R.string.http_get_settings).
                setView(progressBar).
                setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().cancel();
                    }
                }).create();

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().getSettings();
    }

    @Override
    protected GetSettingsModel createModel() {
        return new GetSettingsModel(this, getDomain());
    }

    @Override
    public void onTestFinished(String response) {
        if(response == null) response = getString(R.string.result_is_empty);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.settings_received).
                setMessage(response).
                setPositiveButton(R.string.ok, null).
                create().show();

        SettingsFragment settingsFragment = (SettingsFragment) getFragmentManager().
                findFragmentByTag(SettingsFragment.TAG);

        settingsFragment.refresh();

        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        GetSettingsDialog.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().getSettings();
                    }
                }).
                setCancelable(false).
                create().show();
    }
}
