package com.example.thebeginning.extended.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Константин on 11.07.2017.
 */

public abstract class ModelDialog<D, M extends BaseModel> extends DomainDialog<D> {
    private M model;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = createModel();
        if(model instanceof Model) ((Model) model).registerEventBus();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(model instanceof Model) ((Model) model).unregisterEventBus();
        model.disposeAllBackgroundTasks();
    }

    protected abstract M createModel();

    protected M getModel(){
        return model;
    }
}
