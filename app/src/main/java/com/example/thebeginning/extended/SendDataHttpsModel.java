package com.example.thebeginning.extended;

import android.app.Activity;
import android.util.Base64;
import android.util.Base64OutputStream;

import com.example.thebeginning.AbstractData;
import com.example.thebeginning.FileManager;
import com.example.thebeginning.HashMapManager;
import com.example.thebeginning.ListItem;
import com.example.thebeginning.XmlData;
import com.example.thebeginning.extended.base.BaseModel;
import com.example.thebeginning.extended.repository.network.SendDataInterface;
import com.example.thebeginning.extended_v2.data_types.HttpsInfo;
import com.example.thebeginning.extended_v2.repository.CertificateManager;
import com.example.thebeginning.extended_v2.repository.SharedPreferencesManager;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.observers.DisposableObserver;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Константин on 22.08.2017.
 */

public class SendDataHttpsModel extends BaseModel {
    private File cacheDir;
    private List<ListItem> listItems;
    private FileManager fileManager;
    private String initialLocalPath;
    private int level;

    private ViewInterface viewInterface;
    private Activity activity;

    private DisposableObserver<SendingUpdate> observer;

    public SendDataHttpsModel(File cacheDir, List<ListItem> listItems,
                              FileManager fileManager, int level,
                              ViewInterface viewInterface, Activity activity){
        this.cacheDir = cacheDir;
        this.listItems = listItems;
        this.fileManager = fileManager;
        initialLocalPath = fileManager.getLocalPath();
        this.level = level;
        this.viewInterface = viewInterface;
        this.activity = activity;
    }

    public void send(){
        viewInterface.clearBars();
        fileManager.goTo(initialLocalPath);

        Observable<SendingUpdate> observable = Observable.create(new ObservableOnSubscribe<SendingUpdate>() {
            @Override
            public void subscribe(ObservableEmitter<SendingUpdate> e) throws Exception {
                int size = listItems.size();
                for (int i = 0; i < size; i++){
                    ListItem listItem = listItems.get(i);
                    send(listItem, e);
                    e.onNext(new SendingUpdate(i + 1));
                }
                e.onComplete();
            }
        });

        observer = new DisposableObserver<SendingUpdate>() {
            @Override
            public void onNext(SendingUpdate value) {
                switch (value.getType()){
                    case SendingUpdate.START:
                        viewInterface.onFolderSendingStarted(value.getSize(), value.getFolderName());
                        break;
                    case SendingUpdate.PROGRESS:
                        viewInterface.onFolderSendingProgress(value.getProgress(), value.getSize());
                        break;
                    case SendingUpdate.SENT:
                        viewInterface.onFolderSent(value.getFolder());
                        break;
                }

                viewInterface.updateContentManager();
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }

            @Override
            public void onComplete() {
                viewInterface.onSendingFinished();
            }
        };

        viewInterface.onSendingStarted(listItems.size());
        subscribe(observable, observer);
    }

    public void terminate(){
        observer.dispose();
    }

    private void send(ListItem listItem, ObservableEmitter<SendingUpdate> e) throws Exception {
        clearCacheDir();

        String firstLevelFolder;
        if(level == 1){
            firstLevelFolder = fileManager.getCurrentFolderName();
            fileManager.goAhead(listItem.getName());
        }
        else{
            fileManager.goBack();
            firstLevelFolder = fileManager.getCurrentFolderName();
            fileManager.goAhead(listItem.getName());
        }

        //String uploadDir = fileManager.getUploadDir(listItem.getName());

        fileManager.duplicateModeXmlAsSent(cacheDir, listItem.getName(), activity);

        File[] files = fileManager.getFiles();

        FileManager cacheManager = new FileManager(cacheDir.getPath(), "");

        for (int i = 0; i < files.length; i++){
            if(!files[i].getName().equals("mode.xml")){
                cacheManager.writeFile(cacheManager, files[i]);
            }
        }

        AbstractData abstractData = fileManager.readFromFile("mode.xml", new XmlData());
        String content = abstractData.getContent();
        int index = content.indexOf("<ДатаСоздания>") + 14;
        String formattedDate = new SimpleDateFormat("yyyy/MM/dd").
                format(new SimpleDateFormat("dd-MM-yyyy HH-mm").
                        parse(content.substring(index, index + 16)));

        File zippedFile = zipFiles(cacheManager.getFiles());

        InputStream inputStream = new FileInputStream(zippedFile);

        byte[] buffer = new byte[zippedFile.length() < 7340032 ? (int) zippedFile.length() : 7340032];
        int bytesRead, count = 0;List<File> zippedFileParts = new ArrayList<>();
        long totalSize = 0;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            File file = new File(cacheDir, "mode_" + String.valueOf(count) + ".xml");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            Base64OutputStream output64 = new Base64OutputStream(fileOutputStream, Base64.DEFAULT);

            output64.write(buffer, 0, bytesRead);

            output64.close();
            fileOutputStream.close();

            zippedFileParts.add(file);

            ++count;
            totalSize += file.length();
        }

        inputStream.close();


        boolean result = send(firstLevelFolder, listItem.getName(), formattedDate,
                zippedFileParts.toArray(new File[zippedFileParts.size()]),
                totalSize,
                e);

        if(result){
            String currentDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(Calendar.getInstance().getTime());
            StringBuilder stringBuilder = new StringBuilder(content);
            if(content.contains("ДатаСинхронизации")){
                stringBuilder.replace(stringBuilder.indexOf("<ДатаСинхронизации>") + 19, stringBuilder.indexOf("</ДатаСинхронизации>"), currentDate);
                stringBuilder.replace(stringBuilder.indexOf("<КаталогСинхронизации>") + 22, stringBuilder.indexOf("</КаталогСинхронизации>"), listItem.getName());
            }
            else{
                index = stringBuilder.indexOf("</SystemData>") - 1;
                stringBuilder.insert(index, "<ДатаСинхронизации>" + currentDate + "</ДатаСинхронизации>" + "\n" + "<КаталогСинхронизации>" + listItem.getName() + "</КаталогСинхронизации>" + "\n");
            }
            abstractData.setContent(stringBuilder.toString());
            fileManager.writeToFile("mode.xml", abstractData);

            fileManager.goBack();

            HashMapManager hashMapManager = new HashMapManager(fileManager);
            hashMapManager.getHashMap();
            hashMapManager.put(fileManager.getChildLocalPath(listItem.getName()), true);
            hashMapManager.saveChanges();

            listItem.setState("Уже отправлено");
        }
        else throw new Exception("Transmission failed");
    }

    private boolean send(String firstLevelFolder, String secondLevelFolder,
                         String creationDate, File[] modeZipParts, long size,
                         ObservableEmitter<SendingUpdate> e) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        //HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        SharedPreferencesManager manager = new SharedPreferencesManager(activity);

        HttpsInfo httpsInfo = manager.getCommandsHttpsInfo();

        OkHttpClient okHttp = new OkHttpClient.Builder()
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        //eturn s.equals(sslSession.getPeerHost());
                        return true;
                    }
                })
                .sslSocketFactory(new CertificateManager().getDefaultContext().getSocketFactory())
                //.addInterceptor(logging)
                .connectTimeout(40, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(httpsInfo.getHttpsUrl())
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SendDataInterface anInterface = retrofit.create(SendDataInterface.class);

        e.onNext(new SendingUpdate(size, secondLevelFolder));
        long loaded = 0;
        for (int i = 0; i < modeZipParts.length; i++){

            ProgressRequestBody requestBody = new ProgressRequestBody("N1=" + httpsInfo.getGatewayCode() + " \n " +
                    "N2=ЗагрузитьКаталог \n " +
                    "N3=" + manager.getDCTNumber() + "\n" +
                    "N4=" + firstLevelFolder + "\n" +
                    "N5=" + secondLevelFolder + "\n" +
                    "N6=" + creationDate + "\n" +
                    "N7= " + String.valueOf(i + 1) + "\n" +
                    "N8= " + String.valueOf(modeZipParts.length) + "\n" +
                    "N30= \n",
                    modeZipParts[i],
                    loaded, size,
                    e);

            Response<JsonObject> response = anInterface.sendData(httpsInfo.getGateway(), requestBody).execute();

            JsonObject object = response.body();
            if(object != null) {
                JsonElement jsonElement = object.get("РезультатКод");
                if (jsonElement == null || !jsonElement.getAsString().equals("1")) return false;
            }
            else return false;

            loaded = requestBody.getLoaded();
        }

        return true;
    }

    private File zipFiles(File[] files) throws IOException {
        File zippedFile = new File(cacheDir, "mode.zip");
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zippedFile)));

        for (File i: files){
            ZipEntry zipEntry = new ZipEntry(i.getName());
            out.putNextEntry(zipEntry);

            FileInputStream inputStream = new FileInputStream(i);

            byte data[] = new byte[1024];
            int count;

            while ((count = inputStream.read(data)) != -1) {
                out.write(data, 0, count);
            }

            inputStream.close();
        }

        out.flush();
        out.close();

        return zippedFile;
    }

    private void clearCacheDir(){
        File[] files = cacheDir.listFiles();
        if(files != null){
            for (File i: files){
                deleteRecursiveCall(i);
            }
        }
    }

    private void deleteRecursiveCall(File file){
        if(file.isDirectory()){
            File[] files = file.listFiles();
            for(File i: files) this.deleteRecursiveCall(i);
        }
        file.delete();
    }

    public interface ViewInterface{
        void clearBars();

        void onSendingStarted(int folders);

        void onFolderSendingStarted(long size, String folder);
        void onFolderSendingProgress(long progress, long size);
        void onFolderSent(int folder);

        void onSendingFinished();

        void updateContentManager();

        void onError(Throwable e);
    }

    public class SendingUpdate{
        public static final int START = 0, PROGRESS = 1, SENT = 2;

        private int type;
        private String folderName;
        private long progress, size;
        private int folder;

        public SendingUpdate(long size, String folder){
            type = START;
            this.size = size;
            this.folderName = folder;
        }

        public SendingUpdate(long progress, long size){
            type = PROGRESS;
            this.progress = progress;
            this.size = size;
        }

        public SendingUpdate(int folder){
            type = SENT;
            this.folder = folder;
        }

        public int getType() {
            return type;
        }

        public int getFolder() {
            return folder;
        }

        public long getProgress() {
            return progress;
        }

        public long getSize() {
            return size;
        }

        public String getFolderName() {
            return folderName;
        }
    }

    public class ProgressRequestBody extends RequestBody{
        private String description;
        private File part;
        private long loaded, totalSize;
        private ObservableEmitter<SendingUpdate> e;

        public ProgressRequestBody(String description, File part,
                                   long loaded, long totalSize,
                                   ObservableEmitter<SendingUpdate> e){
            this.description = description;
            this.part = part;
            this.loaded = loaded;
            this.totalSize = totalSize;
            this.e = e;
        }

        @Override
        public MediaType contentType() {
            return MediaType.parse("text/plain");
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            sink.write(description.getBytes(), 0, description.getBytes().length);

            InputStream in = new FileInputStream(part);
            try {
                int read;
                byte[] buffer = new byte[204800];
                while ((read = in.read(buffer)) != -1) {
                    loaded += read;
                    e.onNext(new SendingUpdate(loaded, totalSize));

                    sink.write(buffer, 0, read);
                }
            }
            finally {
                in.close();
            }
        }

        @Override
        public long contentLength() throws IOException {
            return description.getBytes().length + part.length();
        }

        public long getLoaded() {
            return loaded;
        }
    }
}
