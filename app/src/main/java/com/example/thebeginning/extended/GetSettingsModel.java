package com.example.thebeginning.extended;

import com.example.thebeginning.extended.base.BaseModel;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 12.07.2017.
 */

public class GetSettingsModel extends BaseModel {
    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable getSettings;

    public GetSettingsModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void getSettings(){
        DisposableSingleObserver<String> observer = new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String value) {
                viewInterface.onTestFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getSettings = observer;

        subscribe(Single.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return domainInterface.getSettings();
            }
        }), observer);
    }

    public void cancel(){
        getSettings.dispose();
    }

    public interface ViewInterface{
        void onTestFinished(String response);
        void onError(Throwable e);
    }

    public interface DomainInterface{
        String getSettings() throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException;
    }
}
