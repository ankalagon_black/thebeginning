package com.example.thebeginning.extended.base;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class Model extends BaseModel {
    public void registerEventBus(){
        EventBus.getDefault().register(this);
    }

    public void unregisterEventBus(){
        EventBus.getDefault().unregister(this);
    }
}
