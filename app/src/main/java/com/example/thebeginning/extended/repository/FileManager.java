package com.example.thebeginning.extended.repository;

import android.app.Activity;
import android.os.Build;
import android.os.Environment;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Константин on 04.07.2017.
 */

public class FileManager {

    public List<String> getDirectories(Activity activity){
        final List<String> directories = new ArrayList<>();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            File[] externalDirs = activity.getExternalFilesDirs(null);

            for (File i: externalDirs){
                if(i != null) {
                    String path = i.getPath();

                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) path = path.split("/Android")[0];

                    directories.add(path);
                }
            }
        }
        else {
            final String rawSecondaryStorageStr = System.getenv("SECONDARY_STORAGE");

            if(!TextUtils.isEmpty(rawSecondaryStorageStr)){
                final String[] rawSecondaryStorages = rawSecondaryStorageStr.split(File.pathSeparator);
                Collections.addAll(directories, rawSecondaryStorages);
            }

            String externalStorage = System.getenv("EXTERNAL_STORAGE");
            if(!TextUtils.isEmpty(externalStorage)) directories.add(externalStorage);
        }

        return directories;
    }

    public List<String> getExternalDirectories(Activity activity){
        final List<String> directories = new ArrayList<>();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            File[] externalDirs = activity.getExternalFilesDirs(null);

            for (File i: externalDirs){
                if(i != null) {
                    String path = i.getPath();

                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) path = path.split("/Android")[0];

                    boolean addPath;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        addPath = Environment.isExternalStorageRemovable(i);
                    else
                        addPath = Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(i));

                    if (addPath) directories.add(path);
                }
            }
        }
        else {
            String externalStorage = System.getenv("EXTERNAL_STORAGE");
            if(!TextUtils.isEmpty(externalStorage)) directories.add(externalStorage);
        }

        return directories;
    }

    public String getDefaultDirectory(){
        return Environment.getExternalStorageDirectory().getPath();
    }
}
