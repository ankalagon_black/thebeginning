package com.example.thebeginning.extended.repository.network;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Константин on 22.08.2017.
 */

public interface SendDataInterface {
    @POST("{type}")
    Call<JsonObject> sendData(@Path("type") String path, @Body RequestBody requestBody);
}
