package com.example.thebeginning.extended;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginning.FileManager;
import com.example.thebeginning.ListItem;
import com.example.thebeginning.MainActivity;
import com.example.thebeginning.R;
import com.example.thebeginning.TextProgressBar;
import com.example.thebeginning.extended.base.SupportModelDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import static com.example.thebeginning.MainActivity.externalStoragePath;

/**
 * Created by Константин on 22.08.2017.
 */

public class SendDataHttps extends SupportModelDialog<SendDataHttpsModel> implements SendDataHttpsModel.ViewInterface{
    public static final String LIST_ITEMS_KEY = "ListItemsKey";
    public static final String FILE_MANAGER_KEY = "FileManagerKey";
    public static final String LEVEL_KEY = "LevelKey";

    public static SendDataHttps get(ArrayList<ListItem> listItems, FileManager fileManager, int level){
        SendDataHttps sendDataHttps = new SendDataHttps();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(LIST_ITEMS_KEY, listItems);
        bundle.putParcelable(FILE_MANAGER_KEY, fileManager);
        bundle.putInt(LEVEL_KEY, level);

        sendDataHttps.setArguments(bundle);
        return sendDataHttps;
    }

    private TextView currentFolder;
    private TextProgressBar byteBar, folderBar;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.send_data_https, null);

        currentFolder = view.findViewById(R.id.progress_bar_current_folder);

        byteBar = view.findViewById(R.id.progress_bar_byte_bar);
        folderBar = view.findViewById(R.id.progress_bar_folder_bar);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        return builder.setTitle(R.string.transmission_in_progress).
                setView(view).
                setCancelable(false).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().terminate();
                    }
                }).
                create();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().send();
    }

    @Override
    protected SendDataHttpsModel createModel() {
        Bundle bundle = getArguments();

        ArrayList<ListItem> listItems = bundle.getParcelableArrayList(LIST_ITEMS_KEY);
        FileManager fileManager = bundle.getParcelable(FILE_MANAGER_KEY);
        int level = bundle.getInt(LEVEL_KEY);

        File file = new File(externalStoragePath + "/TheBeginning_certificate");
        file.mkdirs();

        return new SendDataHttpsModel(getActivity().getCacheDir(), listItems, fileManager, level, this, getActivity());
    }

    @Override
    public void clearBars() {
        byteBar.setProgress(0);
        byteBar.setText("");

        folderBar.setProgress(0);
        folderBar.setText("");
    }

    @Override
    public void onSendingStarted(int folders) {
        folderBar.setProgress(0);
        folderBar.setMax(folders);

        folderBar.setText(String.format(Locale.US, getString(R.string.out_of_folders), 0, folders));
    }

    @Override
    public void onFolderSendingStarted(long size, String folder) {
        currentFolder.setText(String.format(Locale.US, getString(R.string.current_folder), folder));

        float sizeF = (float) size / 1024.f;

        byteBar.setProgress(0);
        byteBar.setMax((int) sizeF);

        byteBar.setText(String.format(Locale.US, getString(R.string.out_of_as_float_with_2_decimal), 0f, sizeF));
    }

    @Override
    public void onFolderSendingProgress(long progress, long size) {
        float progressF = (float) progress / 1024.f, sizeF = (float) size / 1024.f;

        byteBar.setProgress((int) progressF);

        byteBar.setText(String.format(Locale.US, getString(R.string.out_of_as_float_with_2_decimal), progressF, sizeF));
    }

    @Override
    public void onFolderSent(int folder) {
        folderBar.setProgress(folder);

        folderBar.setText(String.format(Locale.US, getString(R.string.out_of_folders), folder, folderBar.getMax()));
    }

    @Override
    public void onSendingFinished() {
        Toast.makeText(getActivity(), R.string.transmission_succesfully_finished, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void updateContentManager() {
        ((MainActivity) getActivity()).updateContent();
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SendDataHttps.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().send();
                    }
                }).
                setCancelable(false).
                create().show();
    }
}
