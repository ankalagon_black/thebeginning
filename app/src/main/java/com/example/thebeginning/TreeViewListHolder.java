package com.example.thebeginning;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by apple on 12.08.16.
 */
public class TreeViewListHolder extends TreeNode.BaseNodeViewHolder<String> {

    private TextView textView;
    private Context context;
    private String v;

    public TreeViewListHolder(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public View createNodeView(TreeNode node, String value) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.create_barcode_tree_item, null, false);
        textView = view.findViewById(R.id.create_barcode_tree_item_text);
        int level = node.getLevel();
        if(level == 1){
            textView.setText(value);
            textView.setPadding(5, 10 , 10 , 10);
        }
        if(v != null) textView.setText(v);
        return view;
    }

    public void setV(String v){
        this.v = v;
    }

    public TextView getTextView(){
        return textView;
    }
}
