package com.example.thebeginning;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Константин on 03.04.2016.
 */
public class PhotoData extends AbstractData{

    protected byte[] jpegBytes;

    public PhotoData(){
    }

    public void write(FileOutputStream outputStream){
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(jpegBytes);
            objectOutputStream.close();
        } catch (IOException e) {
            Log.v(PhotoData.class.toString(), e.toString());
        }
    }

    public void read(FileInputStream inputStream){
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            jpegBytes = (byte[]) objectInputStream.readObject();
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            Log.v(PhotoData.class.toString(), e.toString());
        }
    }

    public Bitmap getBitmap(){
        return BitmapFactory.decodeByteArray(jpegBytes, 0, jpegBytes.length);
    }

    public byte[] getJpegBytes(){
        return jpegBytes;
    }

    public void setJpegBytes(byte[] bytes){
        jpegBytes = bytes;
    }
}
