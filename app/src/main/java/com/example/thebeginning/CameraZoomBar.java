package com.example.thebeginning;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.SeekBar;

/**
 * Created by Константин on 03.07.2016.
 */
public class CameraZoomBar extends SeekBar {

    private Camera camera;
    private SurfaceView surfaceView;
    private boolean isInitialized = false;
    private int screenLenght;

    public CameraZoomBar(Context context) {
        super(context);
    }

    public CameraZoomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
        if (!isInitialized) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.isZoomSupported()) {
                final android.os.Handler handler = new android.os.Handler();
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        CameraZoomBar.this.setVisibility(View.INVISIBLE);
                    }
                };
                final int maxZoom = parameters.getMaxZoom();
                setMax(maxZoom);
                setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        Camera.Parameters parameters = CameraZoomBar.this.camera.getParameters();
                        parameters.setZoom(progress);
                        CameraZoomBar.this.camera.setParameters(parameters);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        handler.removeCallbacks(runnable);
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        handler.postDelayed(runnable, 1000);
                    }
                });
                surfaceView.setOnTouchListener(new OnTouchListener() {

                    private boolean isTwoFingers = false;
                    private float lastDist, currentDist;
                    private float threshold = screenLenght / maxZoom;

                    private float getPowerOfTwoDistance(MotionEvent event) {
                        float x = event.getX(0) - event.getX(1), y = event.getY(0) - event.getY(1);
                        return x * x + y * y;
                    }

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_POINTER_UP:
                                if (event.getPointerCount() == 3) {
                                    isTwoFingers = true;
                                    CameraZoomBar.this.setVisibility(View.VISIBLE);
                                    lastDist = getPowerOfTwoDistance(event);
                                    handler.removeCallbacks(runnable);
                                } else {
                                    isTwoFingers = false;
                                    handler.postDelayed(runnable, 1000);
                                }
                                break;
                            case MotionEvent.ACTION_POINTER_DOWN:
                                if (event.getPointerCount() == 2) {
                                    isTwoFingers = true;
                                    CameraZoomBar.this.setVisibility(View.VISIBLE);
                                    lastDist = getPowerOfTwoDistance(event);
                                    handler.removeCallbacks(runnable);
                                } else {
                                    isTwoFingers = false;
                                    handler.postDelayed(runnable, 1000);
                                }
                                break;
                            case MotionEvent.ACTION_MOVE:
                                if (isTwoFingers) {
                                    currentDist = getPowerOfTwoDistance(event);
                                    float diff = currentDist - lastDist;
                                    if (Math.abs(diff) > threshold) {
                                        CameraZoomBar.this.incrementProgressBy(diff > 0 ? 1 : -1);
                                        lastDist = currentDist;
                                    }
                                }
                                break;
                        }
                        return true;
                    }
                });
            }
            isInitialized = true;
        }
    }

    public void setScreenLenght(int x, int y) {
        this.screenLenght = (x * x + y * y) / 4;
    }

    public void setSurfaceView(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
    }
}
