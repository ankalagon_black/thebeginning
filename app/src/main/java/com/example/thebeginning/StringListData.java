package com.example.thebeginning;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 03.04.2016.
 */
public class StringListData extends AbstractData {

    private List<String> list;

    public StringListData(){
        list = new ArrayList<>();
    }

    @Override
    public void write(FileOutputStream outputStream) {
        try{
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            for(String i: list){
                writer.write(i);
                writer.write('\r');
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            Log.v(StringListData.class.toString(), e.toString());
        }
    }

    @Override
    public void read(FileInputStream inputStream) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while((line = reader.readLine()) != null) list.add(line);
            reader.close();
        } catch (IOException e) {
            Log.v(StringListData.class.toString(), e.toString());
        }
    }

    public List<String> getList(){
        return list;
    }

    public void add(String string){
        list.add(string);
    }

    public void clear(){
        list.clear();
    }
}
