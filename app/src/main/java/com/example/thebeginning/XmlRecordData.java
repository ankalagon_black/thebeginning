package com.example.thebeginning;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 20.04.2016.
 */
public class XmlRecordData extends AbstractData {

    private List<XmlRecord> records;

    public XmlRecordData(){
        records = new ArrayList<>();
    }

    @Override
    public void write(FileOutputStream outputStream) {
        try {
            XmlSerializer xmlSerializer = Xml.newSerializer();
            xmlSerializer.setOutput(outputStream, "UTF-8");
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            xmlSerializer.startTag("", "root");
            for(XmlRecord i: records){
                String parentTag =  i.getParentTag();
                xmlSerializer.startTag("", parentTag);
                List<TagRecord> tagRecords = i.getTagRecords();
                for(TagRecord j: tagRecords){
                    String tag = j.getTag();
                    xmlSerializer.startTag("", tag);
                    xmlSerializer.text(j.getValue());
                    xmlSerializer.endTag("", tag);
                }
                xmlSerializer.endTag("", parentTag);
            }
            xmlSerializer.endTag("", "root");
            xmlSerializer.endDocument();
        } catch (IOException e) {
           Log.v(XmlRecordData.class.toString(), e.toString());
        }
    }

    @Override
    public void read(FileInputStream inputStream) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(inputStream, "UTF-8");
            int eventType = xpp.getEventType();
            String currentParentTag = null;
            String currentTag = null;
            XmlRecord xmlRecord = null;
            String domain = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (currentTag != null) currentParentTag = currentTag;
                        currentTag = xpp.getName();
                        switch (currentTag) {
                            case "Fields":
                                domain = "Fields";
                                break;
                            case "Pakets":
                                domain = "Pakets";
                                break;
                            case "PhotoFields":
                                domain = "PhotoFields";
                                break;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (xpp.getName().equals(currentParentTag)) {
                            records.add(xmlRecord);
                            xmlRecord = null;
                        }
                        break;
                    case XmlPullParser.TEXT:
                        String text = xpp.getText();
                        if (!text.contains("\n") || (xmlRecord != null && currentTag != null)) {
                            if (xmlRecord == null) xmlRecord = new XmlRecord(currentParentTag);
                            if(text.contains("\n")) text = "";
                            xmlRecord.add(currentTag, text);
                            if(domain != null) xmlRecord.setDomain(domain);
                            currentTag = null;
                        }
                        break;
                }
                eventType = xpp.next();
            }
        }
        catch (IOException | XmlPullParserException e){
            Log.v(XmlRecordData.class.toString(), e.toString());
        }
    }

    public List<XmlRecord> getRecords() {
        return records;
    }

    public XmlRecord getRecordByParentTag(String parentTag){
        for(XmlRecord i: records){
            if(i.getParentTag().equals(parentTag)) return i;
        }
        return null;
    }

    public List<XmlRecord> getRecordsByParentTag(String parentTag){
        List<XmlRecord> xmlRecords = new ArrayList<>();

        for (XmlRecord i: records){
            String temp = i.getParentTag();
            if(temp != null && temp.equals(parentTag)) xmlRecords.add(i);
        }

        return xmlRecords;
    }

    public List<XmlRecord> getRecordsByDomain(String domain){
        List<XmlRecord> xmlRecords = new ArrayList<>();

        for (XmlRecord i: records){
            String temp = i.getDomain();
            if(temp != null && temp.equals(domain)) xmlRecords.add(i);
        }

        return xmlRecords;
    }

    public void addRecord(XmlRecord xmlRecord){
        records.add(xmlRecord);
    }
}




