package com.example.thebeginning;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Константин on 21.10.2016.
 */

public class ArrayAdapterChoiceRecord extends ArrayAdapter {

    private String[] strings;
    private Integer[] integers;

    public ArrayAdapterChoiceRecord(Context context, int recourse, String[] strings, Integer[] images) {
        super(context, recourse, strings);
        this.strings = strings;
        this.integers = images;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View view = layoutInflater.inflate(R.layout.create_barcode_list_item_choice_row, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textView = view.findViewById(R.id.create_barcode_list_item_choice_row_text_view);
            viewHolder.imageView = view.findViewById(R.id.create_barcode_list_item_choice_row_image_view);
            convertView = view;
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.textView.setText(strings[position]);
        if(integers[position] != null) viewHolder.imageView.setImageResource(integers[position]);
        return convertView;
    }

    public static class ViewHolder{
        public TextView textView;
        public ImageView imageView;
    }
}
