package com.example.thebeginning;

import android.support.annotation.NonNull;

/**
 * Created by Константин on 05.12.2015.
 */
public class Element implements Comparable<Element>{

    private String name;
    private String isSent;

    public Element(String name, String isSent){
        this.name = name;
        this.isSent = isSent;
    }

    @Override
    public int compareTo(@NonNull Element element){
        return this.name.compareToIgnoreCase(element.getName());
    }

    public String getName(){
        return name;
    }

    public String getIsSent(){
        return isSent;
    }
}
