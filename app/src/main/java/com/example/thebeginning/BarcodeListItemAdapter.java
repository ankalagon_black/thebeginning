package com.example.thebeginning;

/**
 * Created by Константин on 26.05.2016.
 */
//public class BarcodeListItemAdapter extends ArrayAdapter<String> {
//
//    private List<String> list;
//
//    public BarcodeListItemAdapter(Context context, int resource, List list) {
//        super(context, resource, list);
//        this.list = list;
//    }
//
//    private static class ViewHolder {
//        public TextView indexView;
//        public TextView textView;
//        public ImageButton searchButton;
//        public ImageButton deleteButton;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        if(convertView == null){
//            convertView = LayoutInflater.accountedRecordFrom(getContext()).inflate(R.layout.create_barcode_list_item, null);
//
//            ViewHolder viewHolder = new ViewHolder();
//            viewHolder.indexView = (TextView) convertView.findViewById(R.id.create_barcode_index);
//            viewHolder.textView = (TextView) convertView.findViewById(R.id.create_barcode_list_item_text_view);
//            viewHolder.searchButton = (ImageButton) convertView.findViewById(R.id.create_barcode_list_item_search);
//            viewHolder.deleteButton = (ImageButton) convertView.findViewById(R.id.create_barcode_list_item_image_button);
//            convertView.setTag(viewHolder);
//        }
//
//        final String item = requestData(position);
//        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
//        viewHolder.indexView.setText("№" + String.valueOf(position + 1));
//        viewHolder.textView.setText(item);
//        viewHolder.searchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                Activity activity = (Activity) getContext();
//                DialogDisplayDBQuery dialog = new DialogDisplayDBQuery();
//                int commaPosition = item.lastIndexOf(",");
//                String searchString = item.substring(0, commaPosition);
//                Bundle bundle = new Bundle();
//                bundle.putString(DialogDisplayDBQuery.SEARCH_VALUE_KEY ,searchString);
//                dialog.setArguments(bundle);
//                //replace
//                dialog.show(activity.getFragmentManager(), "1");
//            }
//        });
//        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View data) {
//                list.remove(position);
//                notifyDataSetChanged();
//            }
//        });
//        return convertView;
//    }
//}
