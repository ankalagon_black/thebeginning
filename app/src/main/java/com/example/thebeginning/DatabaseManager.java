package com.example.thebeginning;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 26.07.2016.
 */
public class DatabaseManager {

    public static final String DATABASE_NAME = "Inventory.db";

    private static final String MAIN_TABLE = "Inventory";
    private static final String DISPLAY_PATTERN_TABLE = "DisplayPattern";
    private static final String SUPPORT_TAGS_TABLE = "SupportTags";
    //0 - quantity, 1 - DateOfUpload
    private static final String SEARCH_TAGS_TABLE = "SearchTags";
    private static final String RESULT_TAGS_TABLE = "ResultTags";
    public static final String LIST_INFO_TABLE = "ListInfo";

    public static final String CONTENT_FIELD = "Content";
    public static final String ROW_FIELD = "Row";
    public static final String COLUMN_FIELD = "Column";
    public static final String IS_TITLE_FIELD = "IsTitle";
    public static final String BACKGROUND_COLOR_FIELD = "BackgroundColor";
    public static final String TEXT_COLOR_FIELD = "TextColor";
    public static final String TEXT_WIDTH_FIELD = "TextWidth";
    public static final String IS_FONT_BOLD_FIELD = "IsFontBold";
    public static final String TEXT_SIZE_FIELD = "TextSize";
    public static final String SHOW_FIELD = "Show";

    public static final String ID = "id";

    public static final String TAG_FIELD = "Tag";

    public static final String AGR_CNT_FIELD = "Cnt";

    private SQLiteDatabase database;

    private String fileName;

    public DatabaseManager(String fileName){
        this.fileName = fileName;
    }

    public static void updateFromXmlFile(FileManager databaseFileManager, FileManager xmlFileManager, DialogDatabaseUpdateProgressBar dialogDatabaseUpdateProgressBar, String fileName, Activity activity){
        List<XmlRecord> xmlPattern = ((XmlRecordData) xmlFileManager.readFromFile(fileName, new XmlRecordData())).getRecords();

        String dataFileName = "pak.xml";
        List<TagRecord> mainTablePattern = new ArrayList<>();
        List<ContentValues> displayPatternData = new ArrayList<>(), searchTableData = new ArrayList<>(),
                resultTableData = new ArrayList<>(), listInfoTableData = new ArrayList<>(), supportTableData = new ArrayList<>();
        for(XmlRecord i: xmlPattern) {
            if(i.getParentTag().equals("Field")){
                String tag = i.getTagValueByTag("Tag");
                if(tag != null){
                    String row = i.getTagValueByTag("DataRow"), column = i.getTagValueByTag("DataColumn");
                    String content;
                    if(row != null && column != null) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(CONTENT_FIELD, tag);
                        contentValues.put(ROW_FIELD, Integer.parseInt(row));
                        contentValues.put(COLUMN_FIELD, Integer.parseInt(column));
                        content = i.getTagValueByTag("Title");
                        if(content != null) contentValues.put(IS_TITLE_FIELD, content);
                        else contentValues.put(IS_TITLE_FIELD, activity.getString(R.string.name));
                        content = i.getTagValueByTag("DataBackgroundColor");
                        if(content != null) contentValues.put(BACKGROUND_COLOR_FIELD, determineColor(content));
                        content = i.getTagValueByTag("DataTextColor");
                        if(content != null) contentValues.put(TEXT_COLOR_FIELD, determineColor(content));
                        content = i.getTagValueByTag("DataWidth");
                        if(content != null) contentValues.put(TEXT_WIDTH_FIELD, Integer.parseInt(content));
                        content = i.getTagValueByTag("DataUseBoldFont");
                        if(content != null) contentValues.put(IS_FONT_BOLD_FIELD, Integer.parseInt(content));
                        content = i.getTagValueByTag("DataTextSize");
                        if(content != null) contentValues.put(TEXT_SIZE_FIELD, Integer.parseInt(content));
                        content = i.getTagValueByTag("DataShow");
                        if(content != null) contentValues.put(SHOW_FIELD, Integer.getInteger(content));
                        else contentValues.put(SHOW_FIELD, 1);
                        displayPatternData.add(contentValues);
                    }

                    content = i.getTagValueByTag("Title");
                    if(content != null){
                        row = i.getTagValueByTag("TitleRow");
                        column = i.getTagValueByTag("TitleColumn");
                        if(row != null && column != null){
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(CONTENT_FIELD, content);
                            contentValues.put(ROW_FIELD, Integer.parseInt(row));
                            contentValues.put(COLUMN_FIELD, Integer.parseInt(column));
                            contentValues.put(IS_TITLE_FIELD, "1");
                            content = i.getTagValueByTag("TitleBackgroundColor");
                            if(content != null) contentValues.put(BACKGROUND_COLOR_FIELD, determineColor(content));
                            content = i.getTagValueByTag("TitleTextColor");
                            if(content != null) contentValues.put(TEXT_COLOR_FIELD, determineColor(content));
                            content = i.getTagValueByTag("TitleWidth");
                            if(content != null) contentValues.put(TEXT_WIDTH_FIELD, Integer.parseInt(content));
                            content = i.getTagValueByTag("TitleUseBoldFont");
                            if(content != null) contentValues.put(IS_FONT_BOLD_FIELD, Integer.parseInt(content));
                            content = i.getTagValueByTag("TitleTextSize");
                            if(content != null) contentValues.put(TEXT_SIZE_FIELD, Integer.parseInt(content));
                            content = i.getTagValueByTag("TitleShow");
                            if(content != null) contentValues.put(SHOW_FIELD, Integer.getInteger(content));
                            else contentValues.put(SHOW_FIELD, 1);
                            displayPatternData.add(contentValues);
                        }
                    }

                    content = i.getTagValueByTag("Type");
                    mainTablePattern.add(new TagRecord(tag, content != null ? determineAffinity(content) : "TEXT"));
                }
            }
            else if(i.getParentTag().equals("Common")){
                TagRecord tagRecord = i.getTagRecordByTag("FieldQuantity");
                if(tagRecord != null){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, tagRecord.getValue());
                    supportTableData.add(contentValues);
                }
                tagRecord = i.getTagRecordByTag("DBDataFile");
                if(tagRecord != null) dataFileName = tagRecord.getValue();
                List<TagRecord> barcodeFields = i.getTagRecordByMask("FieldBarcode");
                for(TagRecord j: barcodeFields){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, j.getValue());
                    searchTableData.add(contentValues);
                }
                List<TagRecord> RFIDFields = i.getTagRecordByMask("FieldRFID");
                for(TagRecord j: RFIDFields){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, j.getValue());
                    searchTableData.add(contentValues);
                }

                List<TagRecord> nameFields = i.getTagRecordByMask("FieldName");
                for(TagRecord j: nameFields ){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, j.getValue());
                    searchTableData.add(contentValues);
                }

                List<TagRecord> numberFields = i.getTagRecordByMask("FieldNumber");
                for(TagRecord j: numberFields){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, j.getValue());
                    searchTableData.add(contentValues);
                }

                List<TagRecord> resultFields = i.getTagRecordByMask("FieldResult");
                for(TagRecord j: resultFields){
                    if(!j.getValue().contains("Group")) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(TAG_FIELD, j.getValue());
                        resultTableData.add(contentValues);
                    }
                }

                List<TagRecord> listInfoFields = i.getTagRecordByMask("FieldList");
                for(TagRecord j: listInfoFields){
                    if(!j.getValue().contains("Group")) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(TAG_FIELD, j.getValue());
                        listInfoTableData.add(contentValues);
                    }
                }
            }
        }

        List<XmlRecord> xmlData = ((XmlRecordData) xmlFileManager.readFromFile(dataFileName, new XmlRecordData())).getRecords();
        List<ContentValues> mainTableData = new ArrayList<>();
        for (XmlRecord i: xmlData){
            if (!i.getParentTag().equals("Common")) {
                ContentValues contentValues = new ContentValues();
                List<TagRecord> tagRecords = i.getTagRecords();
                for(TagRecord j: tagRecords){
                    for(TagRecord k: mainTablePattern){
                        if(j.getTag().equals(k.getTag())){
                            contentValues.put(j.getTag(), j.getValue());
                            break;
                        }
                    }
                }
                mainTableData.add(contentValues);
            }
            else{
                TagRecord tagRecord = i.getTagRecordByTag("DateOfDownload");
                if(tagRecord != null){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TAG_FIELD, tagRecord.getValue());
                    supportTableData.add(contentValues);
                }
            }
        }

        DatabaseManager dataBaseManager = new DatabaseManager(fileName);
        dataBaseManager.open(databaseFileManager);
        dataBaseManager.udpate(mainTablePattern, displayPatternData, mainTableData, supportTableData, searchTableData, resultTableData, listInfoTableData, dialogDatabaseUpdateProgressBar);
    }

    public boolean open(FileManager fileManager){
        try {
            String name = fileManager.getCurrentFolderName();
            fileManager.goBack();
            database = SQLiteDatabase.openDatabase(fileManager.getChildFullPath(DATABASE_NAME), null, SQLiteDatabase.CREATE_IF_NECESSARY);
            fileManager.goAhead(name);
        }
        catch (Exception e){
            return false;
        }
        return true;
    }

    public String getFileName(){
        return fileName;
    }

    public void udpate(List<TagRecord> mainTablePattern, List<ContentValues> displayPatternData, List<ContentValues> mainTableData,
                       List<ContentValues> supportTableData, List<ContentValues> searchTableData, List<ContentValues> resultTableData,
                       List<ContentValues> listInfoTableData, DialogDatabaseUpdateProgressBar dialogDatabaseUpdateProgressBar){
        database.execSQL("DROP TABLE IF EXISTS " + MAIN_TABLE + ";");
        database.execSQL("DROP TABLE IF EXISTS " + DISPLAY_PATTERN_TABLE + ";");
        database.execSQL("DROP TABLE IF EXISTS " + SUPPORT_TAGS_TABLE + ";");
        database.execSQL("DROP TABLE IF EXISTS " + SEARCH_TAGS_TABLE + ";");
        database.execSQL("DROP TABLE IF EXISTS " + RESULT_TAGS_TABLE + ";");
        database.execSQL("DROP TABLE IF EXISTS " + LIST_INFO_TABLE + ";");

        String displayPatternTable = "CREATE TABLE " + DISPLAY_PATTERN_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + CONTENT_FIELD + " TEXT, " +
                ROW_FIELD + " INTEGER, " + COLUMN_FIELD + " INTEGER, " + IS_TITLE_FIELD + " INTEGER, " + BACKGROUND_COLOR_FIELD + " TEXT, " + TEXT_COLOR_FIELD + " TEXT, " +
                TEXT_WIDTH_FIELD + " INTEGER, " + IS_FONT_BOLD_FIELD + " TEXT, " + TEXT_SIZE_FIELD + " INTEGER, " + SHOW_FIELD + " INTEGER);";
        database.execSQL(displayPatternTable);
        StringBuilder mainTable = new StringBuilder("CREATE TABLE " + MAIN_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT");
        for(TagRecord i: mainTablePattern) mainTable.append(", " + i.getTag() + " " + i.getValue());
        mainTable.append(");");
        database.execSQL(mainTable.toString());
        database.execSQL("CREATE TABLE " + SUPPORT_TAGS_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + TAG_FIELD + " TEXT);");
        database.execSQL("CREATE TABLE " + SEARCH_TAGS_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + TAG_FIELD + " TEXT);");
        database.execSQL("CREATE TABLE " + RESULT_TAGS_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + TAG_FIELD + " TEXT);");
        database.execSQL("CREATE TABLE " + LIST_INFO_TABLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + TAG_FIELD + " TEXT);");

        dialogDatabaseUpdateProgressBar.initialize(displayPatternData.size() + mainTableData.size()
                + searchTableData.size() + resultTableData.size() + listInfoTableData.size());

        for(ContentValues i: displayPatternData){
            database.insert(DISPLAY_PATTERN_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
        for(ContentValues i: mainTableData){
            database.insert(MAIN_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
        for(ContentValues i: supportTableData){
            database.insert(SUPPORT_TAGS_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
        for(ContentValues i: searchTableData){
            database.insert(SEARCH_TAGS_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
        for(ContentValues i: resultTableData){
            database.insert(RESULT_TAGS_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
        for(ContentValues i: listInfoTableData){
            database.insert(LIST_INFO_TABLE, null, i);
            dialogDatabaseUpdateProgressBar.increment();
        }
    }

    public Cursor search(String value, boolean includeId) {
        Cursor cursor = database.rawQuery("SELECT " + TAG_FIELD + " FROM " + SEARCH_TAGS_TABLE, null);
        int size = cursor.getCount();
        StringBuilder dataQuery = new StringBuilder("SELECT * FROM " + MAIN_TABLE + " WHERE");
        String[] values = new String[size];
        cursor.moveToFirst();
        dataQuery.append(" " + cursor.getString(cursor.getColumnIndex(TAG_FIELD)) + " = ?");
        values[0] = value;
        cursor.moveToNext();
        if(includeId) dataQuery.append(" OR id = ?");
        for(int i = 1; i < size; ++i){
            dataQuery.append(" OR " + cursor.getString(cursor.getColumnIndex(TAG_FIELD)) + " = ?");
            values[i] = value;
            cursor.moveToNext();
        }
        cursor.close();
        return database.rawQuery(dataQuery.toString(), values);
    }

    public List<BarcodeRecord> searchFor(String value, boolean full){
        Cursor cursor = database.rawQuery("SELECT " + TAG_FIELD + " FROM " + SEARCH_TAGS_TABLE, null);
        int size = cursor.getCount();
        StringBuilder dataQuery = new StringBuilder("SELECT * FROM " + MAIN_TABLE + " WHERE");
        String[] values = new String[size];
        cursor.moveToFirst();
        String compare = "=";
        if(!full){
            value = "%" + value + "%";
            compare = "like";
        }
        String firstTag = cursor.getString(cursor.getColumnIndex(TAG_FIELD));
        dataQuery.append(" " + firstTag  + " " + compare + " ?");
        values[0] = value;
        cursor.moveToNext();
        for (int i = 1; i < size; ++i) {
            dataQuery.append(" OR " + cursor.getString(cursor.getColumnIndex(TAG_FIELD)) + " " + compare + " ?");
            values[i] = value;
            cursor.moveToNext();
        }
        cursor.close();
        Cursor result = database.rawQuery(dataQuery.toString(), values);
        size = result.getCount() > 50 ? 50 : result.getCount();
        Cursor quantityCursor = getQuantityTag();
        quantityCursor.moveToFirst();
        String quantityTag = quantityCursor.getString(quantityCursor.getColumnIndex(TAG_FIELD));
        quantityCursor.close();
        List<BarcodeRecord> records = new ArrayList<>(size);
        result.moveToFirst();
        for(int i = 0; i < size; i++){
            float quantity = result.getFloat(result.getColumnIndex(quantityTag));
            records.add(new BarcodeRecord(result.getInt(result.getColumnIndex(ID)), result.getString(result.getColumnIndex(firstTag)), quantity, quantity, 0));
            result.moveToNext();
        }
        result.close();
        return records;
    }

    public Cursor getRecordWithId(int id){
        return database.rawQuery("SELECT * FROM " + MAIN_TABLE + " WHERE id = ?", new String[] {String.valueOf(id)});
    }

    public Cursor getAllMainTableRecords(){
        return database.rawQuery("SELECT * FROM " + MAIN_TABLE, null);
    }

    public Cursor getFirstSerachTag(){
        return database.rawQuery("SELECT " + TAG_FIELD + " FROM " + SEARCH_TAGS_TABLE + " LIMIT 1", null);
    }

    public Cursor getDisplayPattern(){
        return database.rawQuery("SELECT " + CONTENT_FIELD + ", " + ROW_FIELD + ", " + COLUMN_FIELD + ", " + IS_TITLE_FIELD + ", " +
                BACKGROUND_COLOR_FIELD + ", " + TEXT_COLOR_FIELD + ", " + IS_FONT_BOLD_FIELD + ", " + TEXT_WIDTH_FIELD + ", " + TEXT_SIZE_FIELD + ", " + SHOW_FIELD +
                " FROM " + DISPLAY_PATTERN_TABLE + " ORDER BY " + ROW_FIELD + ", " + COLUMN_FIELD, null);
    }

    public Cursor getQuantityTag(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT " + TAG_FIELD  + " FROM " + SUPPORT_TAGS_TABLE + " WHERE id = ?", new String[] {"1"});
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getDownloadDate(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT " + TAG_FIELD  + " FROM " + SUPPORT_TAGS_TABLE + " WHERE id = ?", new String[] {"2"});
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getDataRecordsCount(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT COUNT(id) AS " + AGR_CNT_FIELD + " FROM " + MAIN_TABLE, null);
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getDisplayRecordsCount(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT COUNT(id) AS " + AGR_CNT_FIELD + " FROM " + DISPLAY_PATTERN_TABLE, null);
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getSearchTagsRecordsCount(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT COUNT(id) AS " + AGR_CNT_FIELD + " FROM " + SEARCH_TAGS_TABLE, null);
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getDataQuantitySum(String quantityTag){
        return database.rawQuery("SELECT SUM(" + quantityTag + ") AS " + AGR_CNT_FIELD +" FROM " + MAIN_TABLE, null);
    }

    public Cursor getListInfo(){
        Cursor cursor;
        try{
            cursor = database.rawQuery("SELECT " + TAG_FIELD + " FROM " + LIST_INFO_TABLE, null);
        }
        catch (Exception e){
            cursor = null;
        }
        return cursor;
    }

    public Cursor getResultTable(){
        return database.rawQuery("SELECT " + TAG_FIELD + " FROM " + RESULT_TAGS_TABLE, null);
    }

    public Cursor getResultTableCount(){
        return database.rawQuery("SELECT COUNT(id) AS " + AGR_CNT_FIELD + " FROM " + RESULT_TAGS_TABLE, null);
    }

    public Cursor getTagTitle(String tag){
        return database.rawQuery("SELECT " + IS_TITLE_FIELD + " FROM " + DISPLAY_PATTERN_TABLE + " WHERE " + CONTENT_FIELD + " = ?",
                new String[]{tag});
    }

    private static String determineAffinity(String type){
        switch (type){
            case "Integer":
                return "INTEGER";
            case "Double":
                return "REAL";
            default:
                return "TEXT";
        }
    }

    private static String determineColor(String color){
        switch (color){
            case "red":
            case "blue":
            case "green":
            case "black":
            case "white":
            case "gray":
            case "cyan":
            case "magenta":
            case "yellow":
                return color;
            default:
                return "black";
        }
    }
}
