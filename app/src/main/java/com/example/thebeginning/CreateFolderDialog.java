package com.example.thebeginning;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.pinball83.maskededittext.MaskedEditText;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 20.04.2016.
 */
public class CreateFolderDialog extends DialogFragment {
    private FileManager fileManager;
    private FileManager xmlManager;
    boolean partOfNameFound = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        xmlManager = bundle.getParcelable(ContentManager.xmlSettingsKey);
        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Dialog dialog = getDialog();

        final XmlRecordData baseData = ((XmlRecordData) xmlManager.readFromFile("mode.xml", new XmlRecordData()));
        final List<XmlRecord> records = baseData.getRecords();
        final List<MaskedEditText> inputFields = new ArrayList<>();

        final MainActivity mainActivity = (MainActivity) getActivity();
        View mainView = inflater.inflate(R.layout.create_folder_main, null);
        LinearLayout linearLayout = mainView.findViewById(R.id.main_view);
        Toolbar toolbar = mainView.findViewById(R.id.create_folder_toolbar);
        toolbar.setTitle(R.string.folder_creation);

        boolean firstIntegerFound = false;

        for(XmlRecord i: records){
            final String parentTag = i.getParentTag();
            if(parentTag.equals("Field")){
                String tag = i.getTagValueByTag("Tag");
                if(tag != null) {
                    String type = i.getTagValueByTag("Type"), title = i.getTagValueByTag("Title");
                    if(type == null) type = "String";
                    if(title == null) title = getString(R.string.input_a_value);
                    View view;
                    View titleView;
                    final MaskedEditText maskedEditText;
                    switch (type){
                        case "String":
                            view = inflater.inflate(R.layout.create_folder_edit_text, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_text);
                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"),tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, i.getTagValueByTag("DefaultValue"), i.getDomain());
                            break;
                        case "Date":
                            view = inflater.inflate(R.layout.create_folder_edit_date, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_date);
                            maskedEditText.setFocusable(false);
                            ImageButton imageButton = view.findViewById(R.id.create_folder_edit_date_button);
                            imageButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String dateStr = maskedEditText.getUnmaskedText();
                                    Date date;
                                    if(dateStr.equals("")){
                                         date = Calendar.getInstance().getTime();
                                    }
                                    else try {
                                        date = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        date = null;
                                    }

                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                                    final DatePicker datePicker = new DatePicker(mainActivity);
                                    datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                                    datePicker.setCalendarViewShown(false);
                                    builder.setTitle(R.string.choose_a_date);
                                    builder.setView(datePicker);
                                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Calendar c = Calendar.getInstance();

                                            c.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                            maskedEditText.setMaskedText(new SimpleDateFormat("yyyyMMdd").format(c.getTime()));
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alertDialog = builder.create();
                                    alertDialog.show();
                                }
                            });
                            ImageButton currentDateButton = view.findViewById(R.id.create_folder_edit_date_current);
                            currentDateButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    maskedEditText.setMaskedText(new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()));
                                }
                            });
                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, null, i.getDomain());
                            break;
                        case "DateTime":
                            view = inflater.inflate(R.layout.create_folder_edit_date_time, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_date);
                            maskedEditText.setFocusable(false);
                            imageButton = view.findViewById(R.id.create_folder_edit_date_button);
                            imageButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    String dateStr = maskedEditText.getUnmaskedText();
                                    Date date;
                                    if(dateStr.equals("")){
                                        date = Calendar.getInstance().getTime();
                                    }
                                    else try {
                                        date = new SimpleDateFormat("yyyyMMddHHmm").parse(dateStr);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        date = null;
                                    }

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                                    View dateTimeView = inflater.inflate(R.layout.date_time, null);
                                    final DatePicker datePicker = dateTimeView.findViewById(R.id.date_picker);
                                    final TimePicker timePicker = dateTimeView.findViewById(R.id.time_picker);

                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);

                                    datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                                    timePicker.setIs24HourView(true);
                                    timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
                                    timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));

                                    builder.setTitle(R.string.choose_date_and_time);
                                    builder.setView(dateTimeView);
                                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Calendar c = Calendar.getInstance();

                                            c.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());

                                            maskedEditText.setMaskedText(new SimpleDateFormat("yyyyMMddHHmm").format(c.getTime()));
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alertDialog = builder.create();
                                    alertDialog.show();
                                }
                            });
                            currentDateButton = view.findViewById(R.id.create_folder_edit_date_current);
                            currentDateButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    maskedEditText.setMaskedText(new SimpleDateFormat("yyyyMMddHHmm").format(Calendar.getInstance().getTime()));
                                }
                            });
                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, null, i.getDomain());
                            break;
                        case "Integer":
                            view = inflater.inflate(R.layout.create_folder_edit_integer, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_integer);

                            String defaultValue = i.getTagValueByTag("DefaultValue");

                            if(!firstIntegerFound){
                                int maxInteger = getThisMonthMaxInteger(tag);
                                if(maxInteger != -1) defaultValue = String.valueOf(maxInteger + 1);
                                firstIntegerFound = true;
                            }

                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"),tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, defaultValue, i.getDomain());
                            break;
                        case "Double":
                            view = inflater.inflate(R.layout.create_folder_edit_double, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_double);
                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"),tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, i.getTagValueByTag("DefaultValue"), i.getDomain());
                            break;
                        case "List":
                            view = null;
                            titleView = null;
                            imageButton = null;
                            setList(linearLayout, view, titleView, title, i, inflater, imageButton, mainActivity, tag, inputFields, i.getDomain());
                            break;
                        case "Boolean":
                            view = inflater.inflate(R.layout.create_folder_edit_text_boolean, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            maskedEditText = view.findViewById(R.id.create_folder_edit_text_boolean);
                            maskedEditText.setEnabled(false);
                            final CheckBox checkBox = view.findViewById(R.id.create_folder_check_box_boolean);
                            String defaultV = i.getTagValueByTag("DefaultValue");
                            if(defaultV != null && !defaultV.equals("1")) checkBox.setChecked(false);
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if(isChecked){
                                        maskedEditText.setText(getString(R.string.logic_yes));
                                        checkBox.setText(getString(R.string.yes));
                                    }
                                    else{
                                        maskedEditText.setText(getString(R.string.logic_no));
                                        checkBox.setText(getString(R.string.no));
                                    }

                                }
                            });
                            addField(linearLayout, inputFields, view, maskedEditText, i.getTagValueByTag("PartOfName"),
                                    i.getTagValueByTag("Required"),tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, defaultV, i.getDomain());
                            break;
                        default:
                            Toast.makeText(mainActivity, String.format(Locale.US, getString(R.string.error_in_settings_file_type_is_not_recognized), type), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
            else if(parentTag.equals("Common")){
                View view = inflater.inflate(R.layout.create_folder_text_view, null);
                TextView textView = view.findViewById(R.id.create_folder_text_view);
                String title = i.getTagValueByTag("Title");
                if(title == null) textView.setText(R.string.input_data);
                else textView.setText(title);
                textView.setTextSize(18);
                textView.setTextColor(Color.BLUE);
                linearLayout.addView(view, 0);
            }
        }

        if(!partOfNameFound){
            String title = getString(R.string.input_folder_name), tag = "DefaultName";
            View view = inflater.inflate(R.layout.create_folder_edit_text, null);
            MaskedEditText maskedEditText = view.findViewById(R.id.create_folder_edit_text);
            maskedEditText.setTag(new FieldData(tag, true, null));
            maskedEditText.setRequired(true);
            View titleView = inflater.inflate(R.layout.create_folder_text_view, null);
            TextView textView = titleView.findViewById(R.id.create_folder_text_view);
            textView.setTextColor(Color.RED);
            textView.setText("*" + title);
            inputFields.add(maskedEditText);
            linearLayout.addView(titleView, 0);
            linearLayout.addView(view, 1);
        }

        View buttonsView = inflater.inflate(R.layout.create_folder_buttons, null);
        Button cancel = buttonsView.findViewById(R.id.create_folder_cancel);
        Button confirm = buttonsView.findViewById(R.id.create_folder_confirm);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean requiredFieldsFilled = true;
                for(MaskedEditText i: inputFields){
                    if(i.isRequired() && i.getUnmaskedText().equals("")){
                        requiredFieldsFilled = false;
                        Toast.makeText(mainActivity, R.string.fill_all_required_fields, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                if(requiredFieldsFilled){
                    XmlRecordData data = new XmlRecordData();
                    XmlRecord additionalData = new XmlRecord("AdditionalData"), mainData = new XmlRecord("SharedData"), systemData = new XmlRecord("SystemData"), paketsData = new XmlRecord("PaketsData");
                    if(records.size() == 0) additionalData.add("DivideByDates", "0");
                    else{
                        String value = records.get(0).getTagValueByTag("DivideByDates");
                        additionalData.add("DivideByDates", value != null && value.equals("1") ? "1" : "0");
                        value = records.get(0).getTagValueByTag("TimeOfShift");
                        additionalData.add("TimeOfShift", value != null ? value : "00:00");

                        value = records.get(0).getTagValueByTag("RewriteCatalogMode");
                        additionalData.add("RewriteCatalogMode", value != null ? value : "0");

                        value = records.get(0).getTagValueByTag("DBPatternFile");
                        if(value != null) additionalData.add("DBPatternFile", value);
                    }
                    String name = "";
                    for(MaskedEditText i: inputFields){
                        FieldData fieldData = (FieldData) i.getTag();
                        String string = i.getText().toString();
                        if(fieldData.isPartOfName()){
                            name += string + " ";
                            if(partOfNameFound) mainData.add(fieldData.getTag(), string.equals("") ? " " : string);
                            else systemData.add(fieldData.getTag(), string.equals("") ? " " : string);
                        }
                        else{
                            String domain = fieldData.getDomain();
                            if(domain != null){
                                if(domain.equals("Fields")) mainData.add(fieldData.getTag(), string.equals("") ? " " : string);
                                else if(domain.equals("Pakets")) paketsData.add(fieldData.getTag(), string.equals("") ? " " : string);
                                else mainData.add(fieldData.getTag(), string.equals("") ? " " : string);
                            }
                            else mainData.add(fieldData.getTag(), string.equals("") ? " " : string);
                        }
                    }
                    data.addRecord(additionalData);
                    data.addRecord(mainData);
                    Date date = Calendar.getInstance().getTime();
                    name += ", " + new SimpleDateFormat("yyyy-MM-dd HH-mm").format(date);
                    systemData.add(getString(R.string.date_of_creation), new SimpleDateFormat("dd-MM-yyyy HH-mm").format(date));
                    systemData.add(getString(R.string.catalog_of_creation), name);
                    data.addRecord(systemData);

                    if(paketsData.getTagRecords().size() > 0) data.addRecord(paketsData);

                    List<XmlRecord> photoFields = baseData.getRecordsByDomain("PhotoFields");
                    for (XmlRecord i: photoFields) data.addRecord(i);

                    fileManager.createFolder(name);
                    fileManager.goAhead(name);
                    fileManager.writeToFile("mode.xml", data);
                    fileManager.goBack();
                    HashMapManager hashMapManager = new HashMapManager(fileManager);
                    hashMapManager.getHashMap();
                    hashMapManager.put(fileManager.getChildLocalPath(name), false);
                    hashMapManager.saveChanges();
                    mainActivity.updateContent(name, getString(R.string.not_sent_yet));
                    dismiss();
                }
            }
        });
        linearLayout.addView(buttonsView);
        return mainView;
    }

    private int getThisMonthMaxInteger(String tag){
        File folder = getThisMonthLatestFolder();
        if(folder != null){
           FileManager temp = new FileManager(fileManager);
            try{
                temp.goAhead(folder.getName());
                XmlRecordData data = (XmlRecordData) temp.readFromFile("mode.xml", new XmlRecordData());
                XmlRecord xmlRecord = data.getRecordByParentTag("SharedData");

                TagRecord tagRecord = xmlRecord.getTagRecordByTag(tag);

                String value = tagRecord != null ? tagRecord.getValue() : null;

                return value != null ? Integer.parseInt(value) : -1;
            }
            catch (Exception e){
                return -1;
            }
        }
        else return -1;
    }

    private File getThisMonthLatestFolder(){
        File file = new File(fileManager.getFullPath());
        File[] files  = file.listFiles();
        if(files != null){
            try {
                List<Folder> foldersList = new ArrayList<>();
                for (File i: files) if(i.isDirectory()) foldersList.add(new Folder(i));

                if(foldersList.size() != 0) {

                    Folder[] folders = foldersList.toArray(new Folder[foldersList.size()]);

                    Arrays.sort(folders);

                    Folder lastFolder = folders[folders.length - 1];

                    Calendar calendar = Calendar.getInstance();

                    calendar.setTime(lastFolder.getDate());

                    int month = calendar.get(Calendar.MONTH), year = calendar.get(Calendar.YEAR);

                    calendar.setTime(Calendar.getInstance().getTime());

                    int currentMonth = calendar.get(Calendar.MONTH), currentYear = calendar.get(Calendar.YEAR);

                    return (year == currentYear && month == currentMonth) ? lastFolder.getFile() : null;
                }
                else return null;

            } catch (ParseException e) {
                return null;
            }
        }
        else return null;
    }

    private void addField(LinearLayout root, List<MaskedEditText> inputFields,View view, MaskedEditText maskedEditText, String partOfName, String required, String tag,
                               View titleView, TextView textView, String title, String predefined, String domain){
        boolean flag = partOfName != null && partOfName.equals("1");
        if(!partOfNameFound && flag) partOfNameFound = true;
        maskedEditText.setTag(new FieldData(tag, flag, domain));
        flag = required != null && required.equals("1");
        maskedEditText.setRequired(flag);
        if(predefined != null) maskedEditText.setText(predefined);
        if(flag){
            textView.setTextColor(Color.RED);
            textView.setText("*" + title);
        }
        else textView.setText(title);
        inputFields.add(maskedEditText);
        root.addView(titleView);
        root.addView(view);
    }

    private void setList(LinearLayout linearLayout, View view,View titleView, String title, XmlRecord record, final LayoutInflater inflater, ImageButton imageButton, final MainActivity mainActivity,
                         String tag, List<MaskedEditText> inputFields, String domain){
        view = inflater.inflate(R.layout.create_folder_edit_list, null);
        LinearLayout fieldsLayout = view.findViewById(R.id.create_folder_edit_list_fields);
        titleView = inflater.inflate(R.layout.create_folder_text_view, null);
        TextView textView = titleView.findViewById(R.id.create_folder_text_view);
        title = record.getTagValueByTag("Title");
        textView.setText(title == null ? getString(R.string.list) : title);
        textView.setTextColor(Color.BLUE);
        fieldsLayout.addView(titleView);
        final List<TagRecord> listTags = record.getTagRecordByMask("Tag");
        XmlRecordData infoData = (XmlRecordData) xmlManager.readFromFile(record.getTagValueByTag("FileList"), new XmlRecordData());
        XmlRecord commonData = infoData.getRecordByParentTag("Common");
        List<TagRecord> allTags = commonData.getTagRecordByMask("Tag");
        List<XmlRecord> allRecords = infoData.getRecords();
        final List<String> titles = new ArrayList<>();
        final List<MaskedEditText> editTexts = new ArrayList<>();
        List<String> finalTags = new ArrayList<>();
        for(TagRecord i: listTags){
            for(TagRecord j: allTags){
                if(i.getTag().equals(j.getTag())){
                    for(XmlRecord k: allRecords){
                        if(k.getParentTag().equals("Field") && k.getTagValueByTag("Tag").equals(j.getValue())){
                            tag = i.getValue();
                            View innerLayout = inflater.inflate(R.layout.create_folder_edit_list_item, null);
                            titleView = inflater.inflate(R.layout.create_folder_text_view, null);
                            title = k.getTagValueByTag("Title");
                            final MaskedEditText maskedEditText1 = innerLayout.findViewById(R.id.create_folder_edit_list);
                            maskedEditText1.setFocusable(false);
                            addField(fieldsLayout, inputFields, innerLayout, maskedEditText1, k.getTagValueByTag("PartOfName"),
                                    k.getTagValueByTag("Required"), tag, titleView, titleView.findViewById(R.id.create_folder_text_view), title, null, domain);
                            titles.add(title == null ? getString(R.string.field) : title);
                            editTexts.add(maskedEditText1);
                            finalTags.add(j.getValue());
                        }
                    }
                }
            }
        }
        XmlRecordData listItemData = (XmlRecordData) xmlManager.readFromFile(commonData.getTagValueByTag("FileList"), new XmlRecordData());
        List<XmlRecord> listItemRecords = listItemData.getRecords();
        for(XmlRecord i: listItemRecords){
            if(i.getParentTag().equals("Field")){
                for(String j: finalTags){
                    String string = i.getTagValueByTag(j);
                    titles.add(string == null ? "-" : string);
                }
            }
        }
        imageButton = view.findViewById(R.id.create_folder_edit_list_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final GridView gridView = (GridView) inflater.inflate(R.layout.create_folder_list_gridview, null);
                int elementsPerRow = editTexts.size();
                gridView.setNumColumns(elementsPerRow);
                gridView.setAdapter(new ArrayAdapter<>(mainActivity, android.R.layout.simple_list_item_1, titles));
                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                builder.setTitle(R.string.choose_a_value);
                builder.setView(gridView);
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                String value = editTexts.get(0).getText().toString();
                int startPosition = -1;
                if(!value.equals("")){
                    int size = titles.size();
                    for(int o = elementsPerRow; o < size; o+= elementsPerRow){
                        if(titles.get(o).equals(value)){
                            startPosition = o;
                            break;
                        }
                    }
                }
                if(startPosition != -1) gridView.smoothScrollToPosition(startPosition);
                final AlertDialog alertDialog = builder.create();
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int elementsPerRow = editTexts.size();
                        if(position < elementsPerRow) return;
                        position -= position%elementsPerRow;
                        for(MaskedEditText editText: editTexts){
                            editText.setText((String) parent.getItemAtPosition(position));
                            ++position;
                        }
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        linearLayout.addView(view);
    }

    private static class FieldData {
        private String domain;
        private String tag;
        private boolean isPartOfName;

        public FieldData(String tag, boolean isPartOfName, String domain){
            this.tag = tag;
            this.isPartOfName = isPartOfName;
            this.domain = domain;
        }

        public String getDomain(){
            return domain;
        }

        public String getTag() {
            return tag;
        }

        public boolean isPartOfName() {
            return isPartOfName;
        }
    }

    private static class Folder implements Comparable<Folder>{
        private File file;
        private Date date;

        public Folder(File file) throws ParseException {
            this.file = file;
            String name = file.getName();
            date = new SimpleDateFormat("yyyy-MM-dd hh-mm").parse(name.substring(name.indexOf(",") + 2));
        }

        public File getFile() {
            return file;
        }

        public Date getDate() {
            return date;
        }

        @Override
        public int compareTo(@NonNull Folder folder) {
            return date.compareTo(folder.date);
        }
    }
}
