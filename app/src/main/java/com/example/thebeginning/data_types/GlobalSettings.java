package com.example.thebeginning.data_types;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 19.01.2018.
 */

public class GlobalSettings {
    public static final String MODE_FULL = "Full", MODE_PHOTO = "Photo", MODE_INVENTORY = "Inventory", MODE_READ_ONLY = "ReadOnly";
    public static final String RU = "Ru", EN = "En", CH = "Ch";

    public static String getModeTitle(String mode, String[] modeTitles){
        switch (mode){
            case GlobalSettings.MODE_FULL:
                return modeTitles[1];
            case GlobalSettings.MODE_PHOTO:
                return modeTitles[0];
            case GlobalSettings.MODE_INVENTORY:
                return modeTitles[2];
            case GlobalSettings.MODE_READ_ONLY:
                return modeTitles[3];
            default:
                return null;
        }
    }

    public static String getListOfLanguages(List<String> languages,
                                            String[] languagesTitles){
        StringBuilder stringBuilder = new StringBuilder();
        for (String i: languages){
            switch (i){
                case RU:
                    stringBuilder.append(languagesTitles[0]).append(" ");
                    break;
                case EN:
                    stringBuilder.append(languagesTitles[1]).append(" ");
                    break;
                case CH:
                    stringBuilder.append(languagesTitles[2]).append(" ");
                    break;
            }
        }

        return stringBuilder.toString();
    }

    public static String getLanguageTitle(String language, String[] languageTitles){
        switch (language){
            case RU:
                return languageTitles[0];
            case EN:
                return languageTitles[1];
            case CH:
                return languageTitles[2];
            default:
                return languageTitles[0];
        }
    }

    public static Locale getLocale(String language){
        switch (language){
            case RU:
                return new Locale("ru_Ru");
            case EN:
                return Locale.ENGLISH;
            case CH:
                return Locale.CHINESE;
            default:
                return Locale.ROOT;
        }
    }


    private List<String> modes = new ArrayList<>();
    private List<String> languages = new ArrayList<>();
    private int jpegQuality;

    public GlobalSettings(List<String> modes, List<String> languages, int jpegQuality){
        this.modes = modes;
        this.languages = languages;
        this.jpegQuality = jpegQuality;
    }

    public List<String> getModes() {
        return modes;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public int getJpegQuality() {
        return jpegQuality;
    }
}
