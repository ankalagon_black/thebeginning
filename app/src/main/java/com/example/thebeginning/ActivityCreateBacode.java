package com.example.thebeginning;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by apple on 12.08.16.
 */
public class ActivityCreateBacode extends AppCompatActivity {

    private static final String LIST_KEY = "ListKey";
    public static final String IS_SAVED_KEY = "IsSaved";
    public static final String MODE_KEY = "Mode";

    public static final String[] modes = {"Default", "EditWithSave", "Search"};
    private String currentMode;
    private boolean isUpdating = false;

    private FileManager fileManager, xmlSettings;
    private boolean changeHashOnFinish;
    private String inputName;
    private StringListData data;

    private DatabaseManager databaseManager;
    private String quantityTag;

    private CreateBarcodeFragmentAdapter adapter;

    private boolean isSearchAvailable = true, isColorIdentificationAvailable = true, isDuplicate = true, isUnidentified = true;
    private boolean isSaved;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_barcode_view);

        Bundle bundle = getIntent().getExtras();
        fileManager = bundle.getParcelable(ContentManager.fileManagerKey);
        xmlSettings = bundle.getParcelable(ContentManager.xmlSettingsKey);
        changeHashOnFinish = bundle.getBoolean(ContentManager.changeHashMapKey);
        inputName = bundle.getString(ContentManager.listItemNameKey);
        isSaved = bundle.getBoolean(IS_SAVED_KEY);
        currentMode = bundle.getString(MODE_KEY);
        data = new StringListData();
        List<String> listInstanse = bundle.getStringArrayList(LIST_KEY);
        if(listInstanse != null) {
            for (String i : listInstanse) {
                data.add(i);
            }
        }

        XmlRecordData xmlRecordData = (XmlRecordData) fileManager.readFromFile("mode.xml", new XmlRecordData());
        String prefix = null;
        if(xmlRecordData != null){
            XmlRecord commomRecord = xmlRecordData.getRecordByParentTag("AdditionalData");
            if(commomRecord != null) {
                TagRecord prefixRecord = commomRecord.getTagRecordByTag("DBPatternFile");
                if (prefixRecord != null) prefix = prefixRecord.getValue();
                XmlRecord xmlRecord = xmlRecordData.getRecordByParentTag("PaketsData");
                if (xmlRecord != null && xmlRecord.getTagRecords().size() > 0) {
                    String value = xmlRecord.getTagValueByTag("AllowDuplicates");
                    if (value != null && value.equals("0")) isDuplicate = false;
                    value = xmlRecord.getTagValueByTag("AllowUnidentifiedRecords");
                    if (value != null && value.equals("0")) isUnidentified = false;
                    value = xmlRecord.getTagValueByTag("AllowColorIdentification");
                    if (value != null && value.equals("0")) isColorIdentificationAvailable = false;
                }
            }
        }

        if(prefix == null) prefix = "pak_mode.xml";

        databaseManager = new DatabaseManager(prefix);
        databaseManager.open(fileManager);

        final List<String> errorList = new ArrayList<>();

        Cursor dataCntCursor = databaseManager.getDataRecordsCount();
        if(dataCntCursor != null) {
            dataCntCursor.moveToFirst();
            int dataRowCount = dataCntCursor.getInt(dataCntCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD));
            if (dataRowCount == 0) errorList.add(getString(R.string.error_no_data));
            dataCntCursor.close();
        }
        else errorList.add(getString(R.string.error_no_data));

        Cursor searchTagsCursor = databaseManager.getSearchTagsRecordsCount();
        if(searchTagsCursor != null) {
            searchTagsCursor.moveToFirst();
            if (searchTagsCursor.getInt(searchTagsCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD)) == 0)
                errorList.add(getString(R.string.error_no_search_tags));
            searchTagsCursor.close();
        }
        else errorList.add(getString(R.string.error_no_search_tags));

        Cursor displayCursor = databaseManager.getDisplayRecordsCount();
        if(displayCursor != null) {
            displayCursor.moveToFirst();
            if (displayCursor.getInt(displayCursor.getColumnIndex(DatabaseManager.AGR_CNT_FIELD)) == 0)
                errorList.add(getString(R.string.error_no_display_pattern));
            displayCursor.close();
        }
        else errorList.add(getString(R.string.error_no_display_pattern));

        if(errorList.size() != 0) isSearchAvailable = false;

        Cursor quantityCursor = databaseManager.getQuantityTag();
        if(quantityCursor != null) {
            if(quantityCursor.getCount() != 0){
                quantityCursor.moveToFirst();
                quantityTag = quantityCursor.getString(quantityCursor.getColumnIndex(DatabaseManager.TAG_FIELD));
            }
            else errorList.add(getString(R.string.error_no_quantity_tag));
            quantityCursor.close();
        }
        else errorList.add(getString(R.string.error_no_quantity_tag));

        if(errorList.size() > 0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCreateBacode.this);
                    builder.setTitle(R.string.error);
                    builder.setItems(errorList.toArray(new CharSequence[errorList.size()]), null);
                    builder.setPositiveButton(getString(R.string.ok), null);
                    builder.create().show();
                }
            }, 1000);
            isColorIdentificationAvailable = false;
        }


        adapter = new CreateBarcodeFragmentAdapter(getSupportFragmentManager());
        final ViewPager viewPager = findViewById(R.id.create_barcode_view_pager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if(state == 1 && isColorIdentificationAvailable){
                    FragmentCreateBarcode zero = (FragmentCreateBarcode) adapter.getItem(0),
                            one = (FragmentCreateBarcode) adapter.getItem(1);
                    one.fillTreeView(zero.getAllResults(), zero.getGreenResults(), zero.getBlueResults(), zero.getLightRedResults(),
                            zero.getDarkResults(), zero.getYellowResults());
                }
            }
        });

        Toolbar toolbar = findViewById(R.id.create_barocode_toolbar);
        toolbar.setNavigationIcon(R.drawable.bl);
        switch (currentMode){
            case "Default":
                toolbar.setTitle(R.string.mode_0);
                break;
            case "EditWithSave":
                toolbar.setTitle(R.string.mode_1);
                break;
            case "Search":
                toolbar.setTitle(R.string.mode_2);
                Toast.makeText(ActivityCreateBacode.this, "В режиме поиска нельзя создавать и сохранять данные", Toast.LENGTH_LONG).show();
                break;
        }
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_barcode_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_update_bd:
                updateDataBase();
                return true;
            case R.id.action_search_bd:
//                searchInDatabase();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateDataBase(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                isUpdating = true;
                DialogDatabaseUpdateProgressBar dialogDatabaseUpdateProgressBar = new DialogDatabaseUpdateProgressBar();
                dialogDatabaseUpdateProgressBar.show(getFragmentManager(),"1");
                dialogDatabaseUpdateProgressBar.waitForCreation();
                DatabaseManager.updateFromXmlFile(fileManager, xmlSettings, dialogDatabaseUpdateProgressBar, databaseManager.getFileName(), ActivityCreateBacode.this);
                dialogDatabaseUpdateProgressBar.finish();
                isUpdating = false;
                Intent intent = getIntent();
                FragmentCreateBarcode fragmentCreateBarcode = (FragmentCreateBarcode) adapter.getItem(0);
                Bundle bundle = new Bundle();
                if(fragmentCreateBarcode.getAllAdapter().getCount() > 0) bundle.putBoolean(IS_SAVED_KEY, false);
                else bundle.putBoolean(IS_SAVED_KEY, true);
                bundle.putParcelable(ContentManager.fileManagerKey, fileManager);
                bundle.putStringArrayList(LIST_KEY, fragmentCreateBarcode.getMergedStrings());
                bundle.putParcelable(ContentManager.xmlSettingsKey, xmlSettings);
                bundle.putBoolean(ContentManager.changeHashMapKey, true);
                bundle.putString(ContentManager.listItemNameKey, inputName);
                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                bundle.putString(ActivityCreateBacode.MODE_KEY, currentMode);
                intent.putExtras(bundle);
                finish();
                startActivity(intent);
            }
        }).start();
    }

    private void searchInDatabase(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();
                bundle.putParcelable(ContentManager.fileManagerKey, fileManager);
                bundle.putParcelable(ContentManager.xmlSettingsKey, xmlSettings);
                bundle.putBoolean(ContentManager.changeHashMapKey, true);
                bundle.putString(ContentManager.listItemNameKey, null);
                bundle.putBoolean(ActivityCreateBacode.IS_SAVED_KEY, true);
                bundle.putString(ActivityCreateBacode.MODE_KEY, ActivityCreateBacode.modes[2]);
                Intent intent = getIntent();
                intent.putExtras(bundle);
                finish();
                startActivity(intent);
            }
        }).start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final ArrayAdapterBarcodeRecord mainArrayAdapter =  ((FragmentCreateBarcode) adapter.getItem(0)).getAllAdapter();
            if(mainArrayAdapter.getCount() == 0 || isSaved || currentMode.equals(modes[2])){
                finish();
                return true;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.warning);
            builder.setMessage(R.string.create_barcode_exit_message);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int size = mainArrayAdapter.getCount();
                    for (int i = 0; i < size; i++) {
                        data.add(mainArrayAdapter.getItem(i).mergeData());
                    }
                    if (changeHashOnFinish) {
                        String newName = inputName == null ? String.valueOf(fileManager.getFilesCount()) + ", " +
                                new SimpleDateFormat("dd MM yyyy HH-mm").format(Calendar.getInstance().getTime())
                                + "." + StringListData.class.getSimpleName() : inputName;
                        fileManager.writeToFile(newName, data);
                        String currentFolderName = fileManager.getCurrentFolderName();
                        fileManager.goBack();
                        HashMapManager hashMapManager = new HashMapManager(new FileManager(fileManager));
                        hashMapManager.getHashMap();
                        hashMapManager.put(fileManager.getChildLocalPath(currentFolderName), false);
                        hashMapManager.saveChanges();
                        fileManager.goAhead(currentFolderName);
                    } else
                        fileManager.writeToFile(String.valueOf(fileManager.getFilesCount()) + "." +
                                StringListData.class.getSimpleName(), data);
                    finish();
                }
            });
            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            if(mainArrayAdapter.getCount() == 0){
                button.setEnabled(false);
                button.setAlpha(0.4f);
            }
            return true;
        }
        return false;
    }

    public DatabaseManager getDatabaseManager(){
        return databaseManager;
    }

    public boolean isDuplicate(){
        return isDuplicate;
    }

    public boolean isUnidentified(){
        return isUnidentified;
    }

    public boolean isSearchAvailable(){
        return isSearchAvailable;
    }

    public boolean isColorIdentificationAvailable(){
        return isColorIdentificationAvailable;
    }

    public String getQuantityTag() {
        return quantityTag;
    }

    public FileManager getFileManager(){
        return fileManager;
    }

    public String getInputName(){
        return inputName;
    }

    public StringListData getData(){
        return data;
    }

    public void setInputName(String value){
        this.inputName = value;
    }

    public boolean isSaved(){
        return isSaved;
    }

    public void setIsSaved(boolean isSaved){
        this.isSaved = isSaved;
    }

    public String getCurrentMode() {
        return currentMode;
    }

    public boolean isUpdating(){
        return isUpdating;
    }

    private class CreateBarcodeFragmentAdapter extends FragmentStatePagerAdapter {

        public CreateBarcodeFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentCreateBarcode.getInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void finish() {
        super.finish();
        FragmentCreateBarcode.zero = null;
        FragmentCreateBarcode.one = null;
    }
}
